import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FriendsIdsDto } from 'src/common/dto/friends/friends.dto.out';
import { CreateRelationShipDto } from 'src/common/dto/friends/friends.dto.in';
import { FriendsEntity } from 'src/infra/typeorm/entities/friends.entity';
import { EntityManager, Repository } from 'typeorm';
import { User } from 'src/common/dto/user/user.dto.out';
import { UsersService } from '../user/user.service';

@Injectable()
export class FriendsService {
    private readonly logger = new Logger(FriendsService.name);
    constructor(
        private entityManager: EntityManager,
        private usersService: UsersService,
        @InjectRepository(FriendsEntity)
        private friendsRepository: Repository<FriendsEntity>,
    ) {}

    async relationshipExists(friendsIds: FriendsIdsDto): Promise<boolean> {
        const relationship = await this.friendsRepository.find(friendsIds);
        if (
            relationship[0]?.userId === friendsIds.userId &&
            relationship[0]?.associatedFriendUserId === friendsIds.associatedFriendUserId
        )
            return true;
        return false;
    }

    async storeNewRelationship(newFriendsIds: CreateRelationShipDto): Promise<void> {
        await this.friendsRepository.insert(newFriendsIds);
        const reciprocalRelationship: CreateRelationShipDto = {
            userId: newFriendsIds.associatedFriendUserId,
            associatedFriendUserId: newFriendsIds.userId,
        };
        await this.friendsRepository.insert(reciprocalRelationship);
    }

    async removeRelationship(friendsIds: FriendsIdsDto): Promise<void> {
        await this.friendsRepository.delete(friendsIds);
        const reciprocalRelationship: FriendsIdsDto = {
            userId: friendsIds.associatedFriendUserId,
            associatedFriendUserId: friendsIds.userId,
        };
        await this.friendsRepository.delete(reciprocalRelationship);
    }

    async findAllFriendsId(userId: number): Promise<number[]> | null {
        const allFriendsRelationship = await this.friendsRepository.find({ where: { userId: userId } });
        const allFriendsId = allFriendsRelationship?.map(({ associatedFriendUserId }) => associatedFriendUserId);
        return allFriendsId;
    }

    async getPotentialFriendsLogin(self: string): Promise<string[]> {
        const userId = await this.usersService.findIdByLogin(self);
        const allFriendsId = await this.findAllFriendsId(userId);
        if (allFriendsId?.length !== 0) {
            const notFriends = await this.entityManager.query(`select login from users
        where not (id = any(ARRAY[${allFriendsId}])) and id != ${userId}`);
            const notFriendsLogin = notFriends?.map((item: { login: string }) => item.login);
            return notFriendsLogin;
        } else {
            const notFriends = await this.entityManager.query(`select login from users
        where id != ${userId}`);
            const notFriendsLogin = notFriends?.map((item: { login: string }) => item.login);
            return notFriendsLogin;
        }
    }

    async getFriendData(friendId: number): Promise<User> | null {
        const userFromDb = await this.usersService.findById(friendId);
        const isActive = await this.usersService.userIsActive(friendId);
        const isPlaying = await this.usersService.userIsPlaying(friendId);
        return {
            id: friendId,
            login: userFromDb.login,
            username: userFromDb.username,
            avatar: userFromDb.avatar,
            isActive,
            isPlaying,
        };
    }
}
