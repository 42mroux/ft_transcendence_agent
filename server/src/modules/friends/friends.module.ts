import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FriendsEntity } from 'src/infra/typeorm/entities/friends.entity';
import { ChatModule } from '../chat/chat.module';
import { UsersModule } from '../user/user.module';
import { FriendsController } from './friends.controller';
import { FriendsService } from './friends.service';

@Module({
    imports: [TypeOrmModule.forFeature([FriendsEntity]), UsersModule, ChatModule],
    controllers: [FriendsController],
    providers: [FriendsService],
})
export class FriendsModule {}
