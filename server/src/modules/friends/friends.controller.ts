import { Controller, Get, HttpException, HttpStatus, Param, Post } from '@nestjs/common';
import { Context } from 'src/config/context.decorator';
import { UsersService } from '../user/user.service';
import { FriendsService } from './friends.service';
import { ContextType } from '../../config/types';
import { ChatService } from '../chat/chat.service';
import { User } from 'src/common/dto/user/user.dto.out';

@Controller('friends')
export class FriendsController {
    constructor(
        private friendsService: FriendsService,
        private readonly usersService: UsersService,
        private chatService: ChatService,
    ) {}

    @Post('new-friend/:login')
    async addNewFriend(@Context() context: ContextType, @Param() params: { login: string }): Promise<void> {
        const userId = await this.usersService.findIdByLogin(context.login);
        const associatedFriendUserId = await this.usersService.findIdByLogin(params.login);
        const alreadyFriends = await this.friendsService.relationshipExists({ userId, associatedFriendUserId });
        if (alreadyFriends === false) {
            await this.friendsService.storeNewRelationship({ userId, associatedFriendUserId });
            const roomId = await this.chatService.createRoomForDM(params.login, userId);
            await this.chatService.notifyUserToJoinRoom(associatedFriendUserId, roomId);
            await this.chatService.associateUserRoomId(userId, roomId);
            await this.chatService.associateUserRoomId(associatedFriendUserId, roomId);
            await this.chatService.notifyUserOfChangeInFriends(associatedFriendUserId);
        }
    }

    @Post('unfriend/:login')
    async removeFriend(@Context() context: ContextType, @Param() params: { login: string }): Promise<void> {
        const userId = await this.usersService.findIdByLogin(context.login);
        const associatedFriendUserId = await this.usersService.findIdByLogin(params.login);
        const alreadyFriends = await this.friendsService.relationshipExists({ userId, associatedFriendUserId });
        if (alreadyFriends === true) {
            await this.friendsService.removeRelationship({ userId, associatedFriendUserId });
            const roomId = await this.chatService.findDMRoomId({ userId, associatedFriendUserId });
            if (roomId !== undefined) {
                await this.chatService.removeDMRoom(roomId);
                await this.chatService.removeUsersFromRoom(roomId);
                await this.chatService.notifyUserOfChangeInFriends(associatedFriendUserId);
            }
        }
    }

    // /!\ UNDER CONSTRUCTION /!\
    @Get('potential-relationships')
    async usersNotFriends(@Context() context: ContextType): Promise<User[]> | null {
        const allLogins = await this.friendsService.getPotentialFriendsLogin(context.login);
        const allPotentialFriends = await Promise.all(
            allLogins?.map(async (login) => {
                const userFromDB = await this.usersService.findByLoginInDB(login);
                return { ...userFromDB, isActive: false, isPlaying: false };
            }),
        );
        return allPotentialFriends;
    }

    @Get('all')
    async returnAllFriends(@Context() context: ContextType): Promise<User[]> | null {
        const userId = await this.usersService.findIdByLogin(context.login);
        const allFriendsId = await this.friendsService.findAllFriendsId(userId);
        const allFriends = await Promise.all(
            allFriendsId?.map(async (friendId) => await this.friendsService.getFriendData(friendId)),
        );
        return allFriends;
    }

    @Get('chat-guests/:roomId')
    async findPotentialFriendsForChannel(
        @Context() context: ContextType,
        @Param() params: { roomId: number },
    ): Promise<User[] | null> {
        if ((params.roomId as unknown as string) == 'undefined')
            throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
        const userId = await this.usersService.findIdByLogin(context.login);
        const allFriendsId = await this.friendsService.findAllFriendsId(userId);
        const usersInRoom = await this.chatService.findUsersIdsForRoom(params.roomId);
        const potentialGuestsId = allFriendsId?.filter(
            (friendId) => usersInRoom?.find((userId) => userId === friendId) === undefined,
        );
        const potentialGuests: User[] = await Promise.all(
            potentialGuestsId?.map(async (guestId) => {
                const userFromDB = await this.usersService.findById(guestId);
                return { ...userFromDB, isActive: false, isPlaying: false };
            }),
        );
        return potentialGuests;
    }

    @Get('already-friends/:login')
    async isAlreadyFriend(@Context() context: ContextType, @Param() params: { login: string }): Promise<boolean> {
        const userId = await this.usersService.findIdByLogin(context.login);
        const associatedFriendUserId = await this.usersService.findIdByLogin(params.login);
        const isFriend = await this.friendsService.relationshipExists({ userId, associatedFriendUserId });
        return isFriend;
    }
}
