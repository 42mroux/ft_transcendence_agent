import { Body, Controller, Get, HttpException, HttpStatus, Param, Post } from '@nestjs/common';
import { Context } from 'src/config/context.decorator';
import { UsersService } from '../user/user.service';
import { ChatService } from './chat.service';
import { ContextType } from '../../config/types';
import { RoomDto, RoomOverviewDto } from 'src/common/dto/chat/room.dto.out';
import { MessageInConversationDto } from 'src/common/dto/chat/messages.dto.out';
import { ChatGateway } from './chat.gateway';
import { User } from 'src/common/dto/user/user.dto.out';
import { InjectRepository } from '@nestjs/typeorm';
import { SocketEntity } from 'src/infra/typeorm/entities/socket.entity';
import { EntityManager, Repository } from 'typeorm';
import { UserRoomEntity } from 'src/infra/typeorm/entities/user_x_room.entity';
import * as bcrypt from 'bcrypt';
import { CreateRoomDto } from 'src/common/dto/chat/room.dto.in';
import { RoomTypeEnum } from 'src/common/types/room.types';

@Controller('chat')
export class ChatController {
    constructor(
        private chatService: ChatService,
        private readonly usersService: UsersService,
        private chatGateway: ChatGateway,
        @InjectRepository(UserRoomEntity)
        private usersRoomRepository: Repository<UserRoomEntity>,
        @InjectRepository(SocketEntity)
        private socketRepository: Repository<SocketEntity>,
        private entityManager: EntityManager,
    ) {}

    @Post('new-room')
    async createRoomForChannel(@Body() body: { room: CreateRoomDto }): Promise<RoomDto> {
        let hashedPassword: string = null;
        if (body.room.typeId === 4) {
            // if (body.room.password === null)
            // throw error
            // else
            hashedPassword = await bcrypt.hash(body.room.password, 10);
        }
        const roomId = await this.chatService.createRoomForChannel({ ...body.room, password: hashedPassword });
        await this.chatService.associateUserRoomId(body.room.ownerId, roomId);
        await this.chatService.associateAdminRoomId(body.room.ownerId, roomId);
        return { roomId, name: body.room.name, image: body.room.image };
    }

    @Get('rooms')
    async findAllRoomsForUser(@Context() context: ContextType): Promise<RoomOverviewDto[]> {
        const userId = await this.usersService.findIdByLogin(context.login);
        const rooms = await this.chatService.findRoomsForUser(userId);
        const roomsOverview = await Promise.all(
            rooms?.map(async (room) => {
                if (room.typeId === 1) return this.chatService.getDMRoomOverview(room.id, userId);
                else return this.chatService.getChannelRoomOverview(room.id);
            }),
        );
        return roomsOverview;
    }

    @Get('discoverable-rooms')
    async findAllDiscoverableRoomsForUser(@Context() context: ContextType): Promise<RoomOverviewDto[]> {
        const userId = await this.usersService.findIdByLogin(context.login);
        const discoverableRooms = await this.chatService.findDiscoverableRoomsForUser(userId);
        return discoverableRooms;
    }

    @Get('users/:roomId')
    async findAllUsersForRoom(@Param() params: { roomId: number }): Promise<User[] | null> {
        if ((params.roomId as unknown as string) == 'undefined')
            throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
        const allUsersId = await this.chatService.findUsersIdsForRoom(params.roomId);
        const allUsers = await Promise.all(
            allUsersId?.map(async (userId) => {
                const userFromDB = await this.usersService.findById(userId);
                const isActive = await this.usersService.userIsActive(userId);
                const isPlaying = await this.usersService.userIsPlaying(userId);
                return { ...userFromDB, isActive, isPlaying };
            }),
        );
        return allUsers;
    }

    // @Get('common-rooms/:login')
    // async findCommonRooms(@Context() context: ContextType, @Param() params: { login: string }): Promise<void> {
    //     const userId = await this.usersService.findIdByLogin(context.login);
    //     const associatedFriendUserId = await this.usersService.findIdByLogin(params.login);
    //     await this.chatService.findCommonRooms({ userId: userId, associatedFriendUserId: associatedFriendUserId });
    // }

    @Get('dm-room/:login')
    async findDMRoom(@Context() context: ContextType, @Param() params: { login: string }): Promise<RoomOverviewDto> {
        try {
            const userId = await this.usersService.findIdByLogin(context.login);
            const associatedFriendUserId = await this.usersService.findIdByLogin(params.login);
            const roomId = await this.chatService.findDMRoomId({
                userId: userId,
                associatedFriendUserId: associatedFriendUserId,
            });
            const dmRoomOverview = await this.chatService.getDMRoomOverview(roomId, userId);
            return dmRoomOverview;
        } catch (e) {
            return null;
        }
    }

    @Post('delete-room/:id')
    async deleteRoom(@Param() params: { id: number }): Promise<void> {
        await this.chatService.removeRoomById(params.id);
        await this.chatService.notifyUsersOfChangeInRoom(params.id);
    }

    @Get('conversation/:roomId')
    async findConversation(
        @Context() context: ContextType,
        @Param() params: { roomId: number },
    ): Promise<MessageInConversationDto[]> {
        if ((params.roomId as unknown as string) == 'undefined') {
            throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
        }
        const conversation = await this.chatService.retrieveMessagesByRoomId(params.roomId);
        const userId = await this.usersService.findIdByLogin(context.login);
        const blockedUsersId = await this.usersService.findUsersIdBlockedBy(userId);
        if (blockedUsersId !== null) {
            const censoredConversation = conversation?.filter(
                (message) => blockedUsersId.find((blockedUserId) => blockedUserId == message.senderId) === undefined,
            );
            return censoredConversation;
        }
        return conversation;
    }

    @Post('room-name')
    async changeRoomName(@Body() body: { data: { roomName: string; roomId: number } }): Promise<void> {
        await this.chatService.changeRoomName(body.data.roomName, body.data.roomId);
        await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
    }

    @Post('user-in')
    async addUserToRoom(
        @Context() context: ContextType,
        @Body() body: { data: { userId: number; roomId: number } },
    ): Promise<void> {
        if ((await this.chatService.addUserToRoom(body.data.userId, body.data.roomId)) === true) {
            await this.chatService.userAddedToRoomNotifications(
                body.data.userId,
                context.login,
                body.data.roomId,
                this.chatGateway.server,
            );
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
        }
    }

    @Post('user-out')
    async removeUserFromRoom(@Body() body: { data: { userId: number; roomId: number } }): Promise<void> {
        if ((await this.chatService.isUserInRoom(body.data.userId, body.data.roomId)) === true) {
            await this.usersRoomRepository.delete({ userId: body.data.userId, roomId: body.data.roomId });
            // leave
            if ((await this.chatService.isAdmin(body.data.userId, body.data.roomId)) === true)
                await this.chatService.removeAdminFromRoom(body.data.userId, body.data.roomId);
            await this.chatService.userLeftRoomNotifications(
                body.data.userId,
                body.data.roomId,
                this.chatGateway.server,
            );
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
        }
    }

    @Post('add-admin')
    async addAdminToRoom(@Body() body: { data: { userId: number; roomId: number } }): Promise<void> {
        if ((await this.chatService.isAdmin(body.data.userId, body.data.roomId)) === false) {
            await this.chatService.addAdminToRoom(body.data.userId, body.data.roomId);
            const roomName = await this.chatService.findRoomName(body.data.roomId);
            const notification = `You are now an administrator of the channel "${roomName}"`;
            const socketId = await this.socketRepository.findOne({ where: { userId: body.data.userId } });
            if (socketId !== undefined)
                await this.chatGateway.server.to(socketId.socketId).emit('notification', notification);
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
        }
    }

    @Post('remove-admin')
    async removeAdminFromRoom(@Body() body: { data: { userId: number; roomId: number } }): Promise<void> {
        if ((await this.chatService.isAdmin(body.data.userId, body.data.roomId)) === true) {
            await this.chatService.removeAdminFromRoom(body.data.userId, body.data.roomId);
            const roomName = await this.chatService.findRoomName(body.data.roomId);
            const notification = `You are not an administrator of the channel "${roomName}" anymore`;
            const socketId = await this.socketRepository.findOne({ where: { userId: body.data.userId } });
            if (socketId !== undefined)
                await this.chatGateway.server.to(socketId.socketId).emit('notification', notification);
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
        }
    }

    @Post('mute')
    async muteUser(@Body() body: { data: { userId: number; roomId: number; duration: number } }): Promise<void> {
        if (
            (await this.chatService.isUserInRoom(body.data.userId, body.data.roomId)) === true &&
            (await this.usersService.isUserStillMutedForRoom(body.data.userId, body.data.roomId)) === false
        ) {
            await this.usersService.muteUserForRoom(body.data.userId, body.data.roomId, body.data.duration);
            const roomName = await this.chatService.findRoomName(body.data.roomId);
            const timeout = await this.chatService.durationTime(body.data.duration);
            const notification = `You have been muted ${timeout}, you can no longer send messages in the channel "${roomName}"`;
            const socketId = await this.socketRepository.findOne({ where: { userId: body.data.userId } });
            if (socketId !== undefined)
                await this.chatGateway.server.to(socketId.socketId).emit('notification', notification);
            const time = new Date().toString();
            const userName = await this.usersService.findLoginById(body.data.userId);
            const content = `${userName} was muted ${timeout}`;
            const conversationNotification: MessageInConversationDto = {
                senderId: null,
                roomId: body.data.roomId,
                time,
                content,
            };
            await this.chatService.storeAndEmitConversationNotification(
                conversationNotification,
                this.chatGateway.server,
            );
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
            if (body.data.duration !== 0) {
                setTimeout(async () => {
                    await this.usersService.deleteMutedUserForRoom(body.data.userId, body.data.roomId);
                    await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
                }, body.data.duration * 60000);
            }
        }
    }

    @Post('ban')
    async banUser(@Body() body: { data: { userId: number; roomId: number; duration: number } }): Promise<void> {
        if (
            (await this.chatService.isUserInRoom(body.data.userId, body.data.roomId)) === true &&
            (await this.usersService.isUserStillBannedFromRoom(body.data.userId, body.data.roomId)) === false
        ) {
            await this.usersService.banUserFromRoom(body.data.userId, body.data.roomId, body.data.duration);
            const roomName = await this.chatService.findRoomName(body.data.roomId);
            const timeout = await this.chatService.durationTime(body.data.duration);
            const notification = `You have been banned ${timeout}, you can no longer see and send messages in the channel "${roomName}"`;
            const socketId = await this.socketRepository.findOne({ where: { userId: body.data.userId } });
            if (socketId !== undefined)
                await this.chatGateway.server.to(socketId.socketId).emit('notification', notification);
            const time = new Date().toString();
            const userName = await this.usersService.findLoginById(body.data.userId);
            const content = `${userName} was banned ${timeout}`;
            const conversationNotification: MessageInConversationDto = {
                senderId: null,
                roomId: body.data.roomId,
                time,
                content,
            };
            await this.chatService.storeAndEmitConversationNotification(
                conversationNotification,
                this.chatGateway.server,
            );
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
            if (body.data.duration !== 0) {
                setTimeout(async () => {
                    await this.usersService.deleteBannedUserFromRoom(body.data.userId, body.data.roomId);
                    await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
                }, body.data.duration * 60000);
            } else await this.usersRoomRepository.delete({ userId: body.data.userId, roomId: body.data.roomId });
        }
    }

    @Get('owners/:roomId')
    async findPotentialOwners(@Param() params: { roomId: number }): Promise<User[] | null> {
        if ((params.roomId as unknown as string) == 'undefined')
            throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
        const roomUsersId = await this.chatService.findUsersIdsForRoom(params.roomId);
        const roomOwnerId = await this.chatService.findOwnerIdForRoom(params.roomId);
        const potentialOwnersId = roomUsersId?.filter(async (roomUserId) => {
            roomUserId !== roomOwnerId &&
                (await this.usersService.isUserStillBannedFromRoom(roomUserId, params.roomId)) === false;
        });
        const potentialOwners = await Promise.all(
            potentialOwnersId?.map(async (potentialOwnerId) => {
                const userFromDB = await this.usersService.findById(potentialOwnerId);
                return { ...userFromDB, isActive: false, isPlaying: false };
            }),
        );
        return potentialOwners;
    }

    @Post('change-owner')
    async changeOwnerForRoom(@Body() body: { data: { userId: number; roomId: number } }): Promise<void> {
        if ((await this.chatService.isUserInRoom(body.data.userId, body.data.roomId)) === true) {
            await this.entityManager.query(
                `update rooms set "ownerId" = ${body.data.userId} where id = ${body.data.roomId}`,
            );
            if ((await this.chatService.isAdmin(body.data.userId, body.data.roomId)) === false)
                this.chatService.addAdminToRoom(body.data.userId, body.data.roomId);
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
        }
    }

    @Post('join-room')
    async joinRoom(
        @Context() context: ContextType,
        @Body() body: { data: { roomId: number; password: string | null } },
    ): Promise<boolean> {
        if ((await this.chatService.isRoomProtected(body.data.roomId)) === true) {
            try {
                await this.chatService.verifyPasswordForRoom(body.data.roomId, body.data.password);
            } catch (e) {
                return false;
            }
        }
        const userId = await this.usersService.findIdByLogin(context.login);
        if ((await this.chatService.addUserToRoom(userId, body.data.roomId)) === true) {
            await this.chatService.userJoinedRoomNotifications(userId, body.data.roomId, this.chatGateway.server);
            await this.chatService.notifyUsersOfChangeInRoom(body.data.roomId);
            return true;
        }
        return false;
    }

    @Get('room-type/:roomId')
    async findRoomType(@Param() params: { roomId: number }): Promise<number> {
        if ((params.roomId as unknown as string) == 'undefined')
            throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
        const roomType = await this.entityManager.query(`select "typeId" from rooms where id = ${params.roomId}`);
        return roomType[0].typeId;
    }

    @Post('new-password')
    async newPasswordForRoom(
        @Context() context: ContextType,
        @Body() body: { data: { roomId: number; password: string } },
    ): Promise<boolean> {
        const ownerId = await this.chatService.findOwnerIdForRoom(body.data.roomId);
        const userId = await this.usersService.findIdByLogin(context.login);
        if (ownerId !== userId) return false;
        else {
            const roomType = await this.findRoomType({ roomId: body.data.roomId });
            try {
                if (roomType === RoomTypeEnum.PUBLIC)
                    await this.chatService.updateRoomStatus(body.data.roomId, RoomTypeEnum.PROTECTED);
                await this.chatService.updatePasswordForRoom(body.data.roomId, body.data.password);
                return true;
            } catch (e) {
                return false;
            }
        }
    }

    @Post('update-type')
    async updateTypeForRoom(
        @Context() context: ContextType,
        @Body() body: { data: { roomId: number; newType: number } },
    ): Promise<void> {
        const ownerId = await this.chatService.findOwnerIdForRoom(body.data.roomId);
        const userId = await this.usersService.findIdByLogin(context.login);
        if (ownerId !== userId) return;
        if ((await this.chatService.roomExists(body.data.roomId)) === true) {
            try {
                await this.chatService.updateRoomStatus(body.data.roomId, RoomTypeEnum.PUBLIC);
            } catch (e) {
                return;
            }
        }
    }
}
