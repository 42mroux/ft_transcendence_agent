import { forwardRef, HttpException, HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InsertNewMessageDto } from 'src/common/dto/chat/messages.dto.in';
import { MessageInConversationDto } from 'src/common/dto/chat/messages.dto.out';
import { RoomOverviewDto } from 'src/common/dto/chat/room.dto.out';
import { FriendsIdsDto } from 'src/common/dto/friends/friends.dto.out';
import { socketAndUserIdsDto } from 'src/common/dto/socket/socket.dto.in';
import { AdminRoomEntity } from 'src/infra/typeorm/entities/admin_x_room.entity';
import { MessageEntity } from 'src/infra/typeorm/entities/message.entity';
import { RoomEntity } from 'src/infra/typeorm/entities/room.entity';
import { SocketEntity } from 'src/infra/typeorm/entities/socket.entity';
import { UserRoomEntity } from 'src/infra/typeorm/entities/user_x_room.entity';
import { EntityManager, Repository } from 'typeorm';
import { UsersService } from '../user/user.service';
import { Socket, Server } from 'socket.io';
import { MutedUserEntity } from 'src/infra/typeorm/entities/muted_user.entity';
import { BannedUserEntity } from 'src/infra/typeorm/entities/banned_user.entity';
import { CreateRoomDto } from 'src/common/dto/chat/room.dto.in';
import * as bcrypt from 'bcrypt';
import { ChatGateway } from './chat.gateway';

@Injectable()
export class ChatService {
    private readonly logger = new Logger(ChatService.name);
    constructor(
        @Inject(forwardRef(() => ChatGateway))
        private chatGateway: ChatGateway,
        private entityManager: EntityManager,
        @InjectRepository(RoomEntity)
        private roomsRepository: Repository<RoomEntity>,
        @InjectRepository(UserRoomEntity)
        private usersRoomRepository: Repository<UserRoomEntity>,
        @InjectRepository(MessageEntity)
        private messagesRepository: Repository<MessageEntity>,
        @InjectRepository(AdminRoomEntity)
        private adminsRoomRepository: Repository<AdminRoomEntity>,
        @InjectRepository(SocketEntity)
        private socketRepository: Repository<SocketEntity>,
        @InjectRepository(MutedUserEntity)
        private mutedUsersRepository: Repository<MutedUserEntity>,
        @InjectRepository(BannedUserEntity)
        private bannedUsersRepository: Repository<MutedUserEntity>,
        private readonly usersService: UsersService,
    ) {}

    async createRoomForDM(newFriendLogin: string, userId: number): Promise<number> {
        const newRoom = await this.roomsRepository.insert({ typeId: 1, ownerId: userId });
        return newRoom.identifiers[0].id;
    }

    async createRoomForChannel(room: CreateRoomDto): Promise<number> {
        const newRoom = await this.roomsRepository.insert({
            ownerId: room.ownerId,
            name: room.name,
            image: room.image,
            typeId: room.typeId,
            password: room.password,
        });
        return newRoom.identifiers[0].id;
    }

    async associateUserRoomId(userId: number, roomId: number): Promise<void> {
        await this.usersRoomRepository.insert({ userId: userId, roomId: roomId });
    }

    async associateAdminRoomId(userId: number, roomId: number): Promise<void> {
        await this.adminsRoomRepository.insert({ adminId: userId, roomId: roomId });
    }

    async removeUsersFromRoom(roomId: number): Promise<void> {
        if (roomId !== undefined) await this.usersRoomRepository.delete(roomId);
    }

    async roomExists(roomId: number): Promise<boolean> {
        try {
            this.roomsRepository.findOne({ where: { id: roomId } });
            return true;
        } catch (e) {
            return false;
        }
    }

    async findRoomNameById(roomId: number): Promise<string> {
        const room = await this.roomsRepository.findOne({ where: { id: roomId } });
        return room.name;
    }

    async findRoomsForUser(userId: number): Promise<RoomEntity[]> {
        const associatedUserRooms = await this.usersRoomRepository.find({ where: { userId: userId } });
        const rooms = await Promise.all(
            associatedUserRooms?.map(
                async (associatedIds) => await this.roomsRepository.findOne({ where: { id: associatedIds.roomId } }),
            ),
        );
        return rooms;
    }

    async findUsersIdsForRoom(roomId: number): Promise<number[]> {
        const usersIdArr = await this.entityManager.query(`select "userId" from users_x_rooms uxr
        where "roomId" = ${roomId}`);
        const usersId = usersIdArr?.map((user: { userId: number }) => user.userId);
        return usersId;
    }

    async findAdminsIdsForRoom(roomId: number): Promise<number[]> {
        const adminsIdArr = await this.entityManager.query(`select "adminId" from admins_x_rooms uxr
        where "roomId" = ${roomId}`);
        const adminsId = adminsIdArr?.map((admin: { adminId: number }) => admin.adminId);
        return adminsId;
    }

    async getChannelRoomOverview(roomId: number): Promise<RoomOverviewDto> {
        const roomFromDB = await this.roomsRepository.findOne({ where: { id: roomId } });
        const imageFilePath = await this.usersService.getUserAvatarFilePath(roomFromDB.image);
        const usersIds = await this.findUsersIdsForRoom(roomId);
        const adminsIds = await this.findAdminsIdsForRoom(roomId);
        const mutedIds = await this.findMutedUsersForRoom(roomId);
        const bannedIds = await this.findBannedUsersFromRoom(roomId);
        return {
            roomId: roomFromDB.id,
            type: roomFromDB.typeId,
            name: roomFromDB.name,
            image: imageFilePath,
            usersIds,
            adminsIds,
            ownerId: roomFromDB.ownerId,
            mutedIds,
            bannedIds,
        };
    }

    async getDMRoomOverview(roomId: number, selfId: number): Promise<RoomOverviewDto> {
        const roomFromDB = await this.roomsRepository.findOne({ where: { id: roomId } });
        const name = await this.dmRoomName(roomId, selfId);
        const image = await this.dmRoomImage(roomId, selfId);
        const usersIds = await this.findUsersIdsForRoom(roomId);
        const roomOverview: RoomOverviewDto = {
            roomId: roomFromDB.id,
            type: roomFromDB.typeId,
            name,
            image,
            usersIds,
            adminsIds: usersIds,
            ownerId: roomFromDB.ownerId,
            mutedIds: null,
            bannedIds: null,
        };
        return roomOverview;
    }

    // /!\ Both following find to return RoomEntity[] soon ?
    async findCommonRooms(friendsId: FriendsIdsDto): Promise<number[]> {
        const commonRooms = await this.entityManager.query(`select A."roomId" from (
        select * from users_x_rooms uxr
        where "userId" = ${friendsId.userId}
    ) A
    inner join (
        select * from users_x_rooms uxr2
        where "userId" = ${friendsId.associatedFriendUserId}
    ) B
    on A."roomId" = B."roomId"`);
        const commonRoomsId = commonRooms.map((room: { [x: string]: unknown }) => room['roomId']);
        return commonRoomsId;
    }

    async findDMRoomId(friendsId: FriendsIdsDto): Promise<number> {
        const commonRoomsId = await this.findCommonRooms(friendsId);
        if (!commonRoomsId || commonRoomsId?.length == 0) throw new Error('No Room');
        const commonDMRoom = await this.entityManager.query(`select * from rooms
            where id = any(ARRAY[${commonRoomsId}]) and "typeId" = 1`);
        return commonDMRoom[0].id;
    }

    async removeDMRoom(roomId: number): Promise<void> {
        if (roomId !== undefined) await this.roomsRepository.delete(roomId);
    }

    async removeRoomById(roomId: number): Promise<void> {
        if (roomId !== undefined) await this.roomsRepository.delete(roomId);
    }

    async dmRoomName(roomId: number, userId: number): Promise<string> {
        const friendId = await this.entityManager.query(
            `select "userId" from users_x_rooms uxr where "roomId" = ${roomId} and "userId" != ${userId}`,
        );
        if (friendId !== undefined) {
            const friendLogin = await this.usersService.findUsernameById(friendId[0].userId);
            return friendLogin;
        }
        return null;
    }

    async dmRoomImage(roomId: number, userId: number): Promise<string> {
        const friendId = await this.entityManager.query(
            `select "userId" from users_x_rooms uxr where "roomId" = ${roomId} and "userId" != ${userId}`,
        );
        if (friendId !== undefined) {
            const friendAvatar = await this.usersService.findAvatarById(friendId[0].userId);
            return friendAvatar;
        }
        return null;
    }

    async storeMessageInDB(newMessage: InsertNewMessageDto): Promise<void> {
        await this.messagesRepository.insert(newMessage);
    }

    async retrieveMessagesByRoomId(roomId: number): Promise<MessageInConversationDto[]> {
        const conversation = await this.messagesRepository.find({ where: { roomId } });
        return conversation;
    }

    async changeRoomName(roomName: string, roomId: number): Promise<void> {
        await this.entityManager.query(`update rooms set "name" = '${roomName}' where id = ${roomId}`);
    }

    async isUserInRoom(userId: number, roomId: number): Promise<boolean> {
        const roomUsersId = await this.findUsersIdsForRoom(roomId);
        let alreadyIn = false;
        roomUsersId?.map((roomUserId) => {
            if (roomUserId === userId) alreadyIn = true;
        });
        return alreadyIn;
    }

    async addUserToRoom(userId: number, roomId: number): Promise<boolean> {
        if (
            userId !== undefined &&
            roomId !== undefined &&
            (await this.isUserInRoom(userId, roomId)) === false &&
            (await this.usersService.isUserStillBannedFromRoom(userId, roomId)) === false
        ) {
            await this.usersRoomRepository.insert({ userId, roomId });
            return true;
        }
        return false;
    }

    async findRoomName(roomId: number): Promise<string> {
        const roomName = await this.entityManager.query(`select "name" from rooms where id = ${roomId}`);
        return roomName?.[0].name;
    }

    async findOwnerIdForRoom(roomId: number): Promise<number> {
        const room = await this.roomsRepository.find({ where: { id: roomId } });
        return room[0].ownerId;
    }

    async isAdmin(userId: number, roomId: number): Promise<boolean> {
        const isAdmin = await this.adminsRoomRepository.find({ where: { adminId: userId, roomId } });
        if (isAdmin[0]) return true;
        return false;
    }

    async addAdminToRoom(userId: number, roomId: number): Promise<void> {
        this.adminsRoomRepository.insert({ adminId: userId, roomId });
    }

    async removeAdminFromRoom(userId: number, roomId: number): Promise<void> {
        this.adminsRoomRepository.delete({ adminId: userId, roomId });
    }

    async findMutedUsersForRoom(roomId: number): Promise<number[] | null> {
        const mutedUsers = await this.mutedUsersRepository.find({ where: { roomId } });
        const stillMutedUsers = await Promise.all(
            mutedUsers?.filter(
                async (muteUser) => await this.usersService.isUserStillMutedForRoom(muteUser.userId, roomId),
            ),
        );
        if (stillMutedUsers[0]) {
            const mutedUsersId = stillMutedUsers.map((muteUser) => muteUser.userId);
            return mutedUsersId;
        }
        return null;
    }

    async findBannedUsersFromRoom(roomId: number): Promise<number[] | null> {
        const bannedUsers = await this.bannedUsersRepository.find({ where: { roomId } });
        const stillBannedUsers = await Promise.all(
            bannedUsers?.filter(
                async (bannedUser) => await this.usersService.isUserStillBannedFromRoom(bannedUser.userId, roomId),
            ),
        );
        if (stillBannedUsers[0]) {
            const bannedUsersId = stillBannedUsers.map((bannedUser) => bannedUser.userId);
            return bannedUsersId;
        }
        return null;
    }

    async durationTime(duration: number): Promise<string> {
        if (duration === 0) return 'for ever';
        if (duration < 60) return `for ${duration} minute(s)`;
        if (duration === 60) return `for an hour`;
    }

    // FOR CHAT GATEwAY
    async isUserStored(userId: number): Promise<boolean> {
        const user = await this.socketRepository.find({ where: { userId: userId } });
        if (user) {
            this.logger.log(`user found = ${user}`);
            return true;
        }
        return true;
    }

    async storeSocketAndUserId(socketId: string, userId: number): Promise<void> {
        const toInsert: socketAndUserIdsDto = { socketId: socketId, userId: userId };
        await this.socketRepository.insert(toInsert);
    }

    async deleteSocketsForUser(userId: number): Promise<void> {
        await this.socketRepository.delete({ userId: userId });
    }

    async joinAllRooms(socket: Socket, userId: number): Promise<void> {
        const allRooms = await this.findRoomsForUser(userId);
        allRooms?.map((room) => {
            socket.join(room.id.toString());
        });
    }

    async leaveAllRooms(socket: Socket, userId: number): Promise<void> {
        const allRooms = await this.findRoomsForUser(userId);
        allRooms?.map((room) => {
            socket.leave(room.id.toString());
        });
    }

    async notifyFriendsOfConnection(userId: number): Promise<void> {
        const allRooms = await this.findRoomsForUser(userId);
        if (allRooms !== null) {
            allRooms.map((room) => {
                if (room.typeId === 1) this.chatGateway.server.to(room.id.toString()).emit('online', userId);
            });
        }
    }

    async notifyFriendsOfDisconnection(userId: number): Promise<void> {
        const allRooms = await this.findRoomsForUser(userId);
        if (allRooms !== null) {
            allRooms.map((room) => {
                if (room.typeId === 1) this.chatGateway.server.to(room.id.toString()).emit('offline', userId);
            });
        }
    }

    async userAddedToRoomNotifications(
        userAdded: number,
        addedBy: string,
        channelAddedTo: number,
        server: Server,
    ): Promise<void> {
        const time = new Date().toString();
        const channelName = await this.findRoomName(channelAddedTo);
        const addedUserNotification = `You were added to the discussion "${channelName}"`;
        const socketId = await this.socketRepository.findOne({ where: { userId: userAdded } });
        if (socketId !== undefined) {
            server.to(socketId.socketId).emit('notification', addedUserNotification);
            server.to(socketId.socketId).emit('join-room', channelAddedTo);
        }
        const userName = await this.usersService.findLoginById(userAdded);
        const content = `${userName} was added to this conversation by ${addedBy}`;
        const conversationNotification: MessageInConversationDto = {
            senderId: null,
            roomId: channelAddedTo,
            time,
            content,
        };
        this.storeAndEmitConversationNotification(conversationNotification, server);
    }

    async userLeftRoomNotifications(goneUser: number, channelLeft: number, server: Server): Promise<void> {
        const time = new Date().toString();
        const channelName = await this.findRoomName(channelLeft);
        const goneUserNotification = `You left the discussion "${channelName}"`;
        const socketId = await this.socketRepository.findOne({ where: { userId: goneUser } });
        if (socketId !== undefined) server.to(socketId.socketId).emit('notification', goneUserNotification);
        const userName = await this.usersService.findLoginById(goneUser);
        const content = `${userName} left this conversation`;
        const conversationNotification: MessageInConversationDto = {
            senderId: null,
            roomId: channelLeft,
            time,
            content,
        };
        this.storeAndEmitConversationNotification(conversationNotification, server);
    }

    async userJoinedRoomNotifications(newUser: number, channelJoined: number, server: Server): Promise<void> {
        const time = new Date().toString();
        const channelName = await this.findRoomName(channelJoined);
        const addedUserNotification = `You joined the discussion "${channelName}"`;
        const socketId = await this.socketRepository.findOne({ where: { userId: newUser } });
        if (socketId !== undefined) {
            server.to(socketId.socketId).emit('notification', addedUserNotification);
            server.to(socketId.socketId).emit('join-room', channelJoined);
        }
        const userName = await this.usersService.findLoginById(newUser);
        const content = `${userName} joined this conversation`;
        const conversationNotification: MessageInConversationDto = {
            senderId: null,
            roomId: channelJoined,
            time,
            content,
        };
        this.storeAndEmitConversationNotification(conversationNotification, server);
    }

    async notifyUsersOfChangeInRoom(roomId: number): Promise<void> {
        this.chatGateway.server.in(roomId.toString()).emit('modification');
    }

    async storeAndEmitConversationNotification(
        conversationNotification: MessageInConversationDto,
        server: Server,
    ): Promise<void> {
        this.storeMessageInDB(conversationNotification);
        server.in(conversationNotification.roomId.toString()).emit('events', conversationNotification);
    }

    async getHashedPasswordForRoom(roomId: number): Promise<string> {
        const roomFromDB = await this.roomsRepository.find({ where: { id: roomId } });
        if (roomFromDB[0]) {
            return roomFromDB[0].password;
        } else {
            // throw error
            return "room doesn't exists or doesnt have a password";
        }
    }

    async findDiscoverableRoomsForUser(userId: number): Promise<RoomOverviewDto[]> {
        const allPublicAndProtectedRooms = await this.entityManager.query(`
        select * from rooms
            where "ownerId" != ${userId}
            and ("typeId" = 3 or "typeId" = 4)`);
        const allDiscoverableRoomsIndex = await Promise.all(
            allPublicAndProtectedRooms?.map(async (publicOrProtectedRoom: RoomEntity) => {
                if (
                    (await this.isUserInRoom(userId, publicOrProtectedRoom.id)) === false &&
                    (await this.usersService.isUserStillBannedFromRoom(userId, publicOrProtectedRoom.id)) === false
                )
                    return true;
                else return false;
            }),
        );
        const allDiscoverableRooms = allPublicAndProtectedRooms?.filter(
            (room: RoomEntity, index: number) => allDiscoverableRoomsIndex[index],
        );
        const allDiscoverableRoomsOverview: RoomOverviewDto[] = await Promise.all(
            allDiscoverableRooms?.map(
                async (discoverableRoom: RoomEntity) => await this.getChannelRoomOverview(discoverableRoom.id),
            ),
        );
        return allDiscoverableRoomsOverview;
    }

    async isRoomProtected(roomId: number): Promise<boolean> {
        const roomType = await this.entityManager.query(`select "typeId" from rooms where id = ${roomId}`);
        if (roomType[0].typeId === 4) return true;
        else return false;
    }

    async verifyPasswordForRoom(roomId: number, password: string): Promise<void> {
        const hashedPassword = await this.getHashedPasswordForRoom(roomId);
        const passwordMatches = await bcrypt.compare(password, hashedPassword);
        if (passwordMatches === false) throw new HttpException("Password doesn't match", HttpStatus.MISDIRECTED);
    }

    async updateRoomStatus(roomId: number, newType: number): Promise<void> {
        if ((await this.roomExists(roomId)) === true) {
            try {
                await this.entityManager.query(`
                update rooms set "typeId" = ${newType} where id = ${roomId}`);
            } catch (e) {
                throw new HttpException('Updating room status failed', HttpStatus.FAILED_DEPENDENCY);
            }
        }
    }

    async updatePasswordForRoom(roomId: number, newPassword: string): Promise<void> {
        if ((await this.roomExists(roomId)) === true) {
            const hashedPassword = await bcrypt.hash(newPassword, 10);
            try {
                await this.entityManager.query(
                    `update rooms set "password" = '${hashedPassword}' where id = ${roomId}`,
                );
            } catch (e) {
                throw new HttpException('Updating room password failed', HttpStatus.FAILED_DEPENDENCY);
            }
        }
    }

    async notifyBlockingUser(blockedUserId: number, blockedById: number, server: Server): Promise<boolean> {
        const socketId = await this.socketRepository.findOne({ where: { userId: blockedById } });
        const isBlocked = await this.usersService.isUserBlocked(blockedUserId, blockedById);
        if (socketId !== undefined && isBlocked === true) {
            server.to(socketId.socketId).emit('block-user');
            return true;
        }
        return false;
    }

    async notifyUserOfChangeInFriends(userId: number): Promise<boolean> {
        const socketId = await this.socketRepository.findOne({ where: { userId } });
        if (socketId !== undefined) {
            this.chatGateway.server.to(socketId.socketId).emit('friends-change');
            return true;
        }
        return false;
    }

    async notifyUserToJoinRoom(userId: number, roomId: number) {
        const socketId = await this.socketRepository.findOne({ where: { userId } });
        if (socketId !== undefined) {
            this.chatGateway.server.to(socketId.socketId).emit('join-room', roomId);
        }
    }
}
