import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminRoomEntity } from 'src/infra/typeorm/entities/admin_x_room.entity';
import { MessageEntity } from 'src/infra/typeorm/entities/message.entity';
import { RoomEntity } from 'src/infra/typeorm/entities/room.entity';
import { UserRoomEntity } from 'src/infra/typeorm/entities/user_x_room.entity';
import { UsersModule } from '../user/user.module';
import { ChatGateway } from './chat.gateway';
import { ChatController } from './chat.controller';
import { ChatService } from './chat.service';
import { SocketEntity } from 'src/infra/typeorm/entities/socket.entity';
import { MutedUserEntity } from 'src/infra/typeorm/entities/muted_user.entity';
import { BannedUserEntity } from 'src/infra/typeorm/entities/banned_user.entity';
import { BlockedUserEntity } from 'src/infra/typeorm/entities/blocked_user.entity';

@Module({
    controllers: [ChatController],
    providers: [ChatGateway, ChatService],
    imports: [
        TypeOrmModule.forFeature([
            RoomEntity,
            UserRoomEntity,
            MessageEntity,
            AdminRoomEntity,
            SocketEntity,
            MutedUserEntity,
            BannedUserEntity,
            BlockedUserEntity,
        ]),
        forwardRef(() => UsersModule),
    ],
    exports: [ChatService],
})
export class ChatModule {}
