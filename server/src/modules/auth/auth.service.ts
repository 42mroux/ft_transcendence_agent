import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { UserFrom42ApiDto, UserFromDBDto } from '../../common/dto/user/user.dto.out';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(private httpService: HttpService, private jwtService: JwtService) {}

    async getTokenFromCode(code: string): Promise<string> {
        const response = await this.httpService
            .post(
                `/oauth/token?grant_type=authorization_code&code=${code}&client_id=${process.env.REACT_APP_TRANS_CLIENT_ID}&client_secret=${process.env.TRANS_CLIENT_SECRET}&redirect_uri=http%3A%2F%2Flocalhost%3A3001%2Fprogress`,
            )
            .toPromise();
        return response.data.access_token;
    }

    async find42UserIn42API(token: string): Promise<UserFrom42ApiDto> {
        const response = await this.httpService
            .get(`/v2/me`, { headers: { Authorization: `Bearer ${token}` } })
            .toPromise();
        const user: UserFrom42ApiDto = {
            login: response.data.login,
            firstName: response.data.first_name,
            lastName: response.data.last_name,
            avatar: null,
        };
        return user;
    }

    async createJWT(user: UserFromDBDto): Promise<string> {
        const payload = {
            login: user.login,
            username: user.username,
        };
        const token = this.jwtService.sign(JSON.stringify(payload));
        return token;
    }
}
