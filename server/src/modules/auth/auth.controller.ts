import { Controller, Get, HttpException, HttpStatus, Param } from '@nestjs/common';
import { UsersService } from '../user/user.service';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService,
        private usersService: UsersService,
        private readonly jwtService: JwtService,
    ) {}

    // NEED ACCESS TO INTRA TO GET VALID ACCESS RIGHTS TO API
    @Get(':code')
    async get42UserLogin(@Param() params: { code: string }): Promise<string> {
        try {
            const token = await this.authService.getTokenFromCode(params.code);
            if (!token) throw new HttpException('Token not valid', HttpStatus.FORBIDDEN);
            const user42 = await this.authService.find42UserIn42API(token);
            if (!user42) throw new HttpException('42 user not found', HttpStatus.NOT_FOUND);
            const userDB = await this.usersService.findByLoginInDB(user42.login);
            if (userDB === null)
                this.usersService.insert({
                    login: user42.login,
                    username: null,
                    twoFactorAuthenticationSecret: null,
                    avatar: null,
                });
            return user42.login;
        } catch (e) {
            throw new HttpException('Token not valid', HttpStatus.FORBIDDEN);
        }
    }

    @Get('jwt/generate/:login')
    async generateJWT(@Param() params: { login: string }): Promise<string> {
        const user = await this.usersService.findByLoginInDB(params.login);
        if (user === undefined) throw new HttpException('User do not exist', HttpStatus.FORBIDDEN);
        const token = await this.authService.createJWT(user);
        return token;
    }

    @Get('jwt/validate/:token')
    async isTokenValid(@Param() params: { token: string }): Promise<string> | null {
        try {
            const payload = await this.jwtService.verify(params.token, { secret: process.env.TRANS_JWT_SECRET });
            return payload.login;
        } catch (e) {
            return null;
        }
    }
}
