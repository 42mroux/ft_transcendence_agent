import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { UsersModule } from 'src/modules/user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';

@Module({
    controllers: [AuthController],
    providers: [AuthService],
    imports: [
        UsersModule,
        HttpModule.register({ baseURL: 'https://api.intra.42.fr', timeout: 10000 }),
        JwtModule.register({ secret: process.env.TRANS_JWT_SECRET }),
    ],
})
export class AuthModule {}
