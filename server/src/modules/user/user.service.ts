import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../infra/typeorm/entities/user.entity';
import { EntityManager, Not, Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { UserFromDBDto } from 'src/common/dto/user/user.dto.out';
import { SocketEntity } from 'src/infra/typeorm/entities/socket.entity';
import { CreateUserDto } from 'src/common/dto/user/user.dto.in';
import { MutedUserEntity } from 'src/infra/typeorm/entities/muted_user.entity';
import { BannedUserEntity } from 'src/infra/typeorm/entities/banned_user.entity';
import { ScoresEntity } from 'src/infra/typeorm/entities/scores.entity';
import { BlockedUserEntity } from 'src/infra/typeorm/entities/blocked_user.entity';
import { unlink } from 'fs/promises';
import * as fs from 'fs';
import { GameEntity } from 'src/infra/typeorm/entities/game.entity';
import { GameStatus } from 'src/common/types/game.globals.types';
import { API_PORT } from 'src/common/network';

@Injectable()
export class UsersService {
    private readonly logger = new Logger(UsersService.name);
    constructor(
        private entityManager: EntityManager,
        @InjectRepository(UserEntity)
        private usersRepository: Repository<UserEntity>,
        @InjectRepository(SocketEntity)
        private socketsRepository: Repository<SocketEntity>,
        @InjectRepository(MutedUserEntity)
        private mutedUsersRepository: Repository<MutedUserEntity>,
        @InjectRepository(BannedUserEntity)
        private bannedUsersRepository: Repository<BannedUserEntity>,
        @InjectRepository(GameEntity)
        private gamesRepository: Repository<GameEntity>,
        @InjectRepository(ScoresEntity)
        private scoresRepository: Repository<ScoresEntity>,
        @InjectRepository(BlockedUserEntity)
        private blockedUsersRepository: Repository<BlockedUserEntity>,
        private jwtService: JwtService,
    ) {}

    async insert(user: CreateUserDto): Promise<void> {
        await this.usersRepository.insert(user);
    }
    async findAll(): Promise<UserFromDBDto[]> {
        return this.usersRepository.find();
    }

    async enable2FA(login: string) {
        const userId = await this.findIdByLogin(login);
        await this.usersRepository.update(userId, {});
    }

    async findById(id: number): Promise<UserFromDBDto> {
        const userFromDb = await this.usersRepository.findOne(id);
        const avatarFilePath = await this.getUserAvatarFilePath(userFromDb.avatar);
        return { id: userFromDb.id, login: userFromDb.login, username: userFromDb.username, avatar: avatarFilePath };
    }

    async findIdByLogin(login: string): Promise<number> {
        const user = await this.usersRepository.findOne({ where: { login } });
        return user.id;
    }

    async findLoginById(id: number): Promise<string> {
        const user = await this.usersRepository.findOne({ where: { id } });
        return user?.login;
    }

    async findLoginByUsername(username: string): Promise<string> {
        const user = await this.usersRepository.findOne({ where: { username } });
        return user?.login;
    }

    async findUsernameById(id: number): Promise<string> {
        const user = await this.usersRepository.findOne({ where: { id } });
        return user?.username;
    }

    async findByLoginInDB(login: string): Promise<UserFromDBDto | null> {
        const user = await this.usersRepository.findOne({ where: { login: `${login}` } });
        if (user !== undefined) {
            const avatarFilePath = await this.getUserAvatarFilePath(user.avatar);
            return { id: user.id, login: user.login, username: user.username, avatar: avatarFilePath };
        }
        return null;
    }

    async findByUsernameInDB(username: string): Promise<UserFromDBDto> {
        return this.usersRepository.findOne({ where: { username } });
    }

    async findAvatarById(id: number): Promise<string> {
        const user = await this.usersRepository.findOne({ where: { id } });
        if (user !== undefined) {
            const avatarFilePath = await this.getUserAvatarFilePath(user.avatar);
            return avatarFilePath;
        }
        return null;
    }

    async returnUserEntity(login: string): Promise<UserEntity> {
        return this.usersRepository.findOne({ where: { login: login } });
    }

    async remove(id: string): Promise<void> {
        await this.usersRepository.delete({ id: parseInt(id) });
    }

    async storeUsername(id: number, username: string) {
        await this.entityManager.query(`
        update users set "username" = '${username}' where id = ${id}`);
    }

    async storeAvatarFilename(avatar: string, id: number): Promise<boolean> {
        await this.usersRepository
            .createQueryBuilder()
            .update(UserEntity)
            .set({ avatar })
            .where('id = :id', { id })
            .execute();
        return true;
    }

    async set2FASecret(login: string, secret: string) {
        const userId = await this.findIdByLogin(login);
        return this.usersRepository.update(userId, {
            twoFactorAuthenticationSecret: secret,
        });
    }

    async getSecret(login: string): Promise<string> {
        const twoFASecret = await this.usersRepository.find({
            select: ['twoFactorAuthenticationSecret'],
            where: { login },
        });
        return twoFASecret[0] ? twoFASecret[0].twoFactorAuthenticationSecret : null;
    }

    async is2FASecretCreated(login: string): Promise<boolean> {
        const secret = await this.getSecret(login);
        return secret !== null && secret !== '';
    }

    async remove2FASecret(login: string) {
        const userId = await this.findIdByLogin(login);
        await this.usersRepository.update(userId, { twoFactorAuthenticationSecret: null });
    }

    async getLoginFromValidJWT(token: string): Promise<string> | null {
        try {
            const payload = await this.jwtService.verify(token, { secret: process.env.TRANS_JWT_SECRET });
            return payload.login;
        } catch (e) {
            return null;
        }
    }

    async getIdFromValidJWT(token: string): Promise<number> | null {
        try {
            const payload = await this.jwtService.verify(token, { secret: process.env.TRANS_JWT_SECRET });
            const userId = await this.findIdByLogin(payload.login);
            return userId;
        } catch (e) {
            return null;
        }
    }

    // TODO /!\ UNDER CONSTRUCTION /!\
    async findSocketByUserId(): Promise<string | null> {
        try {
            return 'found socketId';
        } catch (e) {
            return null;
        }
    }

    async userIsActive(userId: number): Promise<boolean> {
        const socket = await this.socketsRepository.find({ where: { userId } });
        if (socket[0]) return true;
        return false;
    }

    async userIsPlaying(userId: number): Promise<boolean> {
        const games = await this.getCurrentGames(userId);
        const playing = !!(games?.length > 0);
        return playing;
    }

    async muteUserForRoom(userId: number, roomId: number, duration: number): Promise<void> {
        const timeout = new Date();
        timeout.setMinutes(timeout.getMinutes() + duration);
        await this.mutedUsersRepository.insert({ userId, roomId, time: timeout });
    }

    async isUserStillMutedForRoom(userId: number, roomId: number): Promise<boolean> {
        const mutedUser = await this.mutedUsersRepository.find({ where: { userId, roomId } });
        if (mutedUser[0]) {
            const remainingTime = mutedUser[0].time.getTime() - new Date().getTime();
            if (remainingTime > 0) return true;
        }
        return false;
    }

    async deleteMutedUserForRoom(userId: number, roomId: number): Promise<void> {
        await this.mutedUsersRepository.delete({ userId, roomId });
    }

    async banUserFromRoom(userId: number, roomId: number, duration: number): Promise<void> {
        const timeout = new Date();
        timeout.setMinutes(timeout.getMinutes() + duration);
        await this.bannedUsersRepository.insert({ userId, roomId, time: timeout });
    }

    async isUserStillBannedFromRoom(userId: number, roomId: number): Promise<boolean> {
        const bannedUser = await this.bannedUsersRepository.find({ where: { userId, roomId } });
        if (bannedUser[0]) {
            const remainingTime = bannedUser[0].time.getTime() - new Date().getTime();
            if (remainingTime > 0) return true;
        }
        return false;
    }

    async deleteBannedUserFromRoom(userId: number, roomId: number): Promise<void> {
        await this.bannedUsersRepository.delete({ userId, roomId });
    }

    async findUserScores(userId: number): Promise<ScoresEntity> {
        const userScores = await this.scoresRepository.findOne({ where: { playerId: userId } });
        return userScores !== undefined
            ? userScores
            : { id: 0, playerId: userId, victories: 0, losses: 0, equalities: 0, winningRatio: 0 };
    }

    // async findRank(userId: number): Promise<number> {
    //     const totalRanking = await this.scoresRepository.find({
    //         select: ['playerId', 'winningRatio'],
    //         order: { winningRatio: 'DESC' },
    //     });
    //     const rank = totalRanking.findIndex((element: { playerId: number }) => element.playerId === userId);
    //     return rank === -1 ? 0 : rank;
    // }
    async findRank(userId: number): Promise<number> {
        const result = await this.entityManager.query(`
		with ranks as (
			select
				"playerId" ,
				rank() over (order by "winningRatio" desc) as "rank"
			from scores s
		)
		select rank from ranks where "playerId" = ${userId}`);
        return result?.[0]?.rank;
    }

    async isUserBlocked(blockedUserId: number, blockedById: number): Promise<boolean> {
        const isBlocked = await this.blockedUsersRepository.find({ where: { blockedUserId, blockedById } });
        if (isBlocked[0] === undefined) return false;
        else return true;
    }

    async findUsersIdBlockedBy(blockedById: number): Promise<number[] | null> {
        const usersBlocked = await this.entityManager.query(
            `select "blockedUserId" from blocked_users where "blockedById"=${blockedById} `,
        );
        if (usersBlocked[0]) {
            const usersBlockedId = usersBlocked.map((el: { blockedUserId: number }) => el.blockedUserId);
            return usersBlockedId;
        }
        return null;
    }

    getUserAvatarFilePath(filename: string): string {
        return filename ? `http://localhost:${API_PORT}/avatars/${filename}` : null;
    }

    async deleteAvatarFromFolder(filename: string): Promise<{ status: string; msg: string }> {
        try {
            await unlink(__dirname + `/../../../avatars/${filename}`);
            return { status: '200', msg: `·successfully deleted ${filename}` };
        } catch (error) {
            return { status: '500', msg: `there was an issue when deleting ${filename}` };
        }
    }

    isFileInFolder(filename: string): boolean {
        const files = fs.readdirSync(__dirname + '/../../../avatars/');
        files.forEach((file) => {
            if (file === filename) return true;
        });
        return false;
    }

    async isAvatarTaken(filename: string): Promise<boolean> {
        const avatarTaken = await this.usersRepository.findOne({ where: { avatar: filename } });
        return avatarTaken !== undefined ? true : false;
    }

    pickRandomAvatar(): string | null {
        const availableAvatars = fs.readdirSync(__dirname + '/../../../avatars/');
        if (availableAvatars === []) return null;
        const chosenAvatar = this.getUserAvatarFilePath(
            availableAvatars[Math.floor(Math.random() * availableAvatars.length)],
        );
        return chosenAvatar;
    }
    async getCurrentGames(userId: number): Promise<GameEntity[]> {
        const games = await this.gamesRepository.find({
            where: [
                { player1Id: userId, status: Not(GameStatus.ENDED) },
                { player2Id: userId, status: Not(GameStatus.ENDED) },
            ],
        });
        return games;
    }
}
