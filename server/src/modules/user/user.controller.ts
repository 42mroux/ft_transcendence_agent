import { Controller, Get, HttpException, HttpStatus, Param, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { UserForAuthContextDto, UserFromDBDto, UserProfile } from 'src/common/dto/user/user.dto.out';
import { UsersService } from './user.service';
import { ContextType } from '../../config/types';
import { Context } from 'src/config/context.decorator';
import { MatchResults } from 'src/common/dto/game/game.dto.out';
import { GameService } from '../game/game.service';
import { EntityManager, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/infra/typeorm/entities/user.entity';
import { Logger } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from '../../middlewares/fileInterceptor/utils';

@Controller('users')
export class UsersController {
    private readonly logger: Logger;

    constructor(
        private usersService: UsersService,
        private gameService: GameService,
        private entityManager: EntityManager,
        @InjectRepository(UserEntity)
        private usersReporitory: Repository<UserEntity>,
    ) {
        this.logger = new Logger(UsersController.name);
    }

    @Get('')
    async getUser(@Context() context: ContextType): Promise<UserForAuthContextDto | null> {
        try {
            const user = await this.usersService.findByLoginInDB(context.login);
            const blockedUsersId = await this.usersService.findUsersIdBlockedBy(user.id);
            return { ...user, blockedUsersId };
        } catch (e) {
            return null;
        }
    }

    @Get('socket')
    async getSocketForUser(): Promise<string | null> {
        return await this.usersService.findSocketByUserId();
    }

    @Get('/blocked-users')
    async getBlockedUsersId(@Context() context: ContextType): Promise<number[] | null> {
        const blockedById = await this.usersService.findIdByLogin(context.login);
        const blockedUsersId = await this.usersService.findUsersIdBlockedBy(blockedById);
        return blockedUsersId;
    }

    @Get('avatar/random')
    async getRandomAvatar(): Promise<string | null> {
        let randomAvatar = this.usersService.pickRandomAvatar();
        // avatar must be move to /users to be static served but doesntt work yet
        // fs.copyFile(
        //     __dirname + '/../../../avatars/available/',
        //     __dirname + '/../../../avatars/users/',
        //     (err) => {
        //         if (err) return null;
        //     },
        // );
        // instead, check in db if already taken
        while (randomAvatar === null || (await this.usersService.isAvatarTaken(randomAvatar.split('/').pop())))
            randomAvatar = this.usersService.pickRandomAvatar();
        return randomAvatar;
    }

    @Get('avatar')
    async getUserAvatarByLogin(@Context() context: ContextType): Promise<string | null> {
        const userId = await this.usersService.findIdByLogin(context.login);
        const avatar = this.usersService.findAvatarById(userId);
        return avatar;
    }

    @Post('/store/:username')
    async validateAndStoreUsername(
        @Context() context: ContextType,
        @Param() params: { username: string },
    ): Promise<boolean> {
        if (params.username.match(/^[a-zA-Z_]+$/)) {
            const user = await this.usersService.findByUsernameInDB(params.username);
            if (user === undefined) {
                const ret = await this.usersReporitory
                    .createQueryBuilder()
                    .update(UserEntity)
                    .set({ username: params.username })
                    .where('login = :login', { login: context.login })
                    .execute();
                if (ret.affected === 1) return true;
            }
        }
        return false;
    }

    @Post('avatar/upload')
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                // destination folder must be added to list of folder excluded in tsconfig.build.json to prevent nestjs to freeze on 'File change detected. Starting incremental compilation...""
                destination: './avatars',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        }),
    )
    async uploadAvatar(@UploadedFile() file: Express.Multer.File) {
        let filePath: string | null = null;
        if (file === undefined)
            return {
                status: '406',
                error: 'Extension not accepted, only jpg, jpeg and png!',
                filePath: filePath,
            };
        // const userId = await this.usersService.findIdByLogin(context.login);
        // if ((await this.usersService.storeAvatarFilename(file.filename, userId)) === false)
        //     return { status: '500', error: 'There was an issue when saving file in database!', filePath: filePath };
        filePath = this.usersService.getUserAvatarFilePath(file.filename);
        return {
            status: '200',
            error: null,
            filePath: filePath,
        };
    }

    @Post('avatar/store/:filename')
    async storeAvatarInDb(@Context() context: ContextType, @Param() params: { filename: string }) {
        const userId = await this.usersService.findIdByLogin(context.login);
        const avatar = await this.usersService.findAvatarById(userId);
        if (avatar !== null) await this.usersService.deleteAvatarFromFolder(avatar.split('/').pop());
        // not usefull until figuring out how to move files with fs
        // if (this.usersService.isFileInFolder(params.filename, 'available'))
        //     await this.usersService.deleteAvatarFromFolder(params.filename, 'available');
        if ((await this.usersService.storeAvatarFilename(params.filename, userId)) === false)
            return { status: '500', error: 'There was an issue when saving file in database!' };
        return {
            status: '200',
            error: null,
        };
    }

    @Post('avatar/delete/:filename')
    async deleteUploadedAvatar(@Param() params: { filename: string }) {
        return this.usersService.deleteAvatarFromFolder(params.filename);
    }

    @Post('avatar/store/:filename')
    async storeAvatar(@Context() context: ContextType, @Param() params: { filename: string }) {
        const userId = await this.usersService.findIdByLogin(context.login);
        const avatar = await this.usersService.findAvatarById(userId);
        if (avatar !== null) {
            try {
                await this.usersService.deleteAvatarFromFolder(avatar);
                await this.usersService.storeAvatarFilename(params.filename, userId);
                return { status: '200', msg: `successfully changed avatar}` };
            } catch (error) {
                return { status: '500', msg: `there was an issue when deleting old avatar or storing new one` };
            }
        }
    }

    @Get('/:userId')
    async getUserById(@Param() params: { userId: string }): Promise<UserFromDBDto | null> {
        try {
            const user = await this.usersService.findById(parseInt(params.userId));
            return user;
        } catch (e) {
            return null;
        }
    }

    @Get('id/:login')
    async getUserIdByLogin(@Param() params: { login: string }): Promise<number | null> {
        try {
            const user = await this.usersService.findByLoginInDB(params.login);
            return user !== null ? user.id : null;
        } catch (e) {
            return null;
        }
    }

    @Get('isActive/:id')
    async isUserActive(@Param() params: { id: number }): Promise<boolean> {
        return await this.usersService.userIsActive(params.id);
    }

    @Get('isPlaying/:id')
    async isUserPlaying(@Param() params: { id: number }): Promise<boolean> {
        return await this.usersService.userIsPlaying(params.id);
    }

    @Get('login/:id')
    async getUserLoginById(@Param() params: { id: number }): Promise<string> {
        return await this.usersService.findLoginById(params.id);
    }

    @Get('login/username/:username')
    async getUserLoginByUsername(@Param() params: { username: string }): Promise<string> {
        return await this.usersService.findLoginByUsername(params.username);
    }

    @Get('profile/:login')
    async getUserProfile(@Param() params: { login: string }): Promise<UserProfile> {
        try {
            const userFromDB = await this.usersService.findByLoginInDB(params.login);
            const scores = await this.usersService.findUserScores(userFromDB.id);
            const rank = await this.usersService.findRank(userFromDB.id);
            const isActive = await this.usersService.userIsActive(userFromDB.id);
            const isPlaying = await this.usersService.userIsPlaying(userFromDB.id);
            const userProfile: UserProfile = {
                id: userFromDB.id,
                login: userFromDB.login,
                username: userFromDB.username,
                avatar: userFromDB.avatar,
                isActive,
                isPlaying,
                victories: scores.victories,
                losses: scores.losses,
                rank,
                gamesPlayed: scores.victories + scores.losses + scores.equalities,
                ballHits: 0,
            };
            return userProfile;
        } catch (e) {
            this.logger.error(e);
            throw new HttpException("User doesn't exist", HttpStatus.MISDIRECTED);
        }
    }

    @Get('match-history/:userId')
    async getMatchHistory(@Param() params: { userId: number }): Promise<MatchResults[] | null> {
        const games = await this.gameService.findGamesForUser(params.userId);
        if (games !== null) {
            const gamesResults: MatchResults[] = await Promise.all(
                games.map(async (game) => {
                    const ownScore = game.player1Id == params.userId ? game.score1 : game.score2;
                    const opponentScore = game.player1Id == params.userId ? game.score2 : game.score1;
                    const opponentId = game.player1Id == params.userId ? game.player2Id : game.player1Id;
                    const opponentAvatar = await this.usersService.findAvatarById(opponentId);
                    const opponentUsername = await this.usersService.findUsernameById(opponentId);
                    return { ownScore, opponentScore, opponentUsername, opponentAvatar };
                }),
            );
            return gamesResults;
        }
        return null;
    }

    @Get('/total-points/:id')
    async getTotalPointsScored(@Param() params: { id: number }): Promise<number> {
        const rawResult = await this.entityManager
            .query(`with s1 as (select coalesce(sum("score1"), 0) as sum1 from games where "player1Id"=${params.id}),
            s2 as (select coalesce(sum("score2"), 0) as sum2 from games where "player2Id"=${params.id})
            select ("sum1" + "sum2") as "result" from s1, s2`);
        const penalties = await this.entityManager.query(`
        select count(*) from games where ("player1Id"=${params.id} and score1=-1) or ("player2Id"=${params.id} and score2=-1)`);
        return rawResult[0].result !== null ? parseInt(rawResult[0].result) + parseInt(penalties[0].count) : 0;
    }

    @Get('total-games/:id')
    async getTotalGamesPlayed(@Param() params: { id: number }): Promise<number> {
        const rawResult = await this.gameService.findGamesForUser(params.id);
        return rawResult !== null ? rawResult.length : 0;
    }

    @Get(`/current-game/:userId`)
    async getCurrentGame(@Param() params: { userId: string }): Promise<number> {
        const games = await this.usersService.getCurrentGames(parseInt(params.userId));
        if (games?.length > 1) throw new Error(`${params.userId} seems to play simultaneous games`);
        return games?.[0]?.id;
    }
}
