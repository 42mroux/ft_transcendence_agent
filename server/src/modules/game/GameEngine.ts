import {
    Attendee,
    BodyTypesEnum,
    ExtraTypesEnum,
    gameConfig,
    gameConfigBase,
    GameLevel,
    GameMap,
    GameSettings,
    GameStatus,
    GameUpdate,
    MoveDirection,
    Role,
    TickMessage,
} from 'src/common/types/game.globals.types';

import * as CANNON from 'cannon-es';
import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { GameService } from './game.service';
import { Box, Sphere } from 'cannon-es';
import { GameMode } from './game.types';

interface InitGameProps {
    gameId: number;
    settings?: GameSettings;
    mode?: GameMode;
    timeBetweenTicksInMillisec?: number;
    height?: number;
    width?: number;
}
@Injectable()
export class GameEngine {
    // a "tick" is an update of the game engine sent to the client

    private logger: Logger;

    settings: GameSettings;
    world: CANNON.World;
    attendees: Map<number, Attendee>;
    shapes: { [key: string]: CANNON.Shape };
    materials: { [key: string]: CANNON.Material };
    bodies: { [key: string]: CANNON.Body };
    extras: { body: CANNON.Body; type: ExtraTypesEnum }[];
    intervalTimer: NodeJS.Timer;
    gameId: number;
    timeBetweenTicks: number;
    height: number;
    width: number;
    status: GameStatus;
    turn: Role;
    score: Map<Role, number>;
    mode: GameMode;
    bodiesToRemove: CANNON.Body[];

    extraEffects = {
        [ExtraTypesEnum.GRAVITY]: () => {
            this.world.gravity.y = -15;
            return () => {
                this.world.gravity.y = 0;
            };
        },
        [ExtraTypesEnum.SPEED_UP]: () => {
            this.bodies[BodyTypesEnum.BALL].velocity.x *= 2;
            return () => {
                return;
            };
        },
    };

    launchEffect = (type: ExtraTypesEnum, timeout: number) => {
        const timeoutAction = this.extraEffects[type]();
        setTimeout(() => timeoutAction(), timeout);
    };

    constructor(
        @Inject(forwardRef(() => GameService))
        private readonly gameService: GameService,
    ) {}
    init({
        gameId,
        settings = { level: GameLevel.EASY, map: GameMap.OUTDOOR, powerups: false },
        mode = GameMode.PLAY,
        timeBetweenTicksInMillisec = 10,
        height = gameConfigBase.height,
        width = gameConfigBase.height * gameConfigBase.displayRatio,
    }: InitGameProps) {
        this.logger = new Logger(GameEngine.name);
        this.settings = settings;
        this.gameId = gameId;
        this.mode = mode;
        this.timeBetweenTicks = timeBetweenTicksInMillisec;
        this.height = height;
        this.width = width;
        this.bodies = {};
        this.shapes = {};
        this.materials = {};
        this.extras = [];
        this.bodiesToRemove = [];
        this.turn = Role.PLAYER_ONE;
        this.score = new Map<Role, number>([
            [Role.PLAYER_ONE, 0],
            [Role.PLAYER_TWO, 0],
        ]);
        this.attendees = new Map<number, Attendee>();
        this.status = GameStatus.WAITING_FOR_PLAYER;
        this.world = new CANNON.World({
            gravity: new CANNON.Vec3(0, 0, 0), // m/s²
        });
        this.initCannon();
    }
    initCannon() {
        this.world.broadphase = new CANNON.NaiveBroadphase();

        const plankMass = 10;
        const ballMass = 10;

        const plankDamping = gameConfig[this.settings.level].linearDamping;
        const ballDamping = 0;

        const {
            initBodies: { ball, plank1, plank2, floor, ceil },
        } = gameConfig[this.settings.level];
        this.shapes[BodyTypesEnum.PLANK1] = new CANNON.Box(
            new CANNON.Vec3(plank1.dimension.w / 2, plank1.dimension.h / 2, plank1.dimension.d / 2),
        );
        this.shapes[BodyTypesEnum.PLANK2] = new CANNON.Box(
            new CANNON.Vec3(plank2.dimension.w / 2, plank2.dimension.h / 2, plank2.dimension.d / 2),
        );
        this.shapes[BodyTypesEnum.BALL] = new CANNON.Sphere(1);
        this.shapes[BodyTypesEnum.FLOOR] = new CANNON.Plane();
        this.shapes[BodyTypesEnum.CEIL] = this.shapes[BodyTypesEnum.FLOOR];
        this.materials[BodyTypesEnum.PLANK1] = new CANNON.Material();
        this.materials[BodyTypesEnum.PLANK2] = this.materials[BodyTypesEnum.PLANK1];
        this.materials[BodyTypesEnum.BALL] = new CANNON.Material();
        this.materials[BodyTypesEnum.FLOOR] = new CANNON.Material();
        this.materials[BodyTypesEnum.CEIL] = this.materials[BodyTypesEnum.FLOOR];

        const plank1Body = new CANNON.Body({
            mass: plankMass,
            shape: this.shapes[BodyTypesEnum.PLANK1],
            material: this.materials[BodyTypesEnum.PLANK1],
            position: new CANNON.Vec3(plank1.position.x, plank1.position.y, plank1.position.z),
            linearDamping: plankDamping,
            linearFactor: new CANNON.Vec3(0, 1, 0),
            angularFactor: new CANNON.Vec3(0, 0, 0),
            // type: CANNON.BODY_TYPES.KINEMATIC,
        });

        const plank2Body = new CANNON.Body({
            mass: plankMass,
            shape: this.shapes[BodyTypesEnum.PLANK2],
            material: this.materials[BodyTypesEnum.PLANK2],
            position: new CANNON.Vec3(plank2.position.x, plank2.position.y, plank2.position.z),
            linearDamping: plankDamping,
            linearFactor: new CANNON.Vec3(0, 1, 0),
            angularFactor: new CANNON.Vec3(0, 0, 0),
            // type: CANNON.BODY_TYPES.KINEMATIC,
        });

        const floorBody = new CANNON.Body({
            type: CANNON.Body.STATIC,
            shape: this.shapes[BodyTypesEnum.FLOOR],
            material: this.materials[BodyTypesEnum.FLOOR],
            position: new CANNON.Vec3(floor.position.x, floor.position.y, floor.position.z),
        });
        floorBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2); // make it face up

        const ceilBody = new CANNON.Body({
            type: CANNON.Body.STATIC,
            shape: this.shapes[BodyTypesEnum.CEIL],
            material: this.materials[BodyTypesEnum.CEIL],
            position: new CANNON.Vec3(ceil.position.x, ceil.position.y, ceil.position.z),
        });
        ceilBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), Math.PI / 2); // make it face up

        const wallBody = new CANNON.Body({
            type: CANNON.Body.STATIC,
            shape: this.shapes[BodyTypesEnum.FLOOR],
            material: this.materials[BodyTypesEnum.FLOOR],
            position: new CANNON.Vec3(plank1.position.x, 0, ceil.position.z),
        });
        wallBody.quaternion.setFromAxisAngle(new CANNON.Vec3(0, 1, 0), Math.PI / 2);

        const ballBody = new CANNON.Body({
            mass: ballMass,
            shape: this.shapes[BodyTypesEnum.BALL],
            material: this.materials[BodyTypesEnum.BALL],
            position: new CANNON.Vec3(ball.position.x, ball.position.y, ball.position.z),
            velocity: new CANNON.Vec3(ball.velocity.x, ball.velocity.y, ball.velocity.z),
            linearDamping: ballDamping,
            linearFactor: new CANNON.Vec3(1, 1, 0),
            angularFactor: new CANNON.Vec3(0, 0, 1),
        });

        this.world.addBody(floorBody);
        this.world.addBody(ceilBody);
        if (this.mode == GameMode.TRAINING) this.world.addBody(wallBody);
        else this.world.addBody(plank1Body);
        this.world.addBody(plank2Body);
        this.world.addBody(ballBody);

        this.bodies[BodyTypesEnum.FLOOR] = floorBody;
        this.bodies[BodyTypesEnum.CEIL] = ceilBody;
        this.bodies[BodyTypesEnum.WALL] = wallBody;
        this.bodies[BodyTypesEnum.PLANK1] = plank1Body;
        this.bodies[BodyTypesEnum.PLANK2] = plank2Body;
        this.bodies[BodyTypesEnum.BALL] = ballBody;

        this.world.addContactMaterial(
            new CANNON.ContactMaterial(this.materials[BodyTypesEnum.FLOOR], this.materials[BodyTypesEnum.PLANK1], {
                friction: 0.0,
                restitution: 0.5,
            }),
        );
        this.world.addContactMaterial(
            new CANNON.ContactMaterial(this.materials[BodyTypesEnum.FLOOR], this.materials[BodyTypesEnum.BALL], {
                friction: 0.0,
                restitution: 1,
            }),
        );

        // Custom contact between planks and ball
        plank1Body.addEventListener(
            'collide',
            (event: { body: CANNON.Body; contact: CANNON.ContactEquation; target: CANNON.Body }) => {
                const { body, target } = event;
                if (body.id !== ballBody.id) return;
                const offset = body.position.y - target.position.y;
                body.velocity.set(
                    -body.velocity.x,
                    body.velocity.y + gameConfig[this.settings.level].plankCurveCoeff * offset,
                    body.velocity.z,
                );
            },
        );
        plank2Body.addEventListener(
            'collide',
            (event: { body: CANNON.Body; contact: CANNON.ContactEquation; target: CANNON.Body }) => {
                const { body, target } = event;
                if (body.id !== ballBody.id) return;
                const offset = body.position.y - target.position.y;
                body.velocity.set(
                    -body.velocity.x,
                    body.velocity.y + gameConfig[this.settings.level].plankCurveCoeff * offset,
                    body.velocity.z,
                );
            },
        );
    }

    addExtra(type: ExtraTypesEnum, timeout = 5000) {
        if (this.extras.length > 0) return;
        let hasCollideWithBall = false;
        const {
            initBodies: { extra },
        } = gameConfig[this.settings.level];

        const extraMass = 10;
        this.shapes[BodyTypesEnum.EXTRA] = new CANNON.Box(
            new CANNON.Vec3(extra.dimension.w / 2, extra.dimension.h / 2, extra.dimension.d / 2),
        );
        this.materials[BodyTypesEnum.EXTRA] = new CANNON.Material();
        const extraBody = new CANNON.Body({
            mass: extraMass,
            shape: this.shapes[BodyTypesEnum.EXTRA],
            material: this.materials[BodyTypesEnum.EXTRA],
            position: new CANNON.Vec3(extra.position.x, extra.position.y, extra.position.z),
            angularVelocity: new CANNON.Vec3(extra.angularVelocity.x, extra.angularVelocity.y, extra.angularVelocity.z),
            velocity: new CANNON.Vec3(extra.velocity.x, extra.velocity.y, extra.velocity.z),
        });
        extraBody.collisionResponse = false;
        this.world.addBody(extraBody);
        this.extras.push({ body: extraBody, type });
        extraBody.addEventListener(
            'collide',
            (event: { body: CANNON.Body; contact: CANNON.ContactEquation; target: CANNON.Body }) => {
                if (hasCollideWithBall) return;
                const { body, target } = event;
                if (body.id == this.bodies[BodyTypesEnum.BALL].id) {
                    hasCollideWithBall = true;
                    this.launchEffect(type, timeout);
                    this.bodiesToRemove.push(extraBody);
                } else if (
                    body.id == this.bodies[BodyTypesEnum.FLOOR].id ||
                    body.id == this.bodies[BodyTypesEnum.CEIL].id
                ) {
                    target.velocity.set(target.velocity.x, -target.velocity.y, 0);
                }
            },
        );
    }
    removeBodies() {
        while (this.bodiesToRemove.length > 0) {
            const body = this.bodiesToRemove.pop();
            this.extras = this.extras.filter((extra) => extra.body !== body);
            this.world.removeBody(body);
        }
    }
    step(stepInterval?: number) {
        this.removeBodies();
        if (stepInterval) this.world.step(stepInterval);
        else this.world.fixedStep();
        this.handleScore();
        const tickMessage = this.makeTickMessage();
        this.gameService.sendTick(this.gameId, tickMessage);
        return tickMessage;
    }
    start() {
        this.gameService.sendGameUpdate(this.gameId, this.getGameUpdate());
        this.intervalTimer = setInterval(() => {
            this.step();
        }, this.timeBetweenTicks);
    }
    stop() {
        clearInterval(this.intervalTimer);
        this.status = GameStatus.ENDED;
    }

    getRole(userId: number) {
        return this.attendees.get(userId)?.role;
    }

    getGameUpdate(): GameUpdate {
        return {
            id: this.gameId,
            player1: this.getAttendeeFromRole(Role.PLAYER_ONE),
            player2: this.getAttendeeFromRole(Role.PLAYER_TWO),
            score1: this.score.get(Role.PLAYER_ONE),
            score2: this.score.get(Role.PLAYER_TWO),
            status: this.status,
        };
    }

    getStatus(): GameStatus {
        return this.status;
    }
    getAttendeeFromRole(role: Role) {
        for (const attendee of this.attendees.values()) {
            if (attendee.role == role) return attendee;
        }
        return null;
    }

    getAttendeeFromId(userId: number) {
        for (const attendee of this.attendees.values()) {
            if (attendee.userId == userId) return attendee;
        }
        return null;
    }

    isGameReady() {
        return this.mode == GameMode.PLAY
            ? this.getAttendeeFromRole(Role.PLAYER_ONE) !== null && this.getAttendeeFromRole(Role.PLAYER_TWO) != null
            : this.getAttendeeFromRole(Role.PLAYER_TWO) != null;
    }

    async addTrainer(userId: number) {
        this.attendees.set(userId, {
            userId,
            role: Role.PLAYER_TWO,
        });
        this.status = this.isGameReady() ? GameStatus.READY : GameStatus.WAITING_FOR_PLAYER;
        this.gameService.sendGameUpdate(this.gameId, this.getGameUpdate());
        this.gameService.saveGame(this.getGameUpdate());
    }

    async addAttendee(userId: number): Promise<Attendee> {
        let newAttendee: Attendee;
        if (this.status == GameStatus.WAITING_FOR_PLAYER && this.mode == GameMode.PLAY) {
            newAttendee = {
                userId,
                role: this.getAttendeeFromRole(Role.PLAYER_ONE) == null ? Role.PLAYER_ONE : Role.PLAYER_TWO,
            };
            this.attendees.set(userId, newAttendee);
            this.status = this.isGameReady() ? GameStatus.READY : GameStatus.WAITING_FOR_PLAYER;
        } else {
            newAttendee = { userId, role: Role.VIEWER };
            this.attendees.set(userId, newAttendee);
        }
        await this.gameService.saveGame(this.getGameUpdate());
        await this.gameService.sendGameUpdate(this.gameId, this.getGameUpdate());
        this.gameService.sendAttendeeEvent(this.gameId, 'arrived', newAttendee);
        return newAttendee;
    }

    async removeAttendee(userId: number): Promise<Attendee> {
        const oldGameUpdate = this.getGameUpdate();
        const leftAttendee = this.attendees.get(userId);
        if (!leftAttendee) throw new Error(`User ${userId} is not an attendee of game ${this.gameId}`);
        if (leftAttendee.role == Role.VIEWER) {
            this.gameService.sendAttendeeEvent(this.gameId, 'left', leftAttendee);
        } else {
            this.score.set(leftAttendee.role, -1);
            this.stop();
        }
        this.attendees.delete(userId);
        const newGameUpdate = this.getGameUpdate();
        const gameUpdate = { ...newGameUpdate, player1: oldGameUpdate.player1, player2: oldGameUpdate.player2 };
        await this.gameService.sendGameUpdate(this.gameId, gameUpdate);
        await this.gameService.saveGame(gameUpdate);
        return leftAttendee;
    }

    makeTickMessage(): TickMessage {
        return {
            bodies: [
                {
                    type: BodyTypesEnum.FLOOR,
                    position: this.bodies[BodyTypesEnum.FLOOR].position.toArray(),
                },
                {
                    type: BodyTypesEnum.CEIL,
                    position: this.bodies[BodyTypesEnum.CEIL].position.toArray(),
                },
                {
                    type: BodyTypesEnum.PLANK1,
                    position: this.bodies[BodyTypesEnum.PLANK1].position.toArray(),
                    velocity: this.bodies[BodyTypesEnum.PLANK1].velocity.toArray(),
                    scale: (this.bodies[BodyTypesEnum.PLANK1].shapes[0] as Box).halfExtents
                        .toArray()
                        .map((halfExtend) => halfExtend * 2) as [number, number, number],
                    quaternion: this.bodies[BodyTypesEnum.PLANK1].quaternion.toArray(),
                },
                {
                    type: BodyTypesEnum.PLANK2,
                    position: this.bodies[BodyTypesEnum.PLANK2].position.toArray(),
                    velocity: this.bodies[BodyTypesEnum.PLANK2].velocity.toArray(),
                    scale: (this.bodies[BodyTypesEnum.PLANK2].shapes[0] as Box).halfExtents
                        .toArray()
                        .map((halfExtend) => halfExtend * 2) as [number, number, number],
                    quaternion: this.bodies[BodyTypesEnum.PLANK2].quaternion.toArray(),
                },
                {
                    type: BodyTypesEnum.BALL,
                    position: this.bodies[BodyTypesEnum.BALL].position.toArray(),
                    velocity: this.bodies[BodyTypesEnum.BALL].velocity.toArray(),
                    scale: [
                        (this.bodies[BodyTypesEnum.BALL].shapes[0] as Sphere).radius,
                        (this.bodies[BodyTypesEnum.BALL].shapes[0] as Sphere).radius,
                        (this.bodies[BodyTypesEnum.BALL].shapes[0] as Sphere).radius,
                    ],
                    quaternion: this.bodies[BodyTypesEnum.BALL].quaternion.toArray(),
                },
            ],
            extras: this.extras.map((extra) => ({
                type: extra.type,
                position: extra.body.position.toArray(),
                quaternion: extra.body.quaternion.toArray(),
            })),
        };
    }

    getScore() {
        return [this.score.get(Role.PLAYER_ONE), this.score.get(Role.PLAYER_TWO)];
    }

    getSettings() {
        return this.settings;
    }

    movePlayer(userId: number, direction: MoveDirection) {
        const role = this.attendees.get(userId)?.role;
        if (role == Role.VIEWER) return;
        this.bodies[role == Role.PLAYER_ONE ? BodyTypesEnum.PLANK1 : BodyTypesEnum.PLANK2].velocity.y +=
            direction === MoveDirection.NORTH
                ? gameConfig[this.settings.level].plankSpeed.y
                : -gameConfig[this.settings.level].plankSpeed.y;
    }

    launchBall(userId: number) {
        const role = this.attendees.get(userId)?.role;
        if (this.status === GameStatus.READY && (role == this.turn || this.mode == GameMode.TRAINING)) {
            this.bodies[BodyTypesEnum.BALL].velocity = new CANNON.Vec3(
                gameConfig[this.settings.level].ballSpeed.x * (role == Role.PLAYER_ONE ? 1.0 : -1.0),
                gameConfig[this.settings.level].ballSpeed.y,
                gameConfig[this.settings.level].ballSpeed.z,
            );
            this.status = GameStatus.PLAYING;
            this.turn = this.turn == Role.PLAYER_ONE ? Role.PLAYER_TWO : Role.PLAYER_ONE;
            this.gameService.sendGameUpdate(this.gameId, this.getGameUpdate());
            this.gameService.saveGame(this.getGameUpdate());
        }
    }

    async handleScore() {
        if (this.bodies[BodyTypesEnum.BALL].position.x <= -this.width / 2)
            this.score.set(Role.PLAYER_TWO, this.score.get(Role.PLAYER_TWO) + 1);
        else if (this.bodies[BodyTypesEnum.BALL].position.x >= this.width / 2)
            this.score.set(Role.PLAYER_ONE, this.score.get(Role.PLAYER_ONE) + 1);
        else return;
        this.bodies[BodyTypesEnum.BALL].velocity = new CANNON.Vec3(0, 0, 0);
        const { x, y, z } = gameConfig[this.settings.level].initBodies.ball.position;
        this.bodies[BodyTypesEnum.BALL].position = new CANNON.Vec3(x, y, z);
        if (this.settings.powerups && this.score.get(Role.PLAYER_ONE) % 2 == 1)
            this.addExtra(this.score.get(Role.PLAYER_ONE) % 4 == 1 ? ExtraTypesEnum.GRAVITY : ExtraTypesEnum.SPEED_UP);
        if (this.score.get(Role.PLAYER_ONE) >= 5 || this.score.get(Role.PLAYER_TWO) >= 5) {
            this.stop();
            await this.gameService.sendGameUpdate(this.gameId, this.getGameUpdate());
            await this.gameService.saveGame(this.getGameUpdate());
            await this.gameService.stopGame(this.gameId);
            return;
        }
        this.status = GameStatus.READY;
        await this.gameService.sendGameUpdate(this.gameId, this.getGameUpdate());
        await this.gameService.saveGame(this.getGameUpdate());
    }
}
