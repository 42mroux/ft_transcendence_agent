import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { GameSettings, GameStatus, MoveDirection } from 'src/common/types/game.globals.types';
import { UsersService } from '../user/user.service';
import { GameService } from './game.service';
import { GameEntity } from 'src/infra/typeorm/entities/game.entity';
import { UserFromDBDto } from 'src/common/dto/user/user.dto.out';

@Controller('games')
export class GameController {
    constructor(private readonly gameService: GameService, private readonly usersService: UsersService) {}

    @Post('step')
    async step(@Body() body: { gameId: string; direction: MoveDirection; stepInterval: number }) {
        const { gameId, direction, stepInterval } = body;
        if (!!direction || direction == 0) this.gameService.move(parseInt(gameId), 0, direction);
        const tickMessage = this.gameService.step(parseInt(gameId), stepInterval);
        const score = this.gameService.getScore(parseInt(gameId));
        return {
            obs: tickMessage?.bodies,
            reward: score?.[0] == 1 ? -100 : +1,
            done: score?.[0] == 1,
        };
    }

    @Get('')
    async getGameList(): Promise<GameEntity[]> {
        const gameList = await this.gameService.getCurrentGames();
        return gameList;
    }
    @Get(':id')
    async getGameUpdate(@Param() params: { id: string }): Promise<{ score: number[]; status: GameStatus }> {
        const status = await this.gameService.getGameUpdate(parseInt(params.id))?.status;
        const score = await this.gameService.getScore(parseInt(params.id));
        return { score, status };
    }
    @Get(':id/users')
    async getGameAttendees(@Param() params: { id: string }): Promise<UserFromDBDto[]> {
        const users = await this.gameService.getAttendees(parseInt(params?.id));
        return users;
    }
    @Get(':id/players')
    async getPlayers(@Param() params: { id: string }): Promise<UserFromDBDto[]> {
        const users = await this.gameService.getPlayers(parseInt(params?.id));
        return users;
    }
    @Get(':id/settings')
    async getSettings(@Param() params: { id: string }): Promise<GameSettings> {
        const settings = await this.gameService.getSettings(parseInt(params?.id));
        return settings;
    }
}
