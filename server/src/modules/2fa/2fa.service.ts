import { Injectable, Logger } from '@nestjs/common';
import { authenticator } from 'otplib';
import { UserSecretCodeDto } from 'src/common/dto/2fa/2fa.dto.in';
import { UserEntity } from 'src/infra/typeorm/entities/user.entity';

@Injectable()
export class TwoFactorsAuthenticationService {
    private readonly logger = new Logger(TwoFactorsAuthenticationService.name);

    async generate2FASecret(login: string): Promise<UserSecretCodeDto> {
        const secret = authenticator.generateSecret();
        const otpauthUrl = authenticator.keyuri(login, 'Transcendence', secret);
        return { otpauthUrl, secret };
    }

    async is2FACodeValid(twoFactorAuthenticationCode: string, user: UserEntity): Promise<boolean> {
        const token = twoFactorAuthenticationCode;
        const secret = user.twoFactorAuthenticationSecret;
        return authenticator.verify({
            token,
            secret,
        });
    }
}
