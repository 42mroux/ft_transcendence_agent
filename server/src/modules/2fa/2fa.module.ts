import { Module } from '@nestjs/common';
import { TwoFactorsAuthenticationService } from './2fa.service';
import { TwoFactorsAuthenticationController } from './2fa.controller';
import { UsersModule } from '../user/user.module';

@Module({
    controllers: [TwoFactorsAuthenticationController],
    providers: [TwoFactorsAuthenticationService],
    imports: [UsersModule],
})
export class TwoFactorsAuthenticationModule {}
