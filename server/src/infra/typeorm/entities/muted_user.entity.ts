import { IsNumber } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('muted_users')
export class MutedUserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    userId: number;

    @Column()
    @IsNumber()
    roomId: number;

    @Column({ type: 'timestamp' })
    time: Date;
}
