import { IsNumber, IsString } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('games')
export class GameEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    player1Id: number;

    @Column()
    @IsNumber()
    player2Id: number;

    @Column()
    @IsNumber()
    score1: number;

    @Column()
    @IsNumber()
    score2: number;

    @Column()
    @IsString()
    status: string;
}
