import { IsNumber, IsString } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('messages')
export class MessageEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    senderId: number;

    @Column()
    @IsNumber()
    roomId: number;

    @Column()
    @IsString()
    time: string;

    @Column()
    @IsString()
    content: string;
}
