import { IsString, MaxLength } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsString()
    login: string;

    @Column()
    @IsString()
    @MaxLength(20)
    @Column({ nullable: true })
    username: string;

    @Column({ nullable: true })
    twoFactorAuthenticationSecret: string;

    @Column()
    @Column({ nullable: true })
    avatar: string;
}
