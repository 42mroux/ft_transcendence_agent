import { IsNumber } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('scores')
export class ScoresEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    playerId: number;

    @Column()
    @IsNumber()
    victories: number;

    @Column()
    @IsNumber()
    losses: number;

    @Column()
    @IsNumber()
    equalities: number;

    @Column()
    @IsNumber()
    winningRatio: number;
}
