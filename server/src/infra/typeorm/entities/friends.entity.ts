import { IsNumber } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('friends')
export class FriendsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    userId: number;

    @Column()
    @IsNumber()
    associatedFriendUserId: number;
}
