import { IsNumber } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users_x_rooms')
export class UserRoomEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    userId: number;

    @Column()
    @IsNumber()
    roomId: number;
}
