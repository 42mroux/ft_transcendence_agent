import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsNumber } from 'class-validator';

@Entity('banned_users')
export class BannedUserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    userId: number;

    @Column()
    @IsNumber()
    roomId: number;

    @Column({ type: 'timestamp' })
    time: Date;
}
