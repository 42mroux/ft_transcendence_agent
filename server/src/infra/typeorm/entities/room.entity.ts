import { IsNumber, IsString, MaxLength, MinLength } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('rooms')
export class RoomEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsString()
    @MinLength(1)
    @MaxLength(20)
    name: string;

    @Column()
    @IsNumber()
    typeId: number;

    @Column()
    @IsNumber()
    ownerId: number;

    @Column()
    @IsString()
    image: string;

    @Column()
    @IsString()
    @MinLength(1)
    password: string;
}
