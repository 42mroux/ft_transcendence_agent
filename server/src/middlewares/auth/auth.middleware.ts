import { HttpException, HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    private readonly jwtService = new JwtService({ secret: process.env.TRANS_JWT_SECRET });

    async use(req: Request, res: Response, next: NextFunction) {
        const authHeaders = req.headers.authorization;
        if (authHeaders && authHeaders.split('Bearer ')[1]) {
            try {
                const payload = await this.jwtService.verify(authHeaders.split('Bearer ')[1], {
                    secret: process.env.TRANS_JWT_SECRET,
                });
                req.context = { login: payload.login };
                next();
                return;
            } catch (e) {
                throw new HttpException('JWT not valid, request denied', HttpStatus.FORBIDDEN);
            }
        }
        throw new HttpException('JWT inexistant, request denied', HttpStatus.MISDIRECTED);
    }
}
