export enum GameMap {
    OUTDOOR = 'OUTDOOR',
    INDOOR = 'INDOOR',
}

export enum GameLevel {
    EASY = 1,
    MEDIUM = 2,
    HARD = 3,
}

export interface GameSettings {
    level: GameLevel;
    map: GameMap;
    powerups: boolean;
}

export const gameConfigBase = {
    height: 50,
    displayRatio: 2,
    plankCurveCoeff: 2,
    linearDamping: 0.6,
    initBodies: {
        ball: {
            position: { x: 0, y: 20, z: 0 },
            velocity: { x: 0, y: 0, z: 0 },
            quaternion: { i: 0, j: 0, k: 0, w: 0 },
            dimension: { ratio: 1 },
        },
        plank1: {
            position: { x: -30, y: 20, z: 0 },
            velocity: { x: 0, y: 0, z: 0 },
            quaternion: { i: 0, j: 0, k: 0, w: 0 },
            dimension: { w: 1, h: 10, d: 4 },
        },
        plank2: {
            position: { x: 30, y: 20, z: 0 },
            velocity: { x: 0, y: 0, z: 0 },
            quaternion: { i: 0, j: 0, k: 0, w: 0 },
            dimension: { w: 1, h: 10, d: 4 },
        },
        floor: {
            position: { x: 0, y: 0, z: 0 },
            velocity: { x: 0, y: 0, z: 0 },
            quaternion: { i: 0, j: 0, k: 0, w: 0 },
        },
        ceil: {
            position: { x: 0, y: 40, z: 0 },
            velocity: { x: 0, y: 0, z: 0 },
            quaternion: { i: 0, j: 0, k: 0, w: 0 },
        },
        extra: {
            position: { x: 10, y: 20, z: 0 },
            velocity: { x: 0, y: 2, z: 0 },
            quaternion: { i: 0, j: 0, k: 0, w: 0 },
            dimension: { w: 3, h: 3, d: 3 },
            angularVelocity: { x: 2, y: 2, z: 2 },
        },
    },
    cameraTarget: { x: 0, y: 20, z: 0 },
    ballSpeed: { x: 25, y: -30, z: 0 },
    plankSpeed: { x: 0, y: 2, z: 0 },
};

export const gameConfig = {
    [GameLevel.EASY]: {
        ...gameConfigBase,
        ballSpeed: { x: 25, y: -15, z: 0 },
        plankSpeed: { x: 0, y: 2, z: 0 },
    },
    [GameLevel.MEDIUM]: {
        ...gameConfigBase,
        ballSpeed: { x: 25 * 1.5, y: -15 * 1.5, z: 0 * 1.5 },
        plankSpeed: { x: 0, y: 4, z: 0 },
    },
    [GameLevel.HARD]: {
        ...gameConfigBase,
        ballSpeed: { x: 25 * 2, y: -15 * 2, z: 0 * 2 },
        plankSpeed: { x: 0, y: 6, z: 0 },
    },
};

export enum BodyTypesEnum {
    BALL = 'BALL',
    PLANK1 = 'PLANK1',
    PLANK2 = 'PLANK2',
    FLOOR = 'FLOOR',
    CEIL = 'CEIL',
    WALL = 'WALL',
    EXTRA = 'WALL',
}

export enum ExtraTypesEnum {
    SPEED_UP = 'SPEED_UP',
    GRAVITY = 'GRAVITY',
}

export interface BodyInfo {
    position: [number, number, number];
    velocity?: [number, number, number];
    quaternion?: [number, number, number, number];
    scale?: [number, number, number];
    type: BodyTypesEnum | ExtraTypesEnum;
}
export interface TickMessage {
    bodies: BodyInfo[];
    extras: BodyInfo[];
}

export enum MoveDirection {
    NORTH,
    SOUTH,
    EAST,
    WEST,
}

export enum Role {
    'PLAYER_ONE',
    'PLAYER_TWO',
    'VIEWER',
}

export enum GameMessage {
    CREATE_GAME = 'create game',
    JOIN_GAME = 'join game',
    LEAVE_GAME = 'leave game',
    LAUNCH_BALL = 'launch ball',
    MOVE = 'move',
    UPDATE = 'update',
    ATTENDEE_ARRIVED = 'attendee arrived',
    ATTENDEE_LEFT = 'attendee left',
    GAME_TICK = 'game tick',
    SETUP_TRAINING_GAME = 'setupTrainingGame',
    SETUP_AI_TRAINING = 'setupAITraining',
}

export enum GameStatus {
    PLAYING = 'PLAYING',
    READY = 'READY',
    WAITING_FOR_PLAYER = 'WAITING_FOR_PLAYER',
    ENDED = 'ENDED',
}

export interface Attendee {
    userId: number;
    role: Role;
}

export interface GameUpdate {
    id: number;
    player1: Attendee | null;
    player2: Attendee | null;
    score1: number;
    score2: number;
    status: GameStatus;
}
