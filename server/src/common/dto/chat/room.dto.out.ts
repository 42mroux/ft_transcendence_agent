export class RoomDto {
    roomId: number;
    name: string;
    image: string;
}

export class RoomOverviewDto {
    roomId: number;
    type: number;
    name: string;
    image: string;
    usersIds: number[];
    adminsIds: number[];
    ownerId: number;
    mutedIds: number[] | null;
    bannedIds: number[] | null;
}
