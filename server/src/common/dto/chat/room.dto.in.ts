export class CreateRoomDto {
    ownerId: number;
    name: string;
    image: string;
    typeId: number;
    password: string;
}
