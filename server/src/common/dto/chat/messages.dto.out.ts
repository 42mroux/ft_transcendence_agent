import { IsNumber, IsString } from 'class-validator';

export class MessageInConversationDto {
    @IsNumber()
    senderId: number;

    @IsNumber()
    roomId: number;

    @IsString()
    time: string;

    @IsString()
    content: string;
}
