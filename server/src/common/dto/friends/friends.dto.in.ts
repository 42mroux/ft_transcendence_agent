import { IsNumber } from 'class-validator';

export class CreateRelationShipDto {
    @IsNumber()
    userId: number;

    @IsNumber()
    associatedFriendUserId: number;
}
