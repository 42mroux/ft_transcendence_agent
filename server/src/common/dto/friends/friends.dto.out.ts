import { IsNumber } from 'class-validator';

export class FriendsIdsDto {
    @IsNumber()
    userId: number;

    @IsNumber()
    associatedFriendUserId: number;
}
