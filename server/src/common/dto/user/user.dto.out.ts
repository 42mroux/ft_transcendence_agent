export class User {
    id: number;
    login: string;
    username: string;
    avatar: string;
    isActive: boolean;
    isPlaying: boolean;
}

export class UserForAuthContextDto {
    id: number;
    login: string;
    username: string;
    avatar: string;
    blockedUsersId: number[] | null;
}

export class UserFrom42ApiDto {
    login: string;
    firstName: string;
    lastName: string;
    avatar: string;
}

export class UserFromCookieDto {
    login: string;
    username: string;
    avatar: string;
}

export class UserFromDBDto {
    id: number;
    login: string;
    username: string;
    avatar: string;
}

export class UserProfile {
    id: number;
    login: string;
    username: string;
    avatar: string;
    isActive: boolean;
    isPlaying: boolean;
    victories: number;
    losses: number;
    rank: number;
    gamesPlayed: number;
    ballHits: number;
}
