#!/bin/bash


# docker-compose up --build

# export DOCKER_IP=$(ipconfig getifaddr en0)

echo "🔫 Shutting down previous docker-compose first"
docker-compose down 2> /dev/null

echo ""
echo "⛴  Building database container and DNS server, please wait... ⏳"
if ! docker-compose build --no-cache database ; then
    echo -e "💣 🧨 \033[31m Building failed \033[0m"
	exit
fi

echo ""
echo "✨ You're doing great ! Building server now, please wait again... ⏳⏳"
if ! docker-compose build --no-cache server ; then
	echo -e "💣 🧨 \033[31m Building failed \033[0m"
	exit
fi

echo ""
echo "🤩 You're AWESOME ! Final step: building client now, please.... wait ⏳⏳⏳"
if ! docker-compose build --no-cache client ; then
	echo -e "💣 🧨 \033[31m Building failed \033[0m"
	exit
fi

echo ""
echo "🙌 Here we are ! Starting services..."
echo ""
docker-compose up -d

# echo ""
# echo "🚀 Transcendance is reachable at : http://$DOCKER_IP"
