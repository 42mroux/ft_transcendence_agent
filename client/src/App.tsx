import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route, Link, Outlet } from 'react-router-dom';
import Profile from './screens/Profile';
import AuthProvider from './contexts/auth.context';
import RequireAuth from './router/RequireAuth';
import RequireLogOut from './router/RequireLogOut';
import Welcome from './screens/Welcome';
import AppLayout from './screens/AppLayout';
import Friends from './screens/Friends';
import Chat from './screens/Chat';
import GameScreen from './screens/Game/GameScreen';
import FriendsProvider from './contexts/friends.context';
import Settings from './screens/Settings';
import UserError from './screens/UserError';
import ProgressLogin from './screens/ProgressLogin';
import DialogProvider from './contexts/dialog.context';
import Progress from './screens/Progress';
import RequireUsername from './router/RequireUsername';
import GlobalErrorBoundary from './errorBoundaries/GlobalErrorBoundary';
import SnackbarProvider from './contexts/snackbar.context';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from './theme';

function App() {
    return (
        <GlobalErrorBoundary>
            <ThemeProvider theme={theme}>
                <AuthProvider>
                    <BrowserRouter>
                        <SnackbarProvider>
                            <DialogProvider>
                                <Routes>
                                    <Route path="/">
                                        <Route
                                            index
                                            element={
                                                <RequireLogOut>
                                                    <Welcome />
                                                </RequireLogOut>
                                            }
                                        />
                                        <Route
                                            path="/progress"
                                            element={
                                                <RequireLogOut>
                                                    <Progress />
                                                </RequireLogOut>
                                            }
                                        />
                                        {/* TO REMOVED ONLY FOR TESTS */}
                                        <Route
                                            path="/progress-login"
                                            element={
                                                <RequireLogOut>
                                                    <ProgressLogin />
                                                </RequireLogOut>
                                            }
                                        />
                                        <Route
                                            path="/error_page"
                                            element={
                                                <RequireLogOut>
                                                    <UserError />
                                                </RequireLogOut>
                                            }
                                        />
                                        <Route
                                            element={
                                                <FriendsProvider>
                                                    <RequireAuth>
                                                        <AppLayout>
                                                            <Outlet />
                                                        </AppLayout>
                                                    </RequireAuth>
                                                </FriendsProvider>
                                            }
                                        >
                                            <Route
                                                path="profile"
                                                element={
                                                    <RequireUsername>
                                                        <Profile />
                                                    </RequireUsername>
                                                }
                                            />
                                            <Route
                                                path="friends"
                                                element={
                                                    <RequireUsername>
                                                        <Friends />
                                                    </RequireUsername>
                                                }
                                            />
                                            <Route
                                                path="chat"
                                                element={
                                                    <RequireUsername>
                                                        <Chat />
                                                    </RequireUsername>
                                                }
                                            />
                                            <Route path="game">
                                                <Route
                                                    path=":gameId"
                                                    element={
                                                        <RequireUsername>
                                                            <GameScreen />
                                                        </RequireUsername>
                                                    }
                                                />
                                                <Route
                                                    path=""
                                                    element={
                                                        <RequireUsername>
                                                            <GameScreen />
                                                        </RequireUsername>
                                                    }
                                                />
                                            </Route>
                                            <Route path="settings" element={<Settings />} />
                                        </Route>
                                        <Route path="*" element={<NoMatch />} />
                                    </Route>
                                </Routes>
                            </DialogProvider>
                        </SnackbarProvider>
                    </BrowserRouter>
                </AuthProvider>
            </ThemeProvider>
        </GlobalErrorBoundary>
    );
}

function NoMatch() {
    return (
        <div>
            <h2>Nothing to see here!</h2>
            <p>
                <Link to="/">Go to the home page</Link>
            </p>
        </div>
    );
}

export default App;
