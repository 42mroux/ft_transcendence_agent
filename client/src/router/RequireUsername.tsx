import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { AuthContext } from '../contexts/auth.context';

const RequireUsername = ({ children }: { children: JSX.Element }) => {
    const authContext = useContext(AuthContext);

    return authContext.isUserNew ? <Navigate to="/settings" /> : children;
};

export default RequireUsername;
