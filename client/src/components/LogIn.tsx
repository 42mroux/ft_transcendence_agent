import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { is2FAEnabled } from '../api/axios/profileRoutes';
import { AuthContext } from '../contexts/auth.context';

const LogIn = (): JSX.Element => {
    const [login, setLogin] = useState<string>('');
    const auth = useContext(AuthContext);
    const navigate = useNavigate();

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            if (await is2FAEnabled(login)) navigate(`/progress-login?login=${login}`);
            else auth.logInWith42(login);
        } catch (e) {
            navigate('/');
        }
    };

    return (
        <div style={{ padding: 10, flex: 1 }}>
            <form onSubmit={onSubmit}>
                <input
                    type="text"
                    value={login}
                    onChange={(event) => {
                        setLogin(event.target.value);
                    }}
                />
                <button type="submit">Log In</button>
            </form>
        </div>
    );
};

export { LogIn };
