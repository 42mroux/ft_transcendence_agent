import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Typography, Box } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getTotalPointsScored } from '../../../api/axios/profileRoutes';
import SportsScoreRoundedIcon from '@mui/icons-material/SportsScoreRounded';

export interface AchievementProps {
    userId: number;
    open: boolean;
    onClose: () => void;
}

const TotalPointsScoredDialog = (props: AchievementProps): JSX.Element => {
    const [points, setPoints] = useState<number>(0);

    const loadPoints = async () => {
        try {
            const totalPoints = await getTotalPointsScored(props.userId);
            setPoints(totalPoints);
        } catch (e) {
            setPoints(0);
        }
    };

    useEffect(() => {
        loadPoints();
    }, [props.userId]);

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Total number of points scored
            </DialogTitle>
            <DialogContent id="scroll-dialog-list">
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderBlockEnd: '1px solid',
                        borderBlockStart: '1px solid',
                        borderColor: 'lightgray',
                    }}
                >
                    <SportsScoreRoundedIcon sx={{ color: 'gold', width: 100, height: 100, m: 1 }} />
                    <Typography sx={{ fontSize: 150, marginLeft: 5, marginRight: 5 }}>{points}</Typography>
                    <SportsScoreRoundedIcon sx={{ color: 'gold', width: 100, height: 100, m: 1 }} />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    exit
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default TotalPointsScoredDialog;
