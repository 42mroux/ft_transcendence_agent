import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Typography, Box } from '@mui/material';
import React, { useEffect, useState } from 'react';
import StarsRoundedIcon from '@mui/icons-material/StarsRounded';
import { AchievementProps } from './TotalPointsScoredDialog';
import { getTotalGamesPlayed } from '../../../api/axios/profileRoutes';

const GamesPlayedDialog = (props: AchievementProps): JSX.Element => {
    const [games, setGames] = useState<number>(0);

    const loadPoints = async () => {
        try {
            const totalPoints = await getTotalGamesPlayed(props.userId);
            setGames(totalPoints);
        } catch (e) {
            setGames(0);
        }
    };

    useEffect(() => {
        loadPoints();
    }, [props.userId]);

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Total number games played
            </DialogTitle>
            <DialogContent id="scroll-dialog-list">
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderBlockEnd: '1px solid',
                        borderBlockStart: '1px solid',
                        borderColor: 'lightgray',
                    }}
                >
                    <StarsRoundedIcon sx={{ color: 'gold', width: 100, height: 100, m: 1 }} />
                    <Typography sx={{ fontSize: 150, marginLeft: 5, marginRight: 5 }}>{games}</Typography>
                    <StarsRoundedIcon sx={{ color: 'gold', width: 100, height: 100, m: 1 }} />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    exit
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default GamesPlayedDialog;
