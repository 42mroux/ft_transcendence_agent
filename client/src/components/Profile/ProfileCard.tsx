import { Box, Avatar, IconButton, Typography, Card } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../contexts/auth.context';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import MoreVertRoundedIcon from '@mui/icons-material/MoreVertRounded';
import { addNewFriend, isAlreadyFriend, removeFriend } from '../../api/axios/friendsRoutes';
import { OptionType, MenuOptionsDialog } from '../MenuOptionsDialog';
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import ChatRoundedIcon from '@mui/icons-material/ChatRounded';
import PersonRemoveRoundedIcon from '@mui/icons-material/PersonRemoveRounded';
import { useNavigate } from 'react-router-dom';
import AvatarWithOnlineBadge from '../AvatarWithOnlineBadge';
import { blockUser, unblockUser } from '../../api/socketIo/chatMessages';
import MobileFriendlyRoundedIcon from '@mui/icons-material/MobileFriendlyRounded';
import { SnackbarContext } from '../../contexts/snackbar.context';
import { FriendsContext } from '../../contexts/friends.context';

interface ProfileCardProps {
    id: number;
    login: string;
    username: string;
    avatar: string;
    isActive: boolean;
    isPlaying: boolean;
}

const ProfileCard = (props: ProfileCardProps): JSX.Element => {
    const navigate = useNavigate();
    const auth = useContext(AuthContext);
    const [isFriend, setIsFriend] = useState<boolean>(false);
    const [openMenu, setOpenMenu] = useState<boolean>(false);
    const { showSnackbar } = useContext(SnackbarContext);
    const friendsContext = useContext(FriendsContext);

    const basicOptions: OptionType[] = [
        {
            title: 'Unfriend',
            icon: <PersonRemoveRoundedIcon />,
            action: async (login: string): Promise<void> => {
                try {
                    await removeFriend(login);
                    await loadData();
                } catch (e) {
                    showSnackbar('Error while unfriending', 'error');
                }
                handleClose();
            },
        },
    ];
    const [options, setOptions] = useState<OptionType[]>(basicOptions);

    const blockOptions: OptionType[] = [
        {
            title: 'Send message',
            icon: <ChatRoundedIcon />,
            action: (login: string) => {
                navigate(`/chat?friend-login=${login}`);
            },
        },
        {
            title: 'Block user',
            icon: <CancelRoundedIcon />,
            action: async () => {
                blockUser(props.id, auth.refreshBlockedUsersId);
                handleClose();
            },
        },
    ];

    const unblockOption: OptionType = {
        title: 'Unblock user',
        icon: <MobileFriendlyRoundedIcon />,
        action: async () => {
            unblockUser(props.id, auth.refreshBlockedUsersId);
            handleClose();
        },
    };

    useEffect(() => {
        // TODO  could be replaced with auth method
        if (auth.blockedUsersId !== null && auth.blockedUsersId.find((id) => id === props.id) !== undefined)
            setOptions([...basicOptions, unblockOption]);
        else setOptions([...basicOptions, blockOptions[0], blockOptions[1]]);
    }, [auth.blockedUsersId]);

    const loadData = async () => {
        try {
            const alreadyFriend = await isAlreadyFriend(props.login);
            setIsFriend(alreadyFriend);
        } catch (e) {
            setIsFriend(false);
        }
    };

    useEffect(() => {
        loadData();
    }, [friendsContext.friends]);

    const handleClickOpen = () => {
        setOpenMenu(true);
    };

    const handleClose = () => {
        setOpenMenu(false);
    };

    const addFriend = async (): Promise<void> => {
        try {
            await addNewFriend(props.login);
            await loadData();
        } catch (e) {
            showSnackbar('Error while adding friend', 'error');
        }
    };

    return (
        <Box
            sx={{
                flexGrow: 1,
                flex: 1,
                padding: 0.1,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                boxShadow: 3,
                borderRadius: 2,
                backgroundColor: 'white',
                m: 1,
            }}
        >
            {isFriend === false ? (
                <Avatar
                    alt="img"
                    src={props.avatar}
                    sx={{
                        width: 150,
                        height: 150,
                        marginBlockStart: 3,
                        marginBlockEnd: 3,
                        marginRight: 7,
                        marginLeft: 7,
                        borderColor: 'orange',
                    }}
                />
            ) : (
                <AvatarWithOnlineBadge
                    image={props.avatar}
                    online={props.isActive}
                    playing={props.isPlaying}
                    sx={{
                        width: 150,
                        height: 150,
                        marginBlockStart: 3,
                        marginBlockEnd: 3,
                        marginRight: 7,
                        marginLeft: 7,
                        borderColor: 'orange',
                    }}
                />
            )}
            <Card sx={{ display: 'flex', flexDirection: 'row', marginBottom: 1 }}>
                <Typography variant="h4" sx={{ m: 1 }}>
                    {props.username}
                </Typography>
                {props.login !== auth.login &&
                    (isFriend === false ? (
                        <IconButton aria-label="add" onClick={addFriend}>
                            <AddCircleOutlineRoundedIcon color="success" sx={{ height: 40, width: 40 }} />
                        </IconButton>
                    ) : (
                        <div>
                            <IconButton aria-label="profile-menu" onClick={handleClickOpen}>
                                <MoreVertRoundedIcon sx={{ height: 40, width: 40 }} />
                            </IconButton>
                            <MenuOptionsDialog
                                open={openMenu}
                                onClose={handleClose}
                                friendLogin={props.login}
                                options={options}
                            />
                        </div>
                    ))}
            </Card>
        </Box>
    );
};

export default ProfileCard;
