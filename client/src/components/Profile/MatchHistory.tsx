import { Box, Card, styled } from '@mui/material';
import React, { useEffect, useState } from 'react';
import SportsEsportsRoundedIcon from '@mui/icons-material/SportsEsportsRounded';
import { UserProfile } from '../../common/user/dto/user.dto.out';
import { getMatchHistory } from '../../api/axios/profileRoutes';
import { MatchResult } from '../../common/game/dto/game.dto.out';
import GameResultsCard from './GameResultsCard';

const MatchHistory = (props: { userScores: UserProfile | undefined }): JSX.Element => {
    const [matchHistory, setMatchHistory] = useState<MatchResult[]>();

    const loadMatchHistory = async () => {
        try {
            if (props.userScores !== undefined) {
                const newMatchHistory = await getMatchHistory(props.userScores.id);
                if (newMatchHistory !== null) setMatchHistory(newMatchHistory);
            }
        } catch (e) {
            setMatchHistory([]);
        }
    };

    useEffect(() => {
        loadMatchHistory();
    }, [props.userScores]);

    const Div = styled('div')(({ theme }) => ({
        ...theme.typography.button,
        backgroundColor: theme.palette.background.paper,
        color: 'gray',
        fontSize: 20,
        borderBlockEnd: '1px solid',
        borderBlockStart: '1px solid',
        borderColor: 'gray',
        lineHeight: 0.5,
        padding: theme.spacing(1),
    }));

    return (
        <Box
            sx={{
                flexGrow: 1,
                flex: 2,
                padding: 0.1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                borderRadius: 2,
                m: 1,
                backgroundColor: 'white',
                boxShadow: 3,
            }}
        >
            <Card
                sx={{
                    display: 'flex',
                    flex: 1,
                    minWidth: 260,
                    flexGrow: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    minHeight: 50,
                }}
            >
                <SportsEsportsRoundedIcon sx={{ color: 'gray', width: 50, height: 50, m: 1 }} />
                <Div>{'MATCH HISTORY'}</Div>
            </Card>
            <Box
                sx={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 6,
                    flexDirection: 'column',
                    overflowY: 'scroll',
                    overflowX: 'scroll',
                    m: 1,
                    maxHeight: 200,
                }}
            >
                {props.userScores !== undefined &&
                    matchHistory?.map((results, index) => (
                        <GameResultsCard
                            key={index}
                            ownUsername={props.userScores?.username}
                            ownAvatar={props.userScores?.avatar}
                            matchResults={results}
                        />
                    ))}
            </Box>
        </Box>
    );
};

export default MatchHistory;
