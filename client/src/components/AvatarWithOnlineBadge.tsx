import { Avatar, Badge, styled, SxProps, Theme } from '@mui/material';
import React from 'react';

const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
        },
    },
}));

interface AvatarWithOnlineBadgeProps {
    image: string;
    online: boolean;
    playing: boolean;
    sx: SxProps<Theme>;
}

const AvatarWithOnlineBadge = (props: AvatarWithOnlineBadgeProps) => {
    return (
        <StyledBadge
            overlap="circular"
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            variant="dot"
            invisible={!props.online}
        >
            <Avatar
                alt="Room image"
                src={props.image}
                sx={[
                    props.playing && {
                        border: 2,
                        animationName: `glow-playing`,
                        animationDuration: '1s',
                        animationIterationCount: 'infinite',
                        animationDirection: 'alternate',
                        '@keyframes glow-playing': {
                            from: { borderColor: 'transparent' },
                            to: { borderColor: 'orange' },
                        },
                    },
                    ...(Array.isArray(props.sx) ? props.sx : [props.sx]),
                ]}
            />
        </StyledBadge>
    );
};

export default AvatarWithOnlineBadge;
