import { Card, CardActionArea, Typography } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { User } from '../../common/user/dto/user.dto.out';
import AvatarWithOnlineBadge from '../AvatarWithOnlineBadge';
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import AccountBoxRoundedIcon from '@mui/icons-material/AccountBoxRounded';
import ChatRoundedIcon from '@mui/icons-material/ChatRounded';
import PersonRemoveRoundedIcon from '@mui/icons-material/PersonRemoveRounded';
import { OptionType, MenuOptionsDialog } from '../MenuOptionsDialog';
import { useNavigate } from 'react-router-dom';
import { removeFriend } from '../../api/axios/friendsRoutes';
import MobileFriendlyRoundedIcon from '@mui/icons-material/MobileFriendlyRounded';
import { blockUser, unblockUser } from '../../api/socketIo/chatMessages';
import { AuthContext } from '../../contexts/auth.context';

interface FriendInListProps {
    friend: User;
    loadData: () => Promise<void>;
}

const FriendInList = (props: FriendInListProps): JSX.Element => {
    const navigate = useNavigate();
    const auth = useContext(AuthContext);
    const [openOptions, setOpenOptions] = useState<boolean>(false);
    const basicOptions: OptionType[] = [
        {
            title: 'Profile',
            icon: <AccountBoxRoundedIcon />,
            action: async (login: string): Promise<void> => {
                navigate(`/profile?friend-login=${login}`);
            },
        },
        {
            title: 'Unfriend',
            icon: <PersonRemoveRoundedIcon />,
            action: async (login: string): Promise<void> => {
                await removeFriend(login);
                await handleClose();
                await props.loadData();
            },
        },
    ];
    const [options, setOptions] = useState<OptionType[]>(basicOptions);

    const blockOptions: OptionType[] = [
        {
            title: 'Send message',
            icon: <ChatRoundedIcon />,
            action: async (login: string): Promise<void> => {
                navigate(`/chat?friend-login=${login}`);
            },
        },
        {
            title: 'Block user',
            icon: <CancelRoundedIcon />,
            action: async (): Promise<void> => {
                blockUser(props.friend.id, auth.refreshBlockedUsersId);
                await handleClose();
            },
        },
    ];

    const unblockOption: OptionType = {
        title: 'Unblock user',
        icon: <MobileFriendlyRoundedIcon />,
        action: async (): Promise<void> => {
            unblockUser(props.friend.id, auth.refreshBlockedUsersId);
            await handleClose();
        },
    };

    useEffect(() => {
        // TODO could be replaced with auth method
        if (auth.blockedUsersId !== null && auth.blockedUsersId.find((id) => id === props.friend.id) !== undefined)
            setOptions([...basicOptions, unblockOption]);
        else setOptions([...basicOptions, blockOptions[0], blockOptions[1]]);
    }, [auth.blockedUsersId]);

    const handleClickOpen = () => {
        setOpenOptions(true);
    };

    const handleClose = () => {
        setOpenOptions(false);
    };

    return (
        <Card sx={{ display: 'flex', m: 2, flexDirection: 'column', width: 150, maxHeight: 150 }}>
            <CardActionArea
                sx={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    overflow: 'hidden',
                }}
                onClick={handleClickOpen}
            >
                <AvatarWithOnlineBadge
                    image={props.friend.avatar}
                    online={props.friend.isActive}
                    playing={props.friend.isPlaying}
                    sx={{ width: 70, height: 70, m: 1 }}
                />
                <Typography
                    component="div"
                    variant="subtitle1"
                    sx={{
                        maxWidth: '100px',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                    }}
                >
                    {props.friend.username}
                    <br />
                </Typography>
            </CardActionArea>
            <MenuOptionsDialog
                open={openOptions}
                onClose={handleClose}
                friendLogin={props.friend.login}
                options={options}
            />
        </Card>
    );
};

export default FriendInList;
