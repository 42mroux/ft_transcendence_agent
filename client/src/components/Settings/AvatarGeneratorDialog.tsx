import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Avatar, Box, IconButton } from '@mui/material';
import React, { useEffect, useState } from 'react';
import NotificationSnackbar from '../NotificationSnackbar';
import ChangeCircleRoundedIcon from '@mui/icons-material/ChangeCircleRounded';
import { generateRandomAvatar } from '../../api/axios/profileRoutes';

interface AvatarGeneratorDialogProps {
    open: boolean;
    onClose: () => void;
    generatedAvatar: string;
    setGeneratedAvatar: (avatar: string) => void;
}

const AvatarGeneratorDialog = (props: AvatarGeneratorDialogProps): JSX.Element => {
    const [openError, setOpenError] = useState<boolean>(false);
    const [newAvatar, setNewAvatar] = useState<string>('');
    const [submitDisabled, setSubmitDisabled] = useState<boolean>(true);

    useEffect(() => {
        newAvatar === '' ? setSubmitDisabled(true) : setSubmitDisabled(false);
    }, [newAvatar]);

    const generateAvatar = async () => {
        setOpenError(false);
        const avatarFilePath = await generateRandomAvatar();
        if (avatarFilePath === null) setOpenError(true);
        else setNewAvatar(avatarFilePath);
    };

    const handleSubmitAvatar = () => {
        props.setGeneratedAvatar(newAvatar);
        setNewAvatar('');
        props.onClose();
    };

    const handleCancel = () => {
        setNewAvatar('');
        props.onClose();
    };

    return (
        <div>
            <Dialog
                open={props.open}
                onClose={props.onClose}
                scroll="paper"
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-list"
                fullWidth
                maxWidth="sm"
            >
                <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                    Click to generate a random avatar
                </DialogTitle>
                <DialogContent
                    id="scroll-dialog-list"
                    sx={{
                        display: 'flex',
                        flexDirection: 'row',
                    }}
                >
                    <Box sx={{ display: 'flex', justifyContent: 'center', flex: 1, flexGrow: 1, m: 2 }}>
                        <IconButton sx={{ height: '125px', width: '125px' }} onClick={generateAvatar}>
                            {newAvatar !== '' ? (
                                <Avatar alt="img" src={newAvatar} sx={{ height: '100px', width: '100px' }} />
                            ) : (
                                <ChangeCircleRoundedIcon sx={{ height: '100px', width: '100px' }} />
                            )}
                        </IconButton>
                    </Box>
                </DialogContent>
                <DialogActions>
                    {!submitDisabled && (
                        <Button onClick={handleSubmitAvatar} color="success" sx={{ marginRight: 2, marginBottom: 2 }}>
                            I like this one
                        </Button>
                    )}
                    <Button onClick={handleCancel} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                        {newAvatar === '' ? 'Cancel' : 'Naah forget it'}
                    </Button>
                </DialogActions>
            </Dialog>
            <NotificationSnackbar
                open={openError}
                setOpen={setOpenError}
                message="We could not generate an avatar, try again!"
            />
        </div>
    );
};

export default AvatarGeneratorDialog;
