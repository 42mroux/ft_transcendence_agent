import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Box, IconButton, Avatar } from '@mui/material';
import React, { useEffect, useState } from 'react';
import Dropzone from 'react-dropzone';
import FileDownloadRoundedIcon from '@mui/icons-material/FileDownloadRounded';
import { deleteUploadedAvatar, uploadAvatar } from '../../api/axios/profileRoutes';
import NotificationSnackbar from '../NotificationSnackbar';

interface AvatarDropzoneDialogProps {
    open: boolean;
    onClose: () => void;
    uploadedAvatar: string;
    setUploadedAvatar: (avatar: string) => void;
}

const AvatarDropzoneDialog = (props: AvatarDropzoneDialogProps): JSX.Element => {
    const [openError, setOpenError] = useState<boolean>(false);
    const [error, setError] = useState<string>('');
    const [selectedAvatar, setSelectedAvatar] = useState<string>('');
    const [submitDisabled, setSubmitDisabled] = useState<boolean>(true);

    useEffect(() => {
        selectedAvatar === '' ? setSubmitDisabled(true) : setSubmitDisabled(false);
    }, [selectedAvatar]);

    const selectAvatar = async (files: File[]) => {
        if (selectedAvatar !== '') {
            setSelectedAvatar('');
            await deleteUploadedAvatar(selectedAvatar);
        }
        const fileData = new FormData();
        fileData.append('file', files[0]);
        const request = await uploadAvatar(fileData);
        if (request.status !== '200') {
            setError(request.error);
            setOpenError(true);
        } else {
            setSelectedAvatar(request.filePath);
        }
    };

    const handleSubmitAvatar = async () => {
        if (props.uploadedAvatar !== '') {
            await deleteUploadedAvatar(props.uploadedAvatar);
        }
        props.setUploadedAvatar(selectedAvatar);
        setSelectedAvatar('');
        props.onClose();
    };

    const handleRemoveAvatar = async () => {
        if (selectedAvatar !== '') {
            setSelectedAvatar('');
            props.setUploadedAvatar('');
            await deleteUploadedAvatar(selectedAvatar);
        }
        props.onClose();
    };

    return (
        <div>
            <Dialog
                open={props.open}
                onClose={props.onClose}
                scroll="paper"
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-list"
                fullWidth
                maxWidth="sm"
            >
                <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                    Drag and drop your image here or click to select one from your device
                </DialogTitle>
                <DialogContent
                    id="scroll-dialog-list"
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                    }}
                >
                    <Dropzone onDrop={(acceptedFiles) => selectAvatar(acceptedFiles)}>
                        {({ getRootProps, getInputProps }) => (
                            <section>
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <Box sx={{ flex: 1, flexGrow: 1 }}>
                                        <IconButton sx={{ height: '125px', width: '125px' }}>
                                            {selectedAvatar !== '' ? (
                                                <Avatar
                                                    alt="img"
                                                    src={selectedAvatar}
                                                    sx={{ height: '100px', width: '100px' }}
                                                />
                                            ) : (
                                                <FileDownloadRoundedIcon sx={{ height: '100px', width: '100px' }} />
                                            )}
                                        </IconButton>
                                    </Box>
                                </div>
                            </section>
                        )}
                    </Dropzone>
                </DialogContent>
                <DialogActions>
                    {!submitDisabled && (
                        <Button onClick={handleSubmitAvatar} color="success" sx={{ marginRight: 2, marginBottom: 2 }}>
                            I like this one
                        </Button>
                    )}
                    <Button onClick={handleRemoveAvatar} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                        {selectedAvatar === '' ? 'Cancel' : 'Naah remove it'}
                    </Button>
                </DialogActions>
            </Dialog>
            <NotificationSnackbar open={openError} setOpen={setOpenError} message={error} />
        </div>
    );
};

export default AvatarDropzoneDialog;
