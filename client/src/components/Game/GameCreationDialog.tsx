import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel,
    MenuItem,
    Rating,
    Select,
    Typography,
    Box,
    Switch,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { sendGameInvite } from '../../api/socketIo/chatMessages';
import { createGame } from '../../api/socketIo/gameMessages';
import { GameLevel, GameMap } from '../../common/game/types/game.types';

interface GameCreationDialogProp {
    open: boolean;
    onClose: () => void;
    roomId?: number;
}
export const GameCreationDialog = (props: GameCreationDialogProp): JSX.Element => {
    const [level, setLevel] = useState<GameLevel>(1);
    const [map, setMap] = useState<GameMap>(GameMap.OUTDOOR);
    const [powerups, setPowerups] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    const onNewGameCreated = (info: { gameId: number; error?: string }) => {
        try {
            if (info?.error) throw new Error(info.error);
            if (props.roomId) sendGameInvite(props.roomId, info.gameId);
            navigate(`/game/${info.gameId}`, { replace: true });
            props.onClose();
            return;
        } catch (e) {
            setErrorMessage('Error during game creation.');
        }
    };

    const handleCreateGame = () => {
        try {
            createGame({ level, map, powerups }, onNewGameCreated);
        } catch (e) {
            setErrorMessage('Error during game creation.');
        }
    };

    useEffect(() => setErrorMessage(''), [props.open]);
    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2, borderBottom: '1px solid', borderColor: 'lightgray' }}>
                New Game
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    overflowX: 'hidden',
                }}
            >
                <Typography sx={{ m: 1 }}>Level</Typography>
                <Rating
                    max={3}
                    sx={{ marginBlockEnd: 5, marginBlockStart: 1, marginLeft: 1 }}
                    value={level}
                    onChange={(event, newValue) => newValue && setLevel(newValue)}
                />
                <Typography sx={{ m: 1 }}>Scenery</Typography>
                <FormControl fullWidth sx={{ m: 1, marginBlockStart: 2 }}>
                    <InputLabel id="demo-simple-select-label">Map</InputLabel>
                    <Select value={map} label="Map" onChange={(event) => setMap(event.target.value as GameMap)}>
                        <MenuItem value={GameMap.OUTDOOR}>Outdoor</MenuItem>
                        <MenuItem value={GameMap.INDOOR}>Indoor</MenuItem>
                    </Select>
                </FormControl>
                <Typography sx={{ m: 1 }}>Power ups</Typography>
                <Switch checked={powerups} onChange={(event) => setPowerups(event.target.checked)} />
                {errorMessage && <Typography color="error">{errorMessage}</Typography>}
            </DialogContent>
            <DialogActions>
                <Box
                    sx={{
                        m: 2,
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'end',
                        borderTop: '1px solid',
                        borderColor: 'lightgray',
                    }}
                >
                    <Button onClick={handleCreateGame} sx={{ marginRight: 2, marginTop: 2 }}>
                        OK
                    </Button>
                    <Button onClick={props.onClose} color="error" sx={{ marginRight: 2, marginTop: 2 }}>
                        Cancel
                    </Button>
                </Box>
            </DialogActions>
        </Dialog>
    );
};
