import React from 'react';
import { BoxTypes } from '../../common/game/types/game.types';

interface BoxProps {
    type: BoxTypes;
    score: number;
}

const BoardBox = (props: BoxProps): JSX.Element => {
    const playerStyle = {
        height: '35px',
        width: '35px',
        borderStyle: 'solid',
        justifyContent: 'center',
        backgroundColor: 'white',
        color: 'black',
    };

    const ballStyle = {
        height: '35px',
        width: '35px',
        display: 'block',
        backgroundColor: 'white',
        justifyContent: 'center',
        borderRadius: '100%',
        color: 'black',
    };

    const scoreStyle = {
        height: '35px',
        width: '35px',
        display: 'block',
        textAlign: 'center',
        color: 'white',
        fontSize: '30px',
    };

    const getStyle = (type: number) => {
        if (type == BoxTypes.BACKGROUND) return {};
        if (type === BoxTypes.PLAYER) return playerStyle;
        if (type === BoxTypes.BALL) return ballStyle;
        else return scoreStyle;
    };

    return (
        <div
            style={{
                height: '35px',
                width: '35px',
                borderStyle: 'solid',
                justifyContent: 'center',
                backgroundColor: 'black',
                borderRadius: '2px',
            }}
        >
            {props.score >= 0 ? (
                <div style={getStyle(props.type)}>{props.score}</div>
            ) : (
                <div style={getStyle(props.type)} />
            )}
        </div>
    );
};

export default BoardBox;
