import React from 'react';
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    List,
    ListItemButton,
    Typography,
    IconButton,
    Box,
} from '@mui/material';
import { GameResult, GameResultType } from './GameResult';
import RefreshIcon from '@mui/icons-material/Refresh';

interface GameListDialogProps {
    open: boolean;
    games: GameResultType[];
    onClose: () => void;
    onGameClick: (gameId: number) => void;
    currentGameId?: number;
    refetchGames: () => Promise<void>;
}

export const GameListDialog = (props: GameListDialogProps): JSX.Element => {
    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle
                id="scroll-dialog-title"
                sx={{
                    m: 2,
                    flex: 1,
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderBottom: '1px solid',
                    borderColor: 'lightgray',
                }}
            >
                Join a game
                <IconButton aria-label="refetch" onClick={props.refetchGames}>
                    <RefreshIcon color="primary" />
                </IconButton>
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    m: 2,
                    flex: 3,
                    display: 'flex',
                    flexDirection: 'column',
                }}
            >
                <>
                    {props.games?.length > 0 ? (
                        <List>
                            {props.games
                                .filter((game) => game.id != props.currentGameId)
                                .map((game) => (
                                    <ListItemButton key={game.id} onClick={() => props.onGameClick(game.id)}>
                                        <GameResult game={game} />
                                    </ListItemButton>
                                ))}
                        </List>
                    ) : (
                        <Typography>There is no game to join</Typography>
                    )}
                </>
            </DialogContent>
            <DialogActions>
                <Box
                    sx={{
                        m: 2,
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'end',
                        borderTop: '1px solid',
                        borderColor: 'lightgray',
                    }}
                >
                    <Button onClick={props.onClose} color="error" sx={{ marginTop: 2 }}>
                        Cancel
                    </Button>
                </Box>
            </DialogActions>
        </Dialog>
    );
};
