import { Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getUserById } from '../../api/axios/profileRoutes';
import { GameStatus } from '../../common/game/types/game.types';

export interface GameResultType {
    id: number;
    player1Id: number;
    player2Id: number;
    score1: number;
    score2: number;
    status: GameStatus;
}

export const GameResult = ({ game }: { game: GameResultType }): JSX.Element => {
    const [player1Login, setPlayer1Login] = useState('');
    const [player2Login, setPlayer2Login] = useState('');

    useEffect(() => {
        const setUsersInfo = async () => {
            const user1 = await getUserById(game.player1Id);
            const user2 = await getUserById(game.player2Id);
            setPlayer1Login(user1.username);
            setPlayer2Login(user2.username);
        };
        setUsersInfo();
    });
    return (
        <>
            <Typography sx={{ flex: 1, m: 1 }} align="left">
                {player1Login}
            </Typography>
            <Typography sx={{ flex: 1, m: 1 }} align="center">
                {game.score1} - {game.score2}{' '}
            </Typography>
            <Typography sx={{ flex: 1, m: 1 }} align="right">
                {player2Login}
            </Typography>
        </>
    );
};
