import { Sky } from './Sky';
import * as THREE from 'three';

export const initSky = (scene) => {
    // Add Sky
    const sky = new Sky();
    sky.scale.setScalar(45000);
    scene.add(sky);

    const sun = new THREE.Vector3();

    const uniforms = sky.material.uniforms;
    uniforms['turbidity'].value = 10;
    uniforms['rayleigh'].value = 3;
    uniforms['mieCoefficient'].value = 0.005;
    uniforms['mieDirectionalG'].value = 0.7;

    const phi = THREE.MathUtils.degToRad(90 - 2);
    const theta = THREE.MathUtils.degToRad(160);

    sun.setFromSphericalCoords(1, phi, theta);

    uniforms['sunPosition'].value.copy(sun);

    // renderer.toneMappingExposure = 0.0001;
    // renderer.render(scene, camera);
    return () => {
        scene.remove(sky);
        sky.geometry.dispose();
        sky.material.dispose();
    };

    // // GUI

    // const effectController = {
    //     turbidity: 10,
    //     rayleigh: 3,
    //     mieCoefficient: 0.005,
    //     mieDirectionalG: 0.7,
    //     elevation: 2,
    //     azimuth: 180,
    //     exposure: renderer.toneMappingExposure,
    // };

    // const guiChanged = () => {
    //     const uniforms = sky.material.uniforms;
    //     uniforms['turbidity'].value = 10;
    //     uniforms['rayleigh'].value = 3;
    //     uniforms['mieCoefficient'].value = 0.005;
    //     uniforms['mieDirectionalG'].value = 0.7;

    //     const phi = THREE.MathUtils.degToRad(90 - 2);
    //     const theta = THREE.MathUtils.degToRad(180);

    //     sun.setFromSphericalCoords(1, phi, theta);

    //     uniforms['sunPosition'].value.copy(sun);

    //     // renderer.toneMappingExposure = 0.0001;
    //     renderer.render(scene, camera);
    // };

    // const gui = new GUI();

    // gui.add(effectController, 'turbidity', 0.0, 20.0, 0.1).onChange(guiChanged);
    // gui.add(effectController, 'rayleigh', 0.0, 4, 0.001).onChange(guiChanged);
    // gui.add(effectController, 'mieCoefficient', 0.0, 0.1, 0.001).onChange(guiChanged);
    // gui.add(effectController, 'mieDirectionalG', 0.0, 1, 0.001).onChange(guiChanged);
    // gui.add(effectController, 'elevation', 0, 90, 0.1).onChange(guiChanged);
    // gui.add(effectController, 'azimuth', -180, 180, 0.1).onChange(guiChanged);
    // gui.add(effectController, 'exposure', 0, 1, 0.0001).onChange(guiChanged);

    // guiChanged();
};
