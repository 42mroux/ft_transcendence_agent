import React, { useEffect } from 'react';
import * as THREE from 'three';
import {
    Attendee,
    BodyTypesEnum,
    gameConfig,
    GameMap,
    GameSettings,
    GameUpdate,
    MoveDirection,
    TickMessage,
} from '../../../common/game/types/game.types';
import {
    stopListeningToGameSocket,
    launchBall,
    move,
    startListeningToGameSocket,
} from '../../../api/socketIo/gameMessages';
import { getUserById } from '../../../api/axios/profileRoutes';
import { UserFromDBDto } from '../../../common/user/dto/user.dto.out';
import { initCamera, createIndoorScene, createOutdoorScene, initRenderer, setBallInScene } from './utils';
import { OrbitControls } from '../threejs/orbitControls';
import { initSky } from './initSky';

interface GameInfo {
    gameId: number;
}

interface GameAppProps {
    sceneWidth: number;
    gameInfo: GameInfo;
    settings: GameSettings;
    onGameUpdate: (data: { update: GameUpdate; attendees: UserFromDBDto[] }) => void;
    onAttendeeArrived: (username: string) => void;
    onAttendeeLeft: (username: string) => void;
}

const fov = 50;
const aspect = gameConfig.displayRatio;
const near = 0.1;
const far = 1000;

const renderer = new THREE.WebGLRenderer({ antialias: true });
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
const scene = new THREE.Scene();
const controls = new OrbitControls(camera, renderer.domElement);

const GameScenery = (props: GameAppProps): JSX.Element => {
    let mount: HTMLDivElement | null;
    useEffect(() => {
        renderer?.setSize(props.sceneWidth, props.sceneWidth / gameConfig.displayRatio);
    }, [props.sceneWidth]);

    useEffect(() => {
        try {
            // console.log(renderer.info);
            initRenderer(renderer, props.sceneWidth);
            initCamera(camera, props.settings?.map);
            const cleanScene =
                props.settings?.map == GameMap.OUTDOOR ? createOutdoorScene(scene) : createIndoorScene(scene);

            const cleanBackground =
                props.settings.map === GameMap.OUTDOOR
                    ? initSky(scene)
                    : () => {
                          return;
                      };
            controls.target.set(0, 20, 0);
            /* SET UP PLAYERS' PLANK */
            const plankGeometry = new THREE.BoxBufferGeometry(1, 1, 1);
            const plankTexture = new THREE.TextureLoader().load(
                props.settings?.map === GameMap.OUTDOOR
                    ? '/threejs/textures/wood.jpeg'
                    : '/threejs/textures/marble.jpg',
            );

            const plankMaterial = new THREE.MeshStandardMaterial({
                map: plankTexture,
            });
            const leftPlankMesh = new THREE.Mesh(plankGeometry, plankMaterial);
            leftPlankMesh.castShadow = true;
            leftPlankMesh.receiveShadow = true;
            scene.add(leftPlankMesh);
            const rightPlankMesh = new THREE.Mesh(plankGeometry, plankMaterial);
            rightPlankMesh.castShadow = true;
            rightPlankMesh.receiveShadow = true;
            scene.add(rightPlankMesh);

            /* SET UP BALL */
            const golfball = setBallInScene(scene, '/threejs/textures/golfball.jpeg');

            /* SETUP EXTRA */

            const extraGeometry = new THREE.BoxBufferGeometry(3, 3, 3);
            const extraTexture = new THREE.TextureLoader().load('/threejs/textures/question.jpg');
            const extraMaterial = new THREE.MeshStandardMaterial({ map: extraTexture });
            const extraMesh = new THREE.Mesh(extraGeometry, extraMaterial);
            extraMesh.castShadow = true;
            extraMesh.receiveShadow = true;
            extraMesh.visible = false;
            if (props.settings?.powerups) scene.add(extraMesh);

            /* RENDER SCENE IN DOM ELEMENt */
            while (mount?.firstChild) {
                mount.removeChild(mount.firstChild);
            }
            mount?.appendChild(renderer.domElement);

            /* SET RENDERING LOOP */
            const animate = () => {
                requestAnimationFrame(animate);
                controls.update();
                renderer.render(scene, camera);
            };

            const onGameTick = ({ bodies, extras }: TickMessage) => {
                if (extras.length > 0) {
                    if (!extraMesh.visible) extraMesh.visible = true;
                    extraMesh.position.copy(new THREE.Vector3(...extras[0].position));
                    extraMesh.quaternion.copy(new THREE.Quaternion(...(extras[0].quaternion ?? [0, 0, 0, 0])));
                } else if (extraMesh.visible) extraMesh.visible = false;
                golfball.mesh.position.copy(
                    new THREE.Vector3(
                        ...(bodies.find((body) => body.type == BodyTypesEnum.BALL)?.position ?? [0, 0, 0]),
                    ),
                );
                leftPlankMesh.position.copy(
                    new THREE.Vector3(
                        ...(bodies.find((body) => body.type == BodyTypesEnum.PLANK1)?.position ?? [0, 0, 0]),
                    ),
                );
                rightPlankMesh.position.copy(
                    new THREE.Vector3(
                        ...(bodies.find((body) => body.type == BodyTypesEnum.PLANK2)?.position ?? [0, 0, 0]),
                    ),
                );
                leftPlankMesh.scale.set(
                    ...(bodies.find((body) => body.type == BodyTypesEnum.PLANK1)?.scale ?? [1, 1, 1]),
                );
                rightPlankMesh.scale.set(
                    ...(bodies.find((body) => body.type == BodyTypesEnum.PLANK2)?.scale ?? [1, 1, 1]),
                );
                golfball.mesh.scale.set(
                    ...(bodies.find((body) => body.type == BodyTypesEnum.BALL)?.scale ?? [1, 1, 1]),
                );
            };

            const onAttendeeArrived = async (attendee: Attendee) => {
                const username = (await getUserById(attendee?.userId))?.username;
                props.onAttendeeArrived(username);
            };
            const onAttendeeLeft = async (attendee: Attendee) => {
                if (!attendee) return;
                const username = (await getUserById(attendee.userId))?.username;
                props.onAttendeeLeft(username);
            };
            const onKeyDown = (e: KeyboardEvent) => {
                e.preventDefault();
                if (!props.gameInfo.gameId) return;
                if (e.key == 'z') move(props.gameInfo.gameId, MoveDirection.NORTH);
                else if (e.key == 's') move(props.gameInfo.gameId, MoveDirection.SOUTH);
                else if (e.key == ' ') launchBall(props.gameInfo.gameId);
            };

            animate();
            startListeningToGameSocket(onGameTick, props.onGameUpdate, onAttendeeArrived, onAttendeeLeft);
            window.addEventListener('keydown', onKeyDown);

            return () => {
                stopListeningToGameSocket();
                window.removeEventListener('keydown', onKeyDown);
                cleanScene();
                cleanBackground();
                scene.remove(leftPlankMesh);
                scene.remove(rightPlankMesh);
                scene.remove(golfball.mesh);
                scene.remove(extraMesh);
                plankGeometry.dispose();
                plankMaterial.dispose();
                plankTexture.dispose();
                golfball.geometry.dispose();
                golfball.material.dispose();
                golfball.texture.dispose();
                golfball.clearcoatBallMap.dispose();
                extraGeometry.dispose();
                extraMaterial.dispose();
                extraTexture.dispose();
                cleanScene();
            };
        } catch (e) {}
    }, []);

    return <div style={{ width: props.sceneWidth, height: props.sceneWidth / 2 }} ref={(ref) => (mount = ref)}></div>;
};

export default GameScenery;
