import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Typography, Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export enum ResultEnum {
    WIN = 1,
    WIN_DEFAULT,
    LOSE,
    LOSE_DEFAULT,
}

interface GameListDialogProps {
    open: boolean;
    onClose: () => void;
    result?: ResultEnum;
}

export const GameResultDialog = (props: GameListDialogProps): JSX.Element => {
    const navigate = useNavigate();
    const handleClose = () => {
        props.onClose();
        navigate('/game');
    };
    return (
        <Dialog
            open={props.open}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            {props.result && (
                <>
                    <DialogTitle
                        id="scroll-dialog-title"
                        sx={{
                            m: 2,
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {props.result > ResultEnum.WIN_DEFAULT ? ' 😭 Soooo baaaad' : ' 🥳 Congraaaaats'}
                    </DialogTitle>
                    <DialogContent
                        id="scroll-dialog-list"
                        sx={{
                            m: 2,
                            flex: 3,
                            display: 'flex',
                            flexDirection: 'column',
                        }}
                    >
                        <Typography>
                            {`You ${props.result > ResultEnum.WIN_DEFAULT ? 'lose' : 'win'} ${
                                props.result % 2 == 1 ? '!' : 'by default !'
                            }`}
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Box
                            sx={{
                                m: 2,
                                flex: 1,
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'end',
                                borderTop: '1px solid',
                                borderColor: 'lightgray',
                            }}
                        >
                            <Button onClick={handleClose} color="primary" sx={{ marginTop: 2 }}>
                                {props.result > ResultEnum.WIN_DEFAULT
                                    ? "Never mind, I'll be better next time 😤"
                                    : "I'm the best, I knew it ! 😏"}
                            </Button>
                        </Box>
                    </DialogActions>
                </>
            )}
        </Dialog>
    );
};
