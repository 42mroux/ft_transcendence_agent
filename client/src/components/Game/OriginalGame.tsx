import React, { useEffect, useState } from 'react';
import { BoxTypes, BoardData, KeyPressed } from '../../common/game/types/game.types';
import BoardBox from './BoardBox';

const OriginalGame = (): JSX.Element => {
    const paddle: number[] = [...Array(BoardData.PADDLE_SIZE)].map((_, pos) => pos);
    const [player, setPlayer] = useState<number[]>(
        paddle.map((x) => x * BoardData.COL_SIZE + BoardData.PADDLE_EDGE_SPACE),
    );
    const [opponent, setOpponent] = useState<number[]>(
        paddle.map((x) => (x + 1) * BoardData.COL_SIZE - (BoardData.PADDLE_EDGE_SPACE + 1)),
    );
    const [ball, setBall] = useState<number>(Math.round((BoardData.ROW_SIZE * BoardData.COL_SIZE) / 2));
    const ballSpeed = 100;
    const [deltaY, setDeltaY] = useState<number>(-BoardData.COL_SIZE);
    const [deltaX, setDeltaX] = useState<number>(-1);
    const [pause, setPause] = useState<boolean>(true);
    const [playerScore, setPlayerScore] = useState<number>(0);
    const [opponentScore, setOpponentScore] = useState<number>(0);
    const board = [...Array(BoardData.ROW_SIZE * BoardData.COL_SIZE)].map((_, pos) => {
        let val = BoxTypes.BACKGROUND;
        let score = -1;
        if (player.indexOf(pos) !== -1 || opponent.indexOf(pos) !== -1) {
            val = BoxTypes.PLAYER;
        } else if (ball === pos) {
            val = BoxTypes.BALL;
        }
        if (pos === BoardData.COL_SIZE / 2 - 2) {
            score = playerScore;
            val = BoxTypes.SCORE;
        } else if (pos === BoardData.COL_SIZE / 2 + 2) {
            score = opponentScore;
            val = BoxTypes.SCORE;
        }
        return <BoardBox key={pos} type={val} score={score} />;
    });

    const moveBoard = (playerPaddle: number[], isUp: boolean) => {
        const playerEdge = isUp ? playerPaddle[0] : playerPaddle[BoardData.PADDLE_SIZE - 1];

        if (!touchingEdge(playerEdge)) {
            const deltaY = isUp ? -BoardData.COL_SIZE : BoardData.COL_SIZE;
            return playerPaddle.map((x) => x + deltaY);
        }

        return false;
    };

    const onKeyDown = (e: KeyboardEvent) => {
        if (e.code == KeyPressed.PAUSE) {
            setPause(!pause);
        } else if (e.code === KeyPressed.PLAYER_DOWN || e.code === KeyPressed.PLAYER_UP) {
            const movedPlayer = moveBoard(player, e.code === KeyPressed.PLAYER_UP);
            if (movedPlayer) {
                setPlayer(movedPlayer);
                setPause(false);
            }
        } else if (e.code === KeyPressed.OPPONENT_DOWN || e.code === KeyPressed.OPPONENT_UP) {
            const movedPlayer = moveBoard(opponent, e.code === KeyPressed.OPPONENT_UP);
            if (movedPlayer) {
                setOpponent(movedPlayer);
                setPause(false);
            }
        }
    };

    useEffect(() => {
        window.addEventListener('keydown', onKeyDown);

        return () => {
            window.removeEventListener('keydown', onKeyDown);
        };
    }, [pause, player, opponent]);

    const touchingEdge = (pos: number): boolean => {
        if (
            (0 <= pos && pos < BoardData.COL_SIZE) ||
            (BoardData.COL_SIZE * (BoardData.ROW_SIZE - 1) <= pos && pos < BoardData.COL_SIZE * BoardData.ROW_SIZE)
        )
            return true;
        return false;
    };

    const touchingPaddle = (pos: number): boolean => {
        if (
            player.indexOf(pos) !== -1 ||
            opponent.indexOf(pos) !== -1 ||
            (deltaX === -1 && player.indexOf(pos + deltaX) !== -1) ||
            (deltaX !== -1 && opponent.indexOf(pos + deltaX) !== -1)
        )
            return true;
        return false;
    };

    const touchingPaddleEdge = (pos: number): boolean => {
        return (
            player[0] === pos ||
            player[BoardData.PADDLE_SIZE - 1] === pos ||
            opponent[0] === pos ||
            opponent[BoardData.PADDLE_SIZE - 1] === pos
        );
    };

    const isScore = (pos: number): boolean => {
        return (
            (deltaX === -1 && pos % BoardData.COL_SIZE === 0) || (deltaX === 1 && (pos + 1) % BoardData.COL_SIZE === 0)
        );
    };

    const resetGame = (): void => {
        setBall(Math.round(BoardData.ROW_SIZE * BoardData.COL_SIZE) / 2);
    };

    const bounceBall = () => {
        const newPos = ball + deltaY + deltaX;

        if (touchingEdge(newPos)) {
            setDeltaY(-deltaY);
        }

        if (touchingPaddleEdge(newPos)) {
            setDeltaY(-deltaY);
        }

        if (touchingPaddle(newPos)) {
            setDeltaX(-deltaX);
        }
        setBall(newPos);
        /* checking if loss or won */
        if (isScore(newPos)) {
            if (deltaX !== -1) {
                /* player won */
                setPlayerScore(playerScore + 1);
            } else {
                /* opponent won */
                setOpponentScore(opponentScore + 1);
            }
            setPause(true);
            resetGame();
        }
    };

    useEffect(() => {
        const id = setInterval(() => {
            if (!pause) {
                bounceBall();
            }
        }, ballSpeed);
        return () => {
            clearInterval(id);
        };
    }, [pause, ball]);

    return (
        <div
            style={{
                display: 'grid',
                gridTemplate: `repeat(${BoardData.ROW_SIZE}, 1fr) / repeat(${BoardData.COL_SIZE}, 1fr)`,
                backgroundColor: 'black',
                maxHeight: '570px',
            }}
        >
            {board}
        </div>
    );
};

export default OriginalGame;
