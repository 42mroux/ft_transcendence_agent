import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Dialog from '@mui/material/Dialog';
import { ListItemIcon } from '@mui/material';

export interface OptionType {
    title: string;
    icon: JSX.Element;
    action: (login: string) => void;
}

interface OptionsListProps {
    open: boolean;
    onClose: () => void;
    friendLogin: string;
    options: OptionType[];
}

export function MenuOptionsDialog(props: OptionsListProps) {
    const handleClose = () => {
        props.onClose();
    };

    const handleListItemClick = async (option: OptionType) => {
        option.action(props.friendLogin);
    };

    return (
        <Dialog onClose={handleClose} open={props.open}>
            <List>
                {props.options.map((option, index) => (
                    <ListItem button onClick={() => handleListItemClick(option)} key={index}>
                        <ListItemIcon>{option.icon}</ListItemIcon>
                        <ListItemText primary={option.title} />
                    </ListItem>
                ))}
            </List>
        </Dialog>
    );
}
