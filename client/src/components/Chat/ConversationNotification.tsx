import { ListItem, ListItemText } from '@mui/material';
import React from 'react';
import { MessageInConversation } from '../../common/chat/dto/messages.dto.in';

interface ConversationNotificationProps {
    message: MessageInConversation;
}
const ConversationNotification = (props: ConversationNotificationProps) => {
    return (
        <ListItem
            sx={{
                marginBottom: 2,
                width: 'auto',
                borderBottom: '1px solid',
                borderTop: '1px solid',
                borderColor: 'lightgray',
            }}
        >
            <ListItemText primary={props.message.content} />
        </ListItem>
    );
};

export default ConversationNotification;
