import {
    Container,
    Dialog,
    DialogTitle,
    DialogContent,
    TextField,
    FormControl,
    MenuItem,
    Select,
    SelectChangeEvent,
    Button,
    Snackbar,
} from '@mui/material';
import React, { useState } from 'react';
import { RoomTypeEnum } from '../../common/chat/types/rooms.types';

export interface NewChannelDialogProps {
    open: boolean;
    handleClose: () => void;
    handleValidate: (name: string, imagePath: string, type: RoomTypeEnum, password: string) => void;
}

const NewChannelDialog = ({ handleClose, open, handleValidate }: NewChannelDialogProps): JSX.Element => {
    const [name, setName] = useState<string>('');
    const [error, setError] = useState<string>('');
    const [type, setType] = useState<number>(RoomTypeEnum.PUBLIC);
    const [imagePath, setImagePath] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [nameUnauthorized, setNameUnauthorized] = useState<string>('');

    const handleTypeChange = (event: SelectChangeEvent<number>) => {
        setType(event.target.value as number);
    };
    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };
    const onClose = () => {
        handleClose();
        setName('');
        setPassword('');
        setImagePath('');
        setError('');
        setType(RoomTypeEnum.PUBLIC);
    };

    const onValidate = async () => {
        if (name.match(/[%<>\\$'"?!_&@(){}§€`£+=\-/:;.,]/)) {
            setName('');
            setNameUnauthorized('Use of forbidden character, only letters and dash (-) are authorized');
            return;
        }
        try {
            await handleValidate(name, imagePath, type, password);
            setName('');
            setPassword('');
            setImagePath('');
            setError('');
            setType(RoomTypeEnum.PUBLIC);
        } catch (e) {
            setError('Error during channel creation');
        }
    };

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle sx={{ m: 2 }}>Please enter channel details</DialogTitle>
            <DialogContent>
                <Container>
                    <FormControl fullWidth>
                        <TextField
                            label="Name"
                            sx={{ m: 1, width: '25ch' }}
                            required
                            value={name}
                            onChange={handleNameChange}
                            helperText={nameUnauthorized}
                            error={!!nameUnauthorized}
                        />
                        <p>Privacy settings</p>
                        <Select value={type} label="Channel Type" onChange={handleTypeChange}>
                            <MenuItem value={RoomTypeEnum.PUBLIC}>Public</MenuItem>
                            <MenuItem value={RoomTypeEnum.PROTECTED}>Protected</MenuItem>
                            <MenuItem value={RoomTypeEnum.PRIVATE}>Private</MenuItem>
                        </Select>
                        {type === RoomTypeEnum.PROTECTED && (
                            <TextField
                                label="Password"
                                type="password"
                                required
                                sx={{ m: 1, width: '25ch' }}
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                            />
                        )}

                        <Button onClick={onValidate} disabled={!name || (type === RoomTypeEnum.PROTECTED && !password)}>
                            Create
                        </Button>
                    </FormControl>
                </Container>
            </DialogContent>
            <Snackbar
                open={!!error}
                autoHideDuration={2000}
                onClose={() => setError('')}
                message={error}
                action={<Button onClick={() => setError('')}>Ok</Button>}
            />
        </Dialog>
    );
};

export default NewChannelDialog;
