import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { RoomOverviewDto } from '../../common/chat/dto/room.dto.out';
import {
    ListItem,
    ListItemAvatar,
    List,
    ListItemText,
    ListItemIcon,
    TextField,
    Box,
    Alert,
    Collapse,
    IconButton,
} from '@mui/material';
import { findDiscoverableChannels, isRoomProtected, joinRoom } from '../../api/axios/chatRoutes';
import MeetingRoomRoundedIcon from '@mui/icons-material/MeetingRoomRounded';
import LockRoundedIcon from '@mui/icons-material/LockRounded';
import { startListeningInRoom } from '../../api/socketIo/chatMessages';
import CloseIcon from '@mui/icons-material/Close';
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded';

interface DiscoverableChannelsDialogProps {
    open: boolean;
    onClose: () => void;
    chatRoomsList: RoomOverviewDto[];
    fetchRooms: (selectedRoom: number) => void;
}

const DiscoverableChannelsDialog = (props: DiscoverableChannelsDialogProps): JSX.Element => {
    const [discoverableChannels, setDiscoverableChannels] = useState<RoomOverviewDto[] | null>([]);
    const [loaded, setLoaded] = useState<boolean>(true);
    const [openPasswordInput, setOpenPasswordInput] = useState<boolean>(false);
    const [password, setPassword] = useState<string>('');
    const [selectedChannelId, setSelectedChannelId] = useState<number>(0);
    const [openError, setOpenError] = useState<boolean>(false);

    useEffect(() => {
        return () => setLoaded(false);
    }, []);

    const loadDiscoverableChannels = async (): Promise<void> => {
        try {
            const discoverableChannels = await findDiscoverableChannels();
            if (loaded) setDiscoverableChannels(discoverableChannels);
        } catch (e) {
            setDiscoverableChannels([]);
        }
    };

    useEffect(() => {
        loadDiscoverableChannels();
    }, [props.chatRoomsList]);

    const handleClose = () => {
        setPassword('');
        setSelectedChannelId(0);
        setOpenPasswordInput(false);
        setOpenError(false);
        props.onClose();
    };

    const selectChannelToJoin = async (roomId: number): Promise<void> => {
        const passwordNeeded = await isRoomProtected(roomId);
        if (passwordNeeded) {
            setSelectedChannelId(roomId);
            setOpenPasswordInput(true);
        } else {
            joinChannel(roomId, null);
            props.onClose();
        }
    };

    const joinChannel = async (roomId: number, password: string | null): Promise<void> => {
        try {
            const joined = await joinRoom(roomId, password);
            if (joined === true) {
                startListeningInRoom(roomId);
                props.fetchRooms(roomId);
                handleClose();
            } else {
                setOpenError(true);
                setPassword('');
            }
        } catch (e) {
            setOpenError(true);
            setPassword('');
        }
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Join channel
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {openPasswordInput === false && discoverableChannels !== null && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {discoverableChannels?.map((discoverableChannel, index) => (
                            <ListItem
                                key={index}
                                sx={{ flex: 1, flexGrow: 1 }}
                                button
                                onClick={() => {
                                    selectChannelToJoin(discoverableChannel.roomId);
                                }}
                            >
                                <ListItemAvatar>
                                    <GroupsRoundedIcon sx={{ color: 'gray', m: 2, marginLeft: 3 }} />
                                </ListItemAvatar>
                                <ListItemText primary={discoverableChannel.name} />
                                <ListItemIcon>
                                    {discoverableChannel.type === 3 && <MeetingRoomRoundedIcon />}
                                    {discoverableChannel.type === 4 && <LockRoundedIcon />}
                                </ListItemIcon>
                            </ListItem>
                        ))}
                    </List>
                )}
                {openPasswordInput === true && (
                    <Box sx={{ display: 'flex', justifyContent: 'center', flexDirection: 'column', width: '100%' }}>
                        <Collapse in={openError}>
                            <Alert
                                color="error"
                                action={
                                    <IconButton
                                        aria-label="close"
                                        color="error"
                                        size="small"
                                        onClick={() => {
                                            setOpenError(false);
                                        }}
                                    >
                                        <CloseIcon fontSize="inherit" />
                                    </IconButton>
                                }
                                sx={{ mb: 2 }}
                            >
                                Wrong password
                            </Alert>
                        </Collapse>
                        <Box sx={{ display: 'flex', alignContent: 'center' }}>
                            <TextField
                                label="Password"
                                type="password"
                                required
                                sx={{ m: 1, width: '25ch', flex: 1 }}
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                            />
                            <Button
                                disabled={!password}
                                onClick={() => {
                                    joinChannel(selectedChannelId, password);
                                }}
                            >
                                Join in
                            </Button>
                        </Box>
                    </Box>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default DiscoverableChannelsDialog;
