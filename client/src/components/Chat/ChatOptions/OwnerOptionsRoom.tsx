import React, { useState } from 'react';
import { List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import ManageAccountsRoundedIcon from '@mui/icons-material/ManageAccountsRounded';
import LockRoundedIcon from '@mui/icons-material/LockRounded';
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import ManageAdminsDialog from './ManageAdminsDialog';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { deleteChannel } from '../../../api/axios/chatRoutes';
import PrivacySettingsDialog from './PrivacySettingsDialog';
import { RoomTypeEnum } from '../../../common/chat/types/rooms.types';

interface OwnerOptionsRoomRoomProps {
    selectedChatRoom: RoomOverviewDto;
    setSelectedChatRoom: (newRoom: RoomOverviewDto) => void;
    closeRoomSettings: () => void;
    fetchRooms: (selectedRoom: number) => void;
}

const OwnerOptionsRoom = (props: OwnerOptionsRoomRoomProps): JSX.Element => {
    const [openManageAdmins, setOpenManageAdmins] = useState<boolean>(false);
    const [openPrivacySettings, setOpenPrivacySettings] = useState<boolean>(false);

    const handleOpenManageAdmins = (): void => {
        setOpenManageAdmins(true);
    };

    const handleCloseManageAdmins = (): void => {
        setOpenManageAdmins(false);
        props.closeRoomSettings();
    };

    const handleOpenPrivacySettings = (): void => {
        setOpenPrivacySettings(true);
    };

    const handleClosePrivacySettings = (): void => {
        setOpenPrivacySettings(false);
        props.closeRoomSettings();
    };

    const handleDeleteChannel = async (): Promise<void> => {
        await deleteChannel(props.selectedChatRoom?.roomId);
        props.closeRoomSettings();
    };

    return (
        <List>
            <ListItem button onClick={handleOpenManageAdmins}>
                <ListItemIcon>
                    <ManageAccountsRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Manage admins" />
            </ListItem>
            {props?.selectedChatRoom && (
                <ManageAdminsDialog
                    open={openManageAdmins}
                    onClose={handleCloseManageAdmins}
                    selectedChatRoom={props.selectedChatRoom}
                    fetchRooms={props.fetchRooms}
                />
            )}
            <ListItem
                button
                onClick={handleOpenPrivacySettings}
                disabled={props?.selectedChatRoom?.type === RoomTypeEnum.PRIVATE}
            >
                <ListItemIcon>
                    <LockRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Privacy settings" />
            </ListItem>
            <PrivacySettingsDialog
                open={openPrivacySettings}
                onClose={handleClosePrivacySettings}
                selectedChatRoom={props.selectedChatRoom}
                fetchRooms={props.fetchRooms}
            />
            <ListItem
                button
                onClick={() => {
                    handleDeleteChannel();
                }}
            >
                <ListItemIcon>
                    <DeleteForeverRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Delete channel" />
            </ListItem>
        </List>
    );
};

export default OwnerOptionsRoom;
