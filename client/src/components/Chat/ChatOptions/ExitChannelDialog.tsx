import {
    Avatar,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    List,
    ListItem,
    ListItemAvatar,
    ListItemIcon,
    ListItemText,
} from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { AuthContext } from '../../../contexts/auth.context';
import { User } from '../../../common/user/dto/user.dto.out';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import { changeOwnerOfRoom, findPotentialOwners, removeUserFromRoom } from '../../../api/axios/chatRoutes';
import { stopListeningInRoom } from '../../../api/socketIo/chatMessages';

interface ExitChannelProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
}

const ExitChannelDialog = (props: ExitChannelProps): JSX.Element => {
    const [potentialOwners, setPotentialOwners] = useState<User[] | null>(null);
    const auth = useContext(AuthContext);
    const [loaded, setLoaded] = useState<boolean>(true);

    useEffect(() => {
        return () => setLoaded(false);
    }, []);

    useEffect(() => {
        if (props.selectedChatRoom !== undefined)
            loadPotentialOwners(props.selectedChatRoom?.roomId).catch(() => {
                return;
            });
    }, [props.selectedChatRoom]);

    const loadPotentialOwners = async (roomId: number): Promise<void> => {
        if (roomId !== 0) {
            const owners = await findPotentialOwners(roomId);
            if (loaded) setPotentialOwners(owners);
        }
    };

    const selectNewOwner = async (userId: number): Promise<void> => {
        if (props.selectedChatRoom?.bannedIds?.find((bannedUserId) => bannedUserId === userId) === undefined) {
            props.onClose();
            await changeOwnerOfRoom(userId, props.selectedChatRoom?.roomId);
            await removeUserFromRoom(auth.userId, props.selectedChatRoom?.roomId);
            stopListeningInRoom(props.selectedChatRoom.roomId);
        }
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                You&rsquo;re the owner of this channel, you must choose another one if you want to leave.
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {potentialOwners !== null && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {potentialOwners?.map((potentialOwner, index) => {
                            if (potentialOwner.id !== auth.userId)
                                return props?.selectedChatRoom?.bannedIds?.find(
                                    (bannedUserId) => bannedUserId === potentialOwner.id,
                                ) ? (
                                    <ListItem key={index} sx={{ flex: 1, flexGrow: 1 }} disabled>
                                        <ListItemAvatar>
                                            <Avatar src={potentialOwner.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={potentialOwner.login} />
                                        <ListItemIcon>
                                            <AddCircleOutlineRoundedIcon />
                                        </ListItemIcon>
                                    </ListItem>
                                ) : (
                                    <ListItem key={index} sx={{ flex: 1, flexGrow: 1 }} button>
                                        <ListItemAvatar>
                                            <Avatar src={potentialOwner.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={potentialOwner.login} />
                                        <ListItemIcon
                                            onClick={() => {
                                                selectNewOwner(potentialOwner.id);
                                            }}
                                        >
                                            <AddCircleOutlineRoundedIcon />
                                        </ListItemIcon>
                                    </ListItem>
                                );
                        })}
                    </List>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ExitChannelDialog;
