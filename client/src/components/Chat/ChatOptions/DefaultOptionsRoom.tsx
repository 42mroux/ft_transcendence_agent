import React, { useState } from 'react';
import { List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import PeopleAltRoundedIcon from '@mui/icons-material/PeopleAltRounded';
import BorderColorRoundedIcon from '@mui/icons-material/BorderColorRounded';
import PersonAddRoundedIcon from '@mui/icons-material/PersonAddRounded';
import RenameChannelDialog from './RenameChannelDialog';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import InviteFriendsDialog from './InviteFriendsDialog';
import SeeParticipantsDialog from './SeeParticipantsDialog';

interface DefaultOptionsRoomProps {
    selectedChatRoom: RoomOverviewDto;
    setSelectedChatRoom: (newRoom: RoomOverviewDto) => void;
    closeRoomSettings: () => void;
    fetchRooms: (selectedRoom: number) => void;
}

const DefaultOptionsRoom = (props: DefaultOptionsRoomProps): JSX.Element => {
    const [openRename, setOpenRename] = useState<boolean>(false);
    const [openInvite, setOpenInvite] = useState<boolean>(false);
    const [openSee, setOpenSee] = useState<boolean>(false);

    const handleOpenSee = (): void => {
        setOpenSee(true);
    };

    const handleCloseSee = (): void => {
        setOpenSee(false);
    };

    const handleOpenInvite = (): void => {
        setOpenInvite(true);
    };

    const handleCloseInvite = (): void => {
        setOpenInvite(false);
        props.closeRoomSettings();
    };

    const handleOpenRename = (): void => {
        setOpenRename(true);
    };

    const handleCloseRename = (): void => {
        setOpenRename(false);
        props.closeRoomSettings();
    };
    return (
        <List>
            <ListItem button onClick={handleOpenInvite}>
                <ListItemIcon>
                    <PersonAddRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Invite friend" />
            </ListItem>
            <InviteFriendsDialog
                open={openInvite}
                onClose={handleCloseInvite}
                selectedChatRoom={props.selectedChatRoom}
            />
            <ListItem
                button
                onClick={() => {
                    handleOpenSee();
                }}
            >
                <ListItemIcon>
                    <PeopleAltRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="See participants" />
            </ListItem>
            <SeeParticipantsDialog open={openSee} onClose={handleCloseSee} selectedChatRoom={props.selectedChatRoom} />
            <ListItem button onClick={handleOpenRename}>
                <ListItemIcon>
                    <BorderColorRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Rename channel" />
            </ListItem>
            <RenameChannelDialog
                open={openRename}
                onClose={handleCloseRename}
                selectedChatRoom={props.selectedChatRoom}
                setSelectedChatRoom={props.setSelectedChatRoom}
                fetchRooms={props.fetchRooms}
            />
        </List>
    );
};

export default DefaultOptionsRoom;
