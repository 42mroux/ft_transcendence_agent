import {
    Dialog,
    DialogTitle,
    DialogContent,
    Button,
    DialogActions,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Alert,
    Box,
    Collapse,
    IconButton,
    TextField,
} from '@mui/material';
import React, { useState } from 'react';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { RoomTypeEnum } from '../../../common/chat/types/rooms.types';
import PublicRoundedIcon from '@mui/icons-material/PublicRounded';
import EnhancedEncryptionRoundedIcon from '@mui/icons-material/EnhancedEncryptionRounded';
import SyncLockRoundedIcon from '@mui/icons-material/SyncLockRounded';
import CloseIcon from '@mui/icons-material/Close';
import { newPasswordForRoom, updateChannelType } from '../../../api/axios/chatRoutes';

interface PrivacySettingsDialogProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
    fetchRooms: (selectedRoom: number) => void;
}

const PrivacySettingsDialog = (props: PrivacySettingsDialogProps): JSX.Element => {
    const [openPasswordInput, setOpenPasswordInput] = useState<boolean>(false);
    const [password, setPassword] = useState<string>('');
    const [openError, setOpenError] = useState<boolean>(false);
    const [forbiddenCharacterError, setForbiddenCharacterError] = useState<string>('');

    const handleClose = (): void => {
        setOpenPasswordInput(false);
        setPassword('');
        setForbiddenCharacterError('');
        props.onClose();
    };

    const passwordInputNeededForChannel = (): void => {
        setOpenPasswordInput(true);
    };

    const submitNewPassword = async (roomId: number): Promise<void> => {
        if (password === '' || password.match(/[<>\\$'"_`/:;.]/)) {
            setPassword('');
            setForbiddenCharacterError('The following characters are forbidden: <>\\$\'"_`-/:;.');
            return;
        }
        const newPasswordStored = await newPasswordForRoom(roomId, password);
        if (newPasswordStored === true) {
            props.fetchRooms(roomId);
            handleClose();
        } else {
            setOpenError(true);
            setPassword('');
        }
    };

    const makeRoomPublic = async (roomId: number): Promise<void> => {
        await updateChannelType(roomId, RoomTypeEnum.PUBLIC);
        props.fetchRooms(roomId);
        handleClose();
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Privacy settings
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {openPasswordInput === false && props?.selectedChatRoom?.type === RoomTypeEnum.PUBLIC && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        <ListItem
                            sx={{ flex: 1, flexGrow: 1 }}
                            button
                            onClick={() => {
                                passwordInputNeededForChannel();
                            }}
                        >
                            <ListItemText primary="Protect with a password" />
                            <ListItemIcon>
                                <EnhancedEncryptionRoundedIcon />
                            </ListItemIcon>
                        </ListItem>
                    </List>
                )}
                {openPasswordInput === false && props?.selectedChatRoom?.type === RoomTypeEnum.PROTECTED && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        <ListItem
                            sx={{ flex: 1, flexGrow: 1 }}
                            button
                            onClick={() => {
                                makeRoomPublic(props.selectedChatRoom.roomId);
                            }}
                        >
                            <ListItemText primary="Make channel public" />
                            <ListItemIcon>
                                <PublicRoundedIcon />
                            </ListItemIcon>
                        </ListItem>
                        <ListItem
                            sx={{ flex: 1, flexGrow: 1 }}
                            button
                            onClick={() => {
                                passwordInputNeededForChannel();
                            }}
                        >
                            <ListItemText primary="Change password" />
                            <ListItemIcon>
                                <SyncLockRoundedIcon />
                            </ListItemIcon>
                        </ListItem>
                    </List>
                )}
                {openPasswordInput === true && (
                    <Box sx={{ display: 'flex', justifyContent: 'center', flexDirection: 'column', width: '100%' }}>
                        <Collapse in={openError}>
                            <Alert
                                color="error"
                                action={
                                    <IconButton
                                        aria-label="close"
                                        color="error"
                                        size="small"
                                        onClick={() => {
                                            setOpenError(false);
                                        }}
                                    >
                                        <CloseIcon fontSize="inherit" />
                                    </IconButton>
                                }
                                sx={{ mb: 2 }}
                            >
                                There was an error when storing the password, please try again.
                            </Alert>
                        </Collapse>
                        <Box sx={{ display: 'flex', alignContent: 'center' }}>
                            <TextField
                                label="Password"
                                type="password"
                                required
                                sx={{ m: 1, width: '25ch', flex: 1 }}
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                                helperText={forbiddenCharacterError}
                                error={!!forbiddenCharacterError}
                            />
                            <Button
                                disabled={!password}
                                onClick={() => {
                                    submitNewPassword(props.selectedChatRoom.roomId);
                                }}
                            >
                                Submit new password
                            </Button>
                        </Box>
                    </Box>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default PrivacySettingsDialog;
