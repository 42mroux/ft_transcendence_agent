import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { User } from '../../../common/user/dto/user.dto.out';
import { findPotentialGuestsForChannel } from '../../../api/axios/friendsRoutes';
import { ListItem, ListItemAvatar, List, ListItemText, ListItemIcon, Avatar } from '@mui/material';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import { addUserToRoom } from '../../../api/axios/chatRoutes';

interface InviteFriendsProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
}

const InviteFriendsDialog = (props: InviteFriendsProps): JSX.Element => {
    const [potentialGuests, setPotentialGuests] = useState<User[] | null>([]);
    const [loaded, setLoaded] = useState<boolean>(true);

    useEffect(() => {
        return () => setLoaded(false);
    }, []);

    useEffect(() => {
        if (props.selectedChatRoom !== undefined)
            loadPotentialGuests().catch(() => {
                return;
            });
    }, [props.selectedChatRoom]);

    const loadPotentialGuests = async (): Promise<void> => {
        const guests = await findPotentialGuestsForChannel(props.selectedChatRoom?.roomId);
        if (loaded) setPotentialGuests(guests);
    };

    const handleClose = () => {
        props.onClose();
    };

    const addFriendToChatRoom = async (friendId: number): Promise<void> => {
        await addUserToRoom(friendId, props.selectedChatRoom.roomId);
        props.onClose();
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Invite friends
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {potentialGuests !== null && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {potentialGuests?.map((potentialGuest, index) => (
                            <ListItem
                                key={index}
                                sx={{ flex: 1, flexGrow: 1 }}
                                button
                                onClick={() => {
                                    addFriendToChatRoom(potentialGuest.id);
                                }}
                            >
                                <ListItemAvatar>
                                    <Avatar src={potentialGuest.avatar} />
                                </ListItemAvatar>
                                <ListItemText primary={potentialGuest.login} />
                                <ListItemIcon>
                                    <AddCircleOutlineRoundedIcon color="success" />
                                </ListItemIcon>
                            </ListItem>
                        ))}
                    </List>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default InviteFriendsDialog;
