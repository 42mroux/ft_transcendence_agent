import React, { useState } from 'react';
import { List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import RemoveCircleRoundedIcon from '@mui/icons-material/RemoveCircleRounded';
import VolumeOffRoundedIcon from '@mui/icons-material/VolumeOffRounded';
import MuteUserDialog from './MuteUserDialog';
import BanUserDialog from './BanUserDialog';

interface AdminOptionsRoomRoomProps {
    selectedChatRoom: RoomOverviewDto;
    setSelectedChatRoom: (newRoom: RoomOverviewDto) => void;
    closeRoomSettings: () => void;
    fetchRooms: (selectedRoom: number) => void;
}

const AdminOptionsRoom = (props: AdminOptionsRoomRoomProps): JSX.Element => {
    const [openBanUser, setOpenBanUser] = useState<boolean>(false);
    const [openMuteUser, setOpenMuteUser] = useState<boolean>(false);

    const handleOpenMuteUser = (): void => {
        setOpenMuteUser(true);
    };

    const handleCloseMuteUser = (): void => {
        setOpenMuteUser(false);
        props.closeRoomSettings();
    };

    const handleOpenBanUser = (): void => {
        setOpenBanUser(true);
    };

    const handleCloseBanUser = (): void => {
        setOpenBanUser(false);
        props.closeRoomSettings();
    };

    return (
        <List>
            <ListItem button onClick={handleOpenMuteUser}>
                <ListItemIcon>
                    <VolumeOffRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Mute user" />
            </ListItem>
            <MuteUserDialog
                open={openMuteUser}
                onClose={handleCloseMuteUser}
                selectedChatRoom={props.selectedChatRoom}
                fetchRooms={props.fetchRooms}
            />
            <ListItem button onClick={handleOpenBanUser}>
                <ListItemIcon>
                    <RemoveCircleRoundedIcon />
                </ListItemIcon>
                <ListItemText primary="Ban user" />
            </ListItem>
            <BanUserDialog
                open={openBanUser}
                onClose={handleCloseBanUser}
                selectedChatRoom={props.selectedChatRoom}
                fetchRooms={props.fetchRooms}
            />
        </List>
    );
};

export default AdminOptionsRoom;
