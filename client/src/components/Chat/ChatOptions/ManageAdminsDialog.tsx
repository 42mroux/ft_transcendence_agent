import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { User } from '../../../common/user/dto/user.dto.out';
import { ListItem, ListItemAvatar, List, ListItemText, ListItemIcon, Avatar } from '@mui/material';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import { addAdminToRoom, findUsersForRoom, removeAdminFromRoom } from '../../../api/axios/chatRoutes';
import RemoveCircleOutlineRoundedIcon from '@mui/icons-material/RemoveCircleOutlineRounded';

interface ManageAdminsProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
    fetchRooms: (selectedRoom: number) => void;
}

const ManageAdminsDialog = (props: ManageAdminsProps): JSX.Element => {
    const [users, setUsers] = useState<User[] | null>([]);
    const [loaded, setLoaded] = useState<boolean>(true);

    useEffect(() => {
        return () => setLoaded(false);
    }, []);

    const loadUsers = async (): Promise<void> => {
        const users = await findUsersForRoom(props.selectedChatRoom?.roomId);
        if (loaded) setUsers(users);
    };

    useEffect(() => {
        if (props.selectedChatRoom !== undefined) {
            loadUsers().catch(() => {
                return;
            });
        }
    }, [props.selectedChatRoom]);

    const handleClose = () => {
        props.onClose();
    };

    const addAdmin = async (newAdminId: number): Promise<void> => {
        await addAdminToRoom(newAdminId, props.selectedChatRoom?.roomId);
        props.onClose();
    };

    const removeAdmin = async (newAdminId: number): Promise<void> => {
        await removeAdminFromRoom(newAdminId, props.selectedChatRoom?.roomId);
        props.onClose();
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Manage channel administrators
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {users !== null && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {users?.map((user, index) => {
                            if (props?.selectedChatRoom?.ownerId !== user.id)
                                return props?.selectedChatRoom?.adminsIds?.find((adminId) => adminId === user.id) ? (
                                    <ListItem key={index} sx={{ flex: 1, flexGrow: 1 }} button>
                                        <ListItemAvatar>
                                            <Avatar src={user.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={user.login} />
                                        <ListItemIcon
                                            onClick={() => {
                                                removeAdmin(user.id);
                                            }}
                                        >
                                            <RemoveCircleOutlineRoundedIcon color="error"></RemoveCircleOutlineRoundedIcon>
                                        </ListItemIcon>
                                    </ListItem>
                                ) : (
                                    <ListItem key={index} sx={{ flex: 1, flexGrow: 1 }} button>
                                        <ListItemAvatar>
                                            <Avatar src={user.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={user.login} />
                                        <ListItemIcon
                                            onClick={() => {
                                                addAdmin(user.id);
                                            }}
                                        >
                                            <AddCircleOutlineRoundedIcon color="success" />
                                        </ListItemIcon>
                                    </ListItem>
                                );
                        })}
                    </List>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ManageAdminsDialog;
