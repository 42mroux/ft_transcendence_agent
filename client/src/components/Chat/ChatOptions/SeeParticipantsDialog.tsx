import {
    Avatar,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    List,
    ListItem,
    ListItemAvatar,
    ListItemIcon,
    ListItemText,
} from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { findUsersForRoom } from '../../../api/axios/chatRoutes';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { User } from '../../../common/user/dto/user.dto.out';
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import { AuthContext } from '../../../contexts/auth.context';
import { blockUser, unblockUser } from '../../../api/socketIo/chatMessages';
import MobileFriendlyRoundedIcon from '@mui/icons-material/MobileFriendlyRounded';

interface SeeParticipantsDialogProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
}

const SeeParticipantsDialog = (props: SeeParticipantsDialogProps): JSX.Element => {
    const [participants, setParticipants] = useState<User[] | null>(null);
    const auth = useContext(AuthContext);

    const loadChatRoomParticipants = async (roomId: number) => {
        const users = await findUsersForRoom(roomId);
        setParticipants(users);
    };

    useEffect(() => {
        if (props.selectedChatRoom !== undefined)
            loadChatRoomParticipants(props.selectedChatRoom.roomId).catch(() => {
                return;
            });
    }, [props.selectedChatRoom]);

    const handleClose = () => {
        props.onClose();
    };

    const blockParticipant = async (userId: number) => {
        blockUser(userId, auth.refreshBlockedUsersId);
        props.onClose();
    };

    const unblockParticipant = async (userId: number) => {
        unblockUser(userId, auth.refreshBlockedUsersId);
        props.onClose();
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                See participants
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {participants !== null && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {participants?.map((participant, index) => {
                            if (participant.id !== auth.userId)
                                return auth.blockedUsersId !== null &&
                                    // TODO could be replace with auth method
                                    auth.blockedUsersId.find((id) => id === participant.id) !== undefined ? (
                                    <ListItem
                                        key={index}
                                        sx={{ flex: 1, flexGrow: 1 }}
                                        button
                                        onClick={() => {
                                            unblockParticipant(participant.id);
                                        }}
                                    >
                                        <ListItemAvatar>
                                            <Avatar src={participant.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={participant.username} />
                                        <ListItemIcon>
                                            <MobileFriendlyRoundedIcon />
                                        </ListItemIcon>
                                    </ListItem>
                                ) : (
                                    <ListItem
                                        key={index}
                                        sx={{ flex: 1, flexGrow: 1 }}
                                        button
                                        onClick={() => {
                                            blockParticipant(participant.id);
                                        }}
                                    >
                                        <ListItemAvatar>
                                            <Avatar src={participant.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={participant.username} />
                                        <ListItemIcon>
                                            <CancelRoundedIcon />
                                        </ListItemIcon>
                                    </ListItem>
                                );
                        })}
                    </List>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default SeeParticipantsDialog;
