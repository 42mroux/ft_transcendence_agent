import {
    Dialog,
    DialogTitle,
    DialogContent,
    List,
    ListItem,
    ListItemAvatar,
    Avatar,
    ListItemText,
    ListItemIcon,
    DialogActions,
    Button,
    Menu,
} from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { findUsersForRoom, muteUserForRoom } from '../../../api/axios/chatRoutes';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import { User } from '../../../common/user/dto/user.dto.out';
import VolumeOffRoundedIcon from '@mui/icons-material/VolumeOffRounded';
import { AuthContext } from '../../../contexts/auth.context';

interface MuteUserProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
    fetchRooms: (selectedRoom: number) => void;
}

const MuteUserDialog = (props: MuteUserProps): JSX.Element => {
    const [users, setUsers] = useState<User[] | null>([]);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [mutedUserId, setMutedUserId] = useState<number>(0);
    const auth = useContext(AuthContext);
    const [loaded, setLoaded] = useState<boolean>(true);

    useEffect(() => {
        return () => setLoaded(false);
    }, []);

    useEffect(() => {
        if (props.selectedChatRoom !== undefined) {
            loadUsers().catch(() => {
                return;
            });
        }
    }, [props.selectedChatRoom]);

    const loadUsers = async (): Promise<void> => {
        const users = await findUsersForRoom(props.selectedChatRoom?.roomId);
        if (loaded) setUsers(users);
    };

    const handleClose = () => {
        handleCloseMenu();
        props.onClose();
    };

    const handleOpenMenu = (event: React.MouseEvent<HTMLElement>, userId: number) => {
        setAnchorEl(event.currentTarget);
        setMutedUserId(userId);
    };

    const handleCloseMenu = () => {
        setAnchorEl(null);
    };

    /*adding for how long later*/
    const muteUserFor = async (duration: number) => {
        await muteUserForRoom(mutedUserId, props.selectedChatRoom?.roomId, duration);
        handleClose();
    };

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-list"
            fullWidth
            maxWidth="sm"
        >
            <DialogTitle id="scroll-dialog-title" sx={{ m: 2 }}>
                Mute user
            </DialogTitle>
            <DialogContent
                id="scroll-dialog-list"
                sx={{
                    display: 'flex',
                }}
            >
                {users !== null && (
                    <List
                        sx={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                            borderTop: '1px solid',
                            borderBottom: '1px solid',
                            borderColor: 'lightgray',
                        }}
                    >
                        {users?.map((user, index) => {
                            if (user.id !== auth.userId && user.id !== props.selectedChatRoom?.ownerId)
                                return props.selectedChatRoom?.mutedIds !== null &&
                                    props.selectedChatRoom?.mutedIds.find((muteUserId) => muteUserId === user.id) ? (
                                    <ListItem key={index} sx={{ flex: 1, flexGrow: 1 }} disabled>
                                        <ListItemAvatar>
                                            <Avatar src={user.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={user.login} />
                                        <ListItemIcon>
                                            <VolumeOffRoundedIcon />
                                        </ListItemIcon>
                                    </ListItem>
                                ) : (
                                    <ListItem key={index} sx={{ flex: 1, flexGrow: 1 }} button>
                                        <ListItemAvatar>
                                            <Avatar src={user.avatar} />
                                        </ListItemAvatar>
                                        <ListItemText primary={user.login} />
                                        <ListItemIcon
                                            onClick={(event: React.MouseEvent<HTMLElement>) => {
                                                handleOpenMenu(event, user.id);
                                            }}
                                        >
                                            <VolumeOffRoundedIcon />
                                        </ListItemIcon>
                                        <Menu
                                            id="block-settings"
                                            anchorEl={anchorEl}
                                            anchorOrigin={{
                                                vertical: 'bottom',
                                                horizontal: 'right',
                                            }}
                                            keepMounted
                                            transformOrigin={{
                                                vertical: 'top',
                                                horizontal: 'right',
                                            }}
                                            open={Boolean(anchorEl)}
                                            onClose={handleCloseMenu}
                                        >
                                            <List>
                                                <ListItem
                                                    button
                                                    onClick={() => {
                                                        muteUserFor(1);
                                                    }}
                                                >
                                                    <ListItemText primary="1 minute" />
                                                </ListItem>
                                                <ListItem
                                                    button
                                                    onClick={() => {
                                                        muteUserFor(10);
                                                    }}
                                                >
                                                    <ListItemText primary="10 minutes" />
                                                </ListItem>
                                                <ListItem
                                                    button
                                                    onClick={() => {
                                                        muteUserFor(30);
                                                    }}
                                                >
                                                    <ListItemText primary="30 minutes" />
                                                </ListItem>
                                                <ListItem
                                                    button
                                                    onClick={() => {
                                                        muteUserFor(60);
                                                    }}
                                                >
                                                    <ListItemText primary="1 hour" />
                                                </ListItem>
                                                <ListItem
                                                    button
                                                    onClick={() => {
                                                        muteUserFor(0);
                                                    }}
                                                >
                                                    <ListItemText primary="For ever" />
                                                </ListItem>
                                            </List>
                                        </Menu>
                                    </ListItem>
                                );
                        })}
                    </List>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="error" sx={{ marginRight: 2, marginBottom: 2 }}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default MuteUserDialog;
