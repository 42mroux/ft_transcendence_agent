import { ListItem, ListItemText } from '@mui/material';
import React from 'react';
import { MessageInConversation } from '../../common/chat/dto/messages.dto.in';

interface MessageSentProps {
    message: MessageInConversation;
}
const MessageSent = (props: MessageSentProps) => {
    return (
        <ListItem
            sx={{
                backgroundColor: 'lightblue',
                marginBottom: 2,
                p: 1,
                width: 'auto',
                borderBottomLeftRadius: 20,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
            }}
        >
            <ListItemText primary={props.message.content} />
        </ListItem>
    );
};

export default MessageSent;
