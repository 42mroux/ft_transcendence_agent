export interface MessageInConversation {
    senderId: number;
    roomId: number;
    time: string;
    content: string;
}
