import { RoomTypeEnum } from '../types/rooms.types';

export interface RoomOverviewDto {
    roomId: number;
    type: RoomTypeEnum;
    name: string;
    image: string | undefined;
    usersIds: number[];
    adminsIds: number[];
    ownerId: number;
    mutedIds: number[] | null;
    bannedIds: number[] | null;
}
