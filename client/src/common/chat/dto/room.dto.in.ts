export interface CreateRoomDto {
    ownerId: number;
    name: string;
    image: string | undefined;
    typeId: number;
    password: string | null;
}
