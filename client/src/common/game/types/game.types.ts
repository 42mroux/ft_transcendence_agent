export const gameConfig = {
    displayRatio: 2,
};

export enum GameMap {
    OUTDOOR = 'OUTDOOR',
    INDOOR = 'INDOOR',
}

export enum GameLevel {
    EASY = 1,
    MEDIUM = 2,
    HARD = 3,
}

export interface GameSettings {
    level: GameLevel;
    map: GameMap;
    powerups: boolean;
}

export enum BodyTypesEnum {
    BALL = 'BALL',
    PLANK1 = 'PLANK1',
    PLANK2 = 'PLANK2',
    FLOOR = 'FLOOR',
    CEIL = 'CEIL',
    WALL = 'WALL',
}

export enum ExtraTypesEnum {
    SPEED_UP = 'SPEED_UP',
    GRAVITY = 'GRAVITY',
}

export interface BodyInfo {
    position: [number, number, number];
    velocity?: [number, number, number];
    quaternion?: [number, number, number, number];
    scale?: [number, number, number];
    type: BodyTypesEnum | ExtraTypesEnum;
}
export interface TickMessage {
    bodies: BodyInfo[];
    extras: BodyInfo[];
}

export enum MoveDirection {
    NORTH,
    SOUTH,
    EAST,
    WEST,
}

export enum Role {
    'PLAYER_ONE',
    'PLAYER_TWO',
    'VIEWER',
}

// Andrea's types

export enum BoxTypes {
    BACKGROUND = 0,
    PLAYER = 1,
    BALL = 2,
    SCORE = 3,
}

export enum BoardData {
    COL_SIZE = 30,
    ROW_SIZE = 15,
    PADDLE_SIZE = 3,
    PADDLE_EDGE_SPACE = 1,
}

export enum KeyPressed {
    PAUSE = 'Space',
    PLAYER_UP = 'KeyW' /* z */,
    PLAYER_DOWN = 'KeyS' /* x */,
    OPPONENT_UP = 'ArrowUp', // up arrow
    OPPONENT_DOWN = 'ArrowDown', // down arrow
}

export enum GameMessage {
    CREATE_GAME = 'create game',
    JOIN_GAME = 'join game',
    LEAVE_GAME = 'leave game',
    LAUNCH_BALL = 'launch ball',
    MOVE = 'move',
    UPDATE = 'update',
    ATTENDEE_ARRIVED = 'attendee arrived',
    ATTENDEE_LEFT = 'attendee left',
    GAME_TICK = 'game tick',
}

export enum GameStatus {
    PLAYING = 'PLAYING',
    READY = 'READY',
    WAITING_FOR_PLAYER = 'WAITING_FOR_PLAYER',
    ENDED = 'ENDED',
}

export interface Attendee {
    userId: number;
    role: Role;
}

export interface GameUpdate {
    id: number;
    player1: Attendee | null;
    player2: Attendee | null;
    score1: number;
    score2: number;
    status: GameStatus;
}
