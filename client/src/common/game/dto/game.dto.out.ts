export interface MatchResult {
    ownScore: number;
    opponentScore: number;
    opponentUsername: string;
    opponentAvatar: string;
}
