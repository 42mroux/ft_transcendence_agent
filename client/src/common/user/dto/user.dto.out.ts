export interface User {
    id: number;
    login: string;
    username: string;
    avatar: string;
    blockedUsersId: number[] | null;
    isActive: boolean;
    isPlaying: boolean;
}

export interface UserFromDBDto {
    id: number;
    login: string;
    username: string;
    avatar: string;
}

export interface UserProfile {
    id: number;
    login: string;
    username: string;
    avatar: string;
    isActive: boolean;
    isPlaying: boolean;
    victories: number;
    losses: number;
    rank: number;
    gamesPlayed: number;
    ballHits: number;
}
