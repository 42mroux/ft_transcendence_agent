export interface CreateUserDto {
    login: string;
    firstName: string;
    lastName: string;
}
