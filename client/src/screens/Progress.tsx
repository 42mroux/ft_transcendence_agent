import React, { useContext, useEffect, useState } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { find42UserLoginFromURLCode, is2FAEnabled, isCodeValid } from '../api/axios/profileRoutes';
import { AuthContext } from '../contexts/auth.context';
import { Button, TextField } from '@mui/material';
import NotificationSnackbar from '../components/NotificationSnackbar';

export default function Progress() {
    const [login, setLogin] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [twoFACode, setTwoFACode] = useState<string>('');
    const [searchParams] = useSearchParams();
    const navigate = useNavigate();
    const auth = useContext(AuthContext);
    const [openNotifAlert, setOpenNotifAlert] = useState<boolean>(false);
    const notification = 'Wrong authentication';

    const loadUser = async (code: string): Promise<void> => {
        try {
            const loginFrom42 = await find42UserLoginFromURLCode(code);
            setLogin(loginFrom42);
            if (await is2FAEnabled(loginFrom42)) {
                setIsLoading(false);
            } else auth.logInWith42(loginFrom42);
        } catch (e) {
            navigate('/error_page');
        }
    };

    useEffect(() => {
        let code: string | null = null;
        if ((code = searchParams.get('code')) !== null) {
            loadUser(code);
        } else {
            navigate('/error_page');
        }
    }, []);

    const checkCodeAgainstSecret = async (): Promise<void> => {
        if (await isCodeValid(login, twoFACode)) auth.logInWith42(login);
        else setOpenNotifAlert(true);
    };

    return isLoading ? (
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
            }}
        >
            <CircularProgress />
        </Box>
    ) : (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                colorBackground: 'black',
                color: 'success',
            }}
        >
            <TextField
                value={twoFACode}
                label="code"
                sx={{ padding: 5 }}
                onChange={(event) => {
                    setTwoFACode(event.target.value);
                }}
            />
            <Button variant="outlined" onClick={checkCodeAgainstSecret}>
                Submit
            </Button>
            <NotificationSnackbar open={openNotifAlert} setOpen={setOpenNotifAlert} message={notification} />
        </Box>
    );
}
