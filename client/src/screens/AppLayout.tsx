import * as React from 'react';
import { styled, useTheme, Theme, CSSObject } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import { FC, useContext } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { AuthContext } from '../contexts/auth.context';
import AccountBoxRoundedIcon from '@mui/icons-material/AccountBoxRounded';
import ChatRoundedIcon from '@mui/icons-material/ChatRounded';
import SportsEsportsRoundedIcon from '@mui/icons-material/SportsEsportsRounded';
import PeopleAltRoundedIcon from '@mui/icons-material/PeopleAltRounded';
import ExitToAppRoundedIcon from '@mui/icons-material/ExitToAppRounded';
import SettingsIcon from '@mui/icons-material/Settings';
import Color from 'color';
import { chatSocket } from '../api/socketIo';
import { SnackbarContext } from '../contexts/snackbar.context';

const drawerWidth = 240;

const openedMixin = (theme: Theme): CSSObject => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});

const closedMixin = (theme: Theme): CSSObject => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(9)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

interface AppBarProps extends MuiAppBarProps {
    open?: boolean;
}

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    ...(open && {
        ...openedMixin(theme),
        '& .MuiDrawer-paper': openedMixin(theme),
    }),
    ...(!open && {
        ...closedMixin(theme),
        '& .MuiDrawer-paper': closedMixin(theme),
    }),
}));

interface MenuItemsType {
    title: string;
    icon: JSX.Element;
    route: string;
}

const menuItems: MenuItemsType[] = [
    {
        title: 'Profile',
        icon: <AccountBoxRoundedIcon />,
        route: '/profile',
    },
    {
        title: 'Chat',
        icon: <ChatRoundedIcon />,
        route: '/chat',
    },
    {
        title: 'Friends',
        icon: <PeopleAltRoundedIcon />,
        route: '/friends',
    },
    {
        title: 'Game',
        icon: <SportsEsportsRoundedIcon />,
        route: '/game',
    },
    {
        title: 'Settings',
        icon: <SettingsIcon />,
        route: '/settings',
    },
];

const AppLayout: FC = ({ children }) => {
    const [open, setOpen] = React.useState<boolean>(false);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [selected, setSelected] = React.useState<number>(0);
    const [pageName, setPageName] = React.useState<string>('Profile');
    const [drawerOptionsDisabled, setDrawerOptionsDisabled] = React.useState<boolean>(false);
    const snackbarContext = useContext(SnackbarContext);
    const theme = useTheme();
    const navigate = useNavigate();
    const auth = useContext(AuthContext);
    const location = useLocation();

    React.useEffect(() => {
        const isSelected = (element: MenuItemsType) => element.route === location.pathname;
        if (location.pathname.startsWith('/game/')) {
            const index = menuItems.findIndex((element) => element.route === '/game');
            setSelected(index);
            setPageName('Game');
        } else {
            menuItems.forEach((item) => {
                if (item.route === location.pathname) {
                    const index = menuItems.findIndex(isSelected);
                    setSelected(index);
                    setPageName(item.title);
                }
            });
        }
    }, [location.pathname]);

    React.useEffect(() => {
        setDrawerOptionsDisabled(auth.isUserNew);
    }, [auth.isUserNew]);

    React.useEffect(() => {
        if (chatSocket.connected)
            chatSocket.on('invite', (data: { message: string; gameId: number }) => {
                snackbarContext.showSnackbar(data.message, 'success', () => navigate(`/game/${data.gameId}`), 'Join');
            });
        else chatSocket.off('invite');
    }, [chatSocket.connected]);
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleMenuItemOpen = (item: MenuItemsType, index: number) => {
        navigate(item.route);
        setSelected(index);
        setPageName(item.title.toUpperCase());
    };

    return (
        <Box sx={{ display: 'flex', minHeight: '100vh' }}>
            <AppBar position="fixed" open={open}>
                <Toolbar
                    sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        background: `linear-gradient(90deg,  ${theme.palette.primary.main} 0%, #FFFFFF 100%);`,
                    }}
                >
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{
                            marginRight: '36px',
                            ...(open && { display: 'none' }),
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6">{pageName}</Typography>
                    <Avatar alt="img" src={auth.avatar} sx={{ width: 40, height: 40 }} onClick={handleMenu} />
                    <Menu
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        <MenuItem onClick={auth.logout}>Log out</MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    {menuItems.map((item, index) => (
                        <ListItem
                            button
                            disabled={drawerOptionsDisabled}
                            key={index}
                            onClick={() => {
                                handleMenuItemOpen(item, index);
                            }}
                            selected={index === selected}
                        >
                            <ListItemIcon>{item.icon}</ListItemIcon>
                            <ListItemText primary={item.title} />
                        </ListItem>
                    ))}
                    <Divider />
                    <ListItem button onClick={auth.logout}>
                        <ListItemIcon>
                            <ExitToAppRoundedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Log out" />
                    </ListItem>
                </List>
            </Drawer>
            <Box
                component="main"
                sx={{
                    flexGrow: 1,
                    px: 2,
                    py: 1,
                    background: `linear-gradient(-45deg, ${Color(theme.palette.primary.main).lightness(95).hex()} 0%,
						${Color(theme.palette.primary.main).lightness(98).hex()} 50%, #FFFFFF 100%);`, // '#eff4fa',
                    display: 'flex',
                    flexDirection: 'column',
                }}
            >
                <DrawerHeader />
                {children}
            </Box>
        </Box>
    );
};

export default AppLayout;
