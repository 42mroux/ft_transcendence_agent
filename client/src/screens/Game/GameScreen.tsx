import React, { useContext, useEffect, useState } from 'react';
import { Button, Snackbar, Box, Card, Avatar, styled } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import { getCurrentGames, getGameUsers, getPlayers } from '../../api/axios/gameRoutes';
import { GameListDialog } from '../../components/Game/GameListDialog';
import { GameResultType } from '../../components/Game/GameResult';
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded';
import AddIcon from '@mui/icons-material/Add';
import JoinInnerIcon from '@mui/icons-material/JoinInner';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import BoltIcon from '@mui/icons-material/Bolt';
import { GameDisplay } from './GameDisplay';
import { UserFromDBDto } from '../../common/user/dto/user.dto.out';
import { leaveGame } from '../../api/socketIo/gameMessages';
import { DialogContext } from '../../contexts/dialog.context';
import { GameStatus } from '../../common/game/types/game.types';
import { AuthContext } from '../../contexts/auth.context';

const GameScreen = (): JSX.Element => {
    const navigate = useNavigate();
    const params = useParams();
    const [openGameListDialog, setOpenGameListDialog] = useState(false);
    const [currentGames, setCurrentGames] = useState<GameResultType[]>([]);
    const [message, setMessage] = useState('');
    const [gameUsers, setGameUsers] = useState<UserFromDBDto[]>([]);
    const dialogContext = useContext(DialogContext);
    const openGames = currentGames?.filter((game) => game.status === GameStatus.WAITING_FOR_PLAYER);
    const authContext = useContext(AuthContext);

    const onGameClick = (gameId: number) => {
        setOpenGameListDialog(false);
        navigate(`/game/${gameId}`, { replace: true });
    };

    const reFetchGameUsers = async () => {
        if (!params?.gameId) setGameUsers([]);
        else {
            const users = await getGameUsers(parseInt(params.gameId));
            setGameUsers(users);
        }
    };
    const goFastGame = () => {
        const randomId = Math.round(Math.random() * (openGames?.length - 1));
        navigate(`/game/${openGames?.[randomId]?.id}`, { replace: true });
    };

    const refetchGames = async () => {
        const gameList = await getCurrentGames();
        setCurrentGames(gameList);
    };

    useEffect(() => {
        const fetch = async () => {
            try {
                await reFetchGameUsers();
                await refetchGames();
            } catch (e) {
                setCurrentGames([]);
                setGameUsers([]);
                setMessage('An error occured during loading');
            }
        };
        fetch();
    }, [params.gameId]);

    const handleLeaveGame = async () => {
        try {
            if (params?.gameId && gameUsers?.length >= 2) {
                const players = await getPlayers(parseInt(params.gameId));
                leaveGame(parseInt(params?.gameId));
                if (!players.find((player) => player.id === authContext.userId)) navigate('/game', { replace: true });
            } else navigate('/game', { replace: true });
        } catch (e) {
            navigate('/game', { replace: true });
        }
    };

    const Div = styled('div')(({ theme }) => ({
        ...theme.typography.button,
        backgroundColor: theme.palette.background.paper,
        color: 'gray',
        fontSize: 20,
        borderBlockEnd: '1px solid',
        borderBlockStart: '1px solid',
        borderColor: 'gray',
        lineHeight: 0.5,
        padding: theme.spacing(1),
        marginRight: 10,
    }));

    return (
        <>
            <div
                style={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 1,
                    flexDirection: 'column',
                }}
            >
                <Box
                    style={{
                        display: 'flex',
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 2,
                    }}
                >
                    {!params.gameId && (
                        <Box>
                            {openGames?.length > 0 && (
                                <Button variant="contained" onClick={goFastGame}>
                                    <BoltIcon sx={{ paddingRight: 1 }} />
                                    Fast Game
                                </Button>
                            )}
                            <Button
                                variant="contained"
                                onClick={() => dialogContext.setGameCreationDialogProps({ open: true })}
                                sx={{ m: 1 }}
                            >
                                <AddIcon sx={{ paddingRight: 1 }} />
                                New Game
                            </Button>
                            <Button variant="contained" onClick={() => setOpenGameListDialog(true)}>
                                <JoinInnerIcon sx={{ paddingRight: 1 }} /> Join Game
                            </Button>
                        </Box>
                    )}
                    {params.gameId && (
                        <GameDisplay
                            gameId={params.gameId}
                            setMessage={setMessage}
                            refetchUsers={async () => {
                                try {
                                    reFetchGameUsers();
                                } catch (e) {
                                    setGameUsers([]);
                                    setMessage('Error while fetching game users');
                                }
                            }}
                            gameUsers={gameUsers}
                        />
                    )}
                </Box>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                    <div style={{ flex: 1 }} />
                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', flex: 3 }}>
                        {!!gameUsers?.length && (
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', flex: 1 }}>
                                <Card
                                    sx={{
                                        my: 1,
                                        display: 'flex',
                                        flexDirection: 'row',
                                        justifyContent: 'space-around',
                                    }}
                                    elevation={5}
                                >
                                    <Box
                                        sx={{
                                            flex: 1,
                                            display: 'flex',
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'start',
                                            boxShadow: 3,
                                            borderRadius: 1,
                                        }}
                                    >
                                        <GroupsRoundedIcon sx={{ m: 1, color: 'gray' }} />
                                        <Div>Attendees</Div>
                                    </Box>
                                    <div
                                        style={{
                                            display: 'flex',
                                            flex: 3,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            // flexWrap: 'wrap',
                                            padding: 4,
                                        }}
                                    >
                                        {gameUsers
                                            .sort((u1, u2) => u1.username.localeCompare(u2.username))
                                            .map((user) => (
                                                <Avatar
                                                    key={user.id}
                                                    alt="Room image"
                                                    src={user.avatar}
                                                    sx={{ m: 1 }}
                                                />
                                            ))}
                                    </div>
                                </Card>
                            </div>
                        )}
                        {params.gameId && (
                            <div
                                style={{
                                    flex: 1,
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Button
                                    variant="contained"
                                    color="primary"
                                    aria-label="add"
                                    size="medium"
                                    onClick={handleLeaveGame}
                                >
                                    <LogoutRoundedIcon sx={{ p: 1 }} /> Leave
                                </Button>
                            </div>
                        )}
                    </div>
                    <div style={{ flex: 1 }} />
                </div>
            </div>
            <GameListDialog
                open={openGameListDialog}
                onClose={() => setOpenGameListDialog(false)}
                onGameClick={onGameClick}
                games={currentGames}
                refetchGames={async () => {
                    try {
                        await refetchGames();
                    } catch (e) {
                        setCurrentGames([]);
                        setMessage('An error occured while loading games');
                    }
                }}
                currentGameId={params.gameId ? parseInt(params.gameId) : undefined}
            />
            <Snackbar open={!!message} autoHideDuration={5000} onClose={() => setMessage('')} message={message} />
        </>
    );
};

export default GameScreen;
