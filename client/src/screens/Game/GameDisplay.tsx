import { Avatar, Card, Typography } from '@mui/material';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getGameUpdate, getPlayers, getSettings } from '../../api/axios/gameRoutes';
import { joinGame, leaveGame } from '../../api/socketIo/gameMessages';
import { GameSettings, GameStatus, GameUpdate } from '../../common/game/types/game.types';
import { UserFromDBDto } from '../../common/user/dto/user.dto.out';
import { ResultEnum } from '../../components/Game/GameResultDialog';
import GameScenery from '../../components/Game/threejs/GameScenery';
import { AuthContext } from '../../contexts/auth.context';
import { DialogContext } from '../../contexts/dialog.context';

interface GameDisplayProps {
    gameId: string;
    setMessage: (mssg: string) => void;
    refetchUsers: () => Promise<void>;
    gameUsers: UserFromDBDto[];
}

export interface gameUpdateData {
    update: GameUpdate;
    attendees: UserFromDBDto[];
}

export const GameDisplay = (props: GameDisplayProps) => {
    const [sceneWidth, setSceneWidth] = useState<number>();
    const [loading, setLoading] = useState(true);
    const [settings, setSettings] = useState<GameSettings>();
    const [score, setScore] = useState<number[]>([]);
    const [status, setStatus] = useState('');
    const [players, setPlayers] = useState<UserFromDBDto[]>([]);
    const [scoreColors, setScoreColors] = useState<string[]>(['black', 'black']);
    const navigate = useNavigate();
    const sceneRef = useRef<HTMLDivElement>(null);
    const auth = useContext(AuthContext);
    const dialogContext = useContext(DialogContext);

    const setSceneDimensions = (): void => {
        if (sceneRef.current) {
            if (sceneRef.current.offsetWidth < 2 * sceneRef.current.offsetHeight) {
                setSceneWidth(sceneRef.current.offsetWidth);
            } else {
                setSceneWidth(2 * sceneRef.current.offsetHeight - 50);
            }
        }
    };

    const fetchPlayers = async () => {
        const pl = await getPlayers(parseInt(props.gameId));
        setPlayers(pl);
    };

    const fetchSettings = async () => {
        const newSettings = await getSettings(parseInt(props.gameId));
        setSettings(newSettings);
    };

    useEffect(() => {
        if (score?.length < 2 || (score?.[0] == 0 && score?.[1] == 0)) setScoreColors(['black', 'black']);
        else if (score?.[0] > score?.[1]) setScoreColors(['green', 'red']);
        else if (score?.[0] < score?.[1]) setScoreColors(['red', 'green']);
        else setScoreColors(['orange', 'orange']);
    }, [score]);

    const onJoinGame = async (info: { gameId?: number; error?: string }) => {
        try {
            if (!info?.gameId) throw new Error();
            const { score, status } = await getGameUpdate(parseInt(props.gameId));
            setScore(score);
            setStatus(status);
            await fetchPlayers();
            await fetchSettings();
            setLoading(false);
        } catch (e) {
            setPlayers([]);
            setSettings(undefined);
            setScore([]);
            setStatus('');
            navigate('/game', { replace: true });
        }
    };

    useEffect(() => {
        if (!props.gameId) return;
        else {
            try {
                joinGame({ gameId: parseInt(props.gameId) }, onJoinGame);
            } catch (e) {
                leaveGame(parseInt(props.gameId as string));
            }
            return () => {
                leaveGame(parseInt(props.gameId as string));
            };
        }
    }, [props.gameId]);

    useEffect(() => {
        setSceneDimensions();
        window.addEventListener('resize', () => {
            setSceneDimensions();
        });
    }, []);

    const showWinner = ({ update: { score1, score2, player1, player2 }, attendees }: gameUpdateData) => {
        if (score1 == score2) props.setMessage('The game ended with a draw');
        else {
            const winnerId = score1 > score2 ? player1?.userId : player2?.userId;
            const looserId = score1 < score2 ? player1?.userId : player2?.userId;
            if (auth.userId == winnerId)
                dialogContext.setGameResultDialogProps({
                    open: true,
                    result: score1 == -1 || score2 == -1 ? ResultEnum.WIN_DEFAULT : ResultEnum.WIN,
                });
            else if (auth.userId == looserId)
                dialogContext.setGameResultDialogProps({
                    open: true,
                    result: score1 == -1 || score2 == -1 ? ResultEnum.LOSE_DEFAULT : ResultEnum.LOSE,
                });
            else {
                const winnerUsername = attendees.find((attendee) => attendee.id == winnerId)?.username;
                props.setMessage(`${winnerUsername} ` + (score1 == -1 || score2 == -1 ? 'won by default !' : 'won !'));
            }
        }
    };

    const onGameUpdate = async ({ update, attendees }: gameUpdateData) => {
        try {
            setScore([update?.score1, update?.score2]);
            setStatus(update.status);
            if (update.status == GameStatus.ENDED) showWinner({ update, attendees });
            await props.refetchUsers();
            await fetchPlayers();
        } catch (e) {}
    };
    const onAttendeeArrived = async (username: string) => {
        try {
            props.setMessage(`${username} just arrived !`);
            await props.refetchUsers();
        } catch (e) {}
    };
    const onAttendeeLeft = async (username: string) => {
        try {
            props.setMessage(`${username} just left !`);
            await props.refetchUsers();
        } catch (e) {}
    };
    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    width: '100%',
                }}
            >
                <div style={{ flex: 1 }} />
                <div style={{ flex: 3 }}>
                    <Card
                        sx={{
                            display: 'flex',
                            width: '100%',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            m: 2,
                            py: 1,
                        }}
                    >
                        <Avatar
                            src={players[0]?.avatar}
                            sx={{
                                width: 50,
                                height: 50,
                                marginLeft: { xs: 2, md: 10 },
                                border: 2,
                                animationName: `glow-${scoreColors[0]}`,
                                animationDuration: '1s',
                                animationIterationCount: 'infinite',
                                animationDirection: 'alternate',
                                '@keyframes glow-black': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'black' },
                                },
                                '@keyframes glow-orange': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'orange' },
                                },
                                '@keyframes glow-red': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'red' },
                                },
                                '@keyframes glow-green': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'green' },
                                },
                            }}
                        />

                        <Typography sx={{ flex: 1, color: scoreColors[0] }} align="center" variant="h3">
                            {score?.[0] == -1 ? 'X' : score?.[0]}
                        </Typography>
                        <Typography sx={{ flex: 1 }} align="center" variant="h3">
                            -
                        </Typography>
                        <Typography sx={{ flex: 1, color: scoreColors[1] }} align="center" variant="h3">
                            {status != GameStatus.WAITING_FOR_PLAYER && (score?.[1] == -1 ? 'X' : score?.[1])}
                        </Typography>

                        <Avatar
                            src={players[1]?.avatar}
                            sx={{
                                width: 50,
                                height: 50,
                                marginRight: { xs: 2, md: 10 },
                                border: 2,
                                animationName: `glow-${scoreColors[1]}`,
                                animationDuration: '1s',
                                animationIterationCount: 'infinite',
                                animationDirection: 'alternate',
                                '@keyframes glow-black': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'black' },
                                },
                                '@keyframes glow-orange': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'orange' },
                                },
                                '@keyframes glow-red': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'red' },
                                },
                                '@keyframes glow-green': {
                                    from: { borderColor: 'transparent' },
                                    to: { borderColor: 'green' },
                                },
                            }}
                        />
                    </Card>
                </div>
                <div style={{ flex: 1 }} />
            </div>
            <div
                ref={sceneRef}
                style={{
                    display: 'flex',
                    width: '100%',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                {props.gameId && settings && sceneWidth && !loading && (
                    <GameScenery
                        sceneWidth={sceneWidth * 0.99}
                        gameInfo={{ gameId: parseInt(props.gameId) }}
                        onGameUpdate={onGameUpdate}
                        onAttendeeArrived={onAttendeeArrived}
                        onAttendeeLeft={onAttendeeLeft}
                        settings={settings}
                    ></GameScenery>
                )}
            </div>
        </div>
    );
};
