import React, { useContext, useEffect, useState } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { is2FAEnabled, isCodeValid } from '../api/axios/profileRoutes';
import { AuthContext } from '../contexts/auth.context';
import { Button, TextField, Typography } from '@mui/material';
import NotificationSnackbar from '../components/NotificationSnackbar';
import { useNavigate, useSearchParams } from 'react-router-dom';
import Background from '../components/Background';

export default function ProgressLogin() {
    const [login, setLogin] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [twoFACode, setTwoFACode] = useState<string>('');
    const auth = useContext(AuthContext);
    const [openNotifAlert, setOpenNotifAlert] = useState<boolean>(false);
    const notification = 'Wrong authentication';
    const [searchParams] = useSearchParams();
    const navigate = useNavigate();

    const loadUser = async (login: string): Promise<void> => {
        if (await is2FAEnabled(login)) {
            setIsLoading(false);
        } else auth.logInWith42(login);
    };

    useEffect(() => {
        let newLogin: string | null;
        if ((newLogin = searchParams.get('login')) !== null) {
            setLogin(newLogin);
            loadUser(newLogin);
        }
    }, []);

    const checkCodeAgainstSecret = async (): Promise<void> => {
        if (await isCodeValid(login, twoFACode)) auth.logInWith42(login);
        else setOpenNotifAlert(true);
    };

    return isLoading ? (
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '90vh',
            }}
        >
            <CircularProgress />
        </Box>
    ) : (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                height: '90vh',
                m: 10,
            }}
        >
            <TextField
                value={twoFACode}
                label="code"
                sx={{ padding: 5 }}
                onChange={(event) => {
                    setTwoFACode(event.target.value);
                }}
            />
            <Button variant="outlined" onClick={checkCodeAgainstSecret}>
                Submit
            </Button>
            <Typography m={2}>OR</Typography>
            <Box sx={{ flex: 1 }}>
                <Button
                    variant="outlined"
                    color="success"
                    onClick={() => {
                        navigate('/');
                    }}
                >
                    Go back to log in page
                </Button>
            </Box>
            <NotificationSnackbar open={openNotifAlert} setOpen={setOpenNotifAlert} message={notification} />
            <Background />
        </Box>
    );
}
