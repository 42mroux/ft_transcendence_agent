import { Box, TextField, Button, styled, Typography, IconButton, Stack, Avatar } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import Background from '../components/Background';
import NotificationSnackbar from '../components/NotificationSnackbar';
import { AuthContext } from '../contexts/auth.context';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import FileDownloadRoundedIcon from '@mui/icons-material/FileDownloadRounded';
import FaceRetouchingNaturalRoundedIcon from '@mui/icons-material/FaceRetouchingNaturalRounded';
import QRCode from 'react-qr-code';
import AvatarDropzoneDialog from '../components/Settings/AvatarDropzoneDialog';
import AvatarGeneratorDialog from '../components/Settings/AvatarGeneratorDialog';
import { deleteUploadedAvatar, generateRandomAvatar, storeUserAvatar } from '../api/axios/profileRoutes';
import DeleteIcon from '@mui/icons-material/Delete';
import { useMediaQuery } from 'react-responsive';

const Settings = (): JSX.Element => {
    const auth = useContext(AuthContext);
    const [username, setUsername] = useState<string>(auth.username);
    const [avatar, setAvatar] = useState<string>(auth.avatar);
    const [uploadedAvatar, setUploadedAvatar] = useState<string>('');
    const [generatedAvatar, setGeneratedAvatar] = useState<string>('');
    const [openUploadAvatar, setOpenUploadAvatar] = useState<boolean>(false);
    const [openGenerateAvatar, setOpenGenerateAvatar] = useState<boolean>(false);
    const [enable2FA, setEnable2FA] = useState<boolean>(auth.twoFAEnabled);
    const [openFinalNotification, setOpenFinalNotification] = useState<boolean>(false);
    const [finalNotification, setFinalNotification] = useState<string>('');
    const [usernameError, setUsernameError] = useState<string>('');
    const [submitDisabled, setSubmitDisabled] = useState<boolean>(true);
    const [twoFAQRCode, setTwoFAQRCode] = useState<string>('');
    const notifications: string[] = [];
    const [titleFontSize, setTitleFontSize] = useState<string>('3vw');
    const [subtitleFontSize, setSubtitleFontSize] = useState<string>('1.5vw');
    const [margins, setMargins] = useState<number>(5);

    const handleScreenSizeChange = (matches: boolean) => {
        if (matches) {
            setTitleFontSize('6vw');
            setSubtitleFontSize('2vw');
            setMargins(1);
        } else {
            setTitleFontSize('3vw');
            setSubtitleFontSize('1.5vw');
            setMargins(5);
        }
    };

    const isMobile = useMediaQuery({ maxWidth: 500 }, undefined, handleScreenSizeChange);
    useEffect(() => {
        if (isMobile) {
            setTitleFontSize('6vw');
            setSubtitleFontSize('2vw');
            setMargins(1);
        }
    }, []);

    useEffect(() => {
        if (
            (username === auth.username && avatar === auth.avatar && enable2FA === auth.twoFAEnabled) ||
            (auth.isUserNew && username === '') ||
            (username === auth.username &&
                enable2FA === auth.twoFAEnabled &&
                avatar !== auth.avatar &&
                uploadedAvatar === '' &&
                generatedAvatar === '') ||
            (auth.isUserNew && username === '')
        )
            setSubmitDisabled(true);
        else setSubmitDisabled(false);
    }, [username, auth.username, avatar, enable2FA, auth.twoFAEnabled, uploadedAvatar, generatedAvatar]);

    useEffect(() => {
        if (uploadedAvatar !== '') {
            setGeneratedAvatar('');
            setAvatar(uploadedAvatar);
        }
    }, [uploadedAvatar]);

    useEffect(() => {
        if (generatedAvatar !== '') {
            setUploadedAvatar('');
            setAvatar(generatedAvatar);
        }
    }, [generatedAvatar]);

    const updateUsername = async () => {
        if (username.length === 0 || username.length > 20 || username.match(/[%<>\\$'"?!&@ (){}§€`£+=\-/:;.,]/)) {
            setUsername(auth.username);
            setUsernameError(
                'Username should be between 1 and 20 characters long, only letters, underscore (_) and numbers are authorized',
            );
            notifications.push('Username not modified, sorry!');
        } else {
            if (await auth.updateUsername(username)) {
                notifications.push(`Username changed to ${username}!`);
            } else {
                setUsername(auth.username);
                setUsernameError('This username is not unique, please enter another one!');
                notifications.push('Username not modified, sorry!');
            }
        }
    };

    const updateUserAvatar = async (newAvatar: string) => {
        if ((await storeUserAvatar(newAvatar)) === false) {
            notifications.push('There was an issue when storing your avatar, please select a new one!');
            if (uploadedAvatar !== '') {
                await deleteUploadedAvatar(uploadedAvatar);
                setUploadedAvatar('');
            }
        } else {
            notifications.push('Avatar modified!');
            setUploadedAvatar('');
            setGeneratedAvatar('');
        }
    };

    const enable2FALoadQRCode = async () => {
        const newQRCode = await auth.enable2FA();
        if (newQRCode) {
            setTwoFAQRCode(newQRCode);
            notifications.push('Scan the QR code to store your secret code, you will need it when logging in!');
        }
    };

    const disable2FA = async () => {
        await auth.disable2FA();
        setTwoFAQRCode('');
        notifications.push('You no longer need a secret code to log in!');
    };

    const updateUserData = async () => {
        if (username !== auth.username) await updateUsername();
        if (auth.isUserNew && uploadedAvatar === '' && generatedAvatar === '') {
            const defaultAvatar = await generateRandomAvatar();
            setAvatar(defaultAvatar);
            await updateUserAvatar(defaultAvatar);
        } else if (avatar !== auth.avatar && (uploadedAvatar !== '' || generatedAvatar !== ''))
            await updateUserAvatar(avatar);
        try {
            if (auth.twoFAEnabled && !enable2FA) await disable2FA();
            else if (!auth.twoFAEnabled && enable2FA) await enable2FALoadQRCode();
        } catch (e) {
            notifications.push('There was an issue when updating your privacy preferences, please try again!');
        }
        setFinalNotification(notifications.join(' '));
        setOpenFinalNotification(true);
        auth.storeUserData();
    };

    const handleOpenUploadAvatar = () => {
        setOpenUploadAvatar(true);
    };

    const handleRemoveUploadedAvatar = async () => {
        setUploadedAvatar('');
        setAvatar(auth.avatar);
        await deleteUploadedAvatar(uploadedAvatar);
    };

    const handleCloseUploadAvatar = () => {
        setOpenUploadAvatar(false);
    };

    const handleOpenGenerateAvatar = () => {
        setOpenGenerateAvatar(true);
    };

    const handleRemoveGeneratedAvatar = () => {
        setGeneratedAvatar('');
        setAvatar(auth.avatar);
    };

    const handleCloseGenerateAvatar = () => {
        setOpenGenerateAvatar(false);
    };

    const IOSSwitch = styled((props) => <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />)(
        ({ theme }) => ({
            width: 42,
            height: 26,
            padding: 0,
            '& .MuiSwitch-switchBase': {
                padding: 0,
                margin: 2,
                transitionDuration: '300ms',
                '&.Mui-checked': {
                    transform: 'translateX(16px)',
                    color: '#fff',
                    '& + .MuiSwitch-track': {
                        backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                        opacity: 1,
                        border: 0,
                    },
                    '&.Mui-disabled + .MuiSwitch-track': {
                        opacity: 0.5,
                    },
                },
                '&.Mui-focusVisible .MuiSwitch-thumb': {
                    color: '#33cf4d',
                    border: '6px solid #fff',
                },
                '&.Mui-disabled .MuiSwitch-thumb': {
                    color: theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[600],
                },
                '&.Mui-disabled + .MuiSwitch-track': {
                    opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
                },
            },
            '& .MuiSwitch-thumb': {
                boxSizing: 'border-box',
                width: 22,
                height: 22,
            },
            '& .MuiSwitch-track': {
                borderRadius: 26 / 2,
                backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
                opacity: 1,
                transition: theme.transitions.create(['background-color'], {
                    duration: 500,
                }),
            },
        }),
    );

    return (
        <Box sx={{ display: 'flex' }}>
            <Box
                sx={{
                    flex: 1,
                    flexGrow: 1,
                    display: 'flex',
                    flexDirection: 'column',
                    width: 500,
                    maxWidth: '100%',
                    m: margins,
                }}
            >
                <h1 style={{ marginBlockStart: 0, marginBlockEnd: 0, fontSize: titleFontSize }}>Choose a username</h1>
                <h3 style={{ display: 'flex', fontSize: subtitleFontSize }}>
                    (only letters, numbers and underscore (_) authorized)
                </h3>
                <Box sx={{ marginBlockStart: 3, marginBlockEnd: 3 }}>
                    <TextField
                        // fullWidth
                        value={username}
                        label="username"
                        required
                        onChange={(event) => {
                            setUsername(event.target.value);
                            if (usernameError !== '') setUsernameError('');
                        }}
                        helperText={usernameError}
                        error={!!usernameError}
                    />
                </Box>
                <h1 style={{ marginBlockStart: 50, marginBlockEnd: 0, fontSize: titleFontSize }}>
                    Upload or generate an avatar
                </h1>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        marginBlockStart: 3,
                        marginBlockEnd: 3,
                        flexWrap: 'wrap',
                    }}
                >
                    <Box sx={{ marginLeft: margins }}>
                        {avatar !== auth.avatar && avatar === uploadedAvatar ? (
                            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <IconButton sx={{ height: '125px', width: '125px' }} onClick={handleOpenUploadAvatar}>
                                    <Avatar alt="img" src={uploadedAvatar} sx={{ height: '100px', width: '100px' }} />
                                </IconButton>
                                <Button
                                    variant="outlined"
                                    startIcon={<DeleteIcon />}
                                    sx={{ m: 1 }}
                                    onClick={handleRemoveUploadedAvatar}
                                >
                                    Delete
                                </Button>
                            </Box>
                        ) : (
                            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <IconButton sx={{ height: '125px', width: '125px' }} onClick={handleOpenUploadAvatar}>
                                    <FileDownloadRoundedIcon sx={{ height: '100px', width: '100px' }} />
                                </IconButton>
                            </Box>
                        )}
                    </Box>
                    <AvatarDropzoneDialog
                        open={openUploadAvatar}
                        onClose={handleCloseUploadAvatar}
                        uploadedAvatar={uploadedAvatar}
                        setUploadedAvatar={setUploadedAvatar}
                    />
                    <Box sx={{ marginLeft: margins }}>
                        {avatar !== auth.avatar && avatar === generatedAvatar ? (
                            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <IconButton sx={{ height: '125px', width: '125px' }} onClick={handleOpenGenerateAvatar}>
                                    <Avatar alt="img" src={generatedAvatar} sx={{ height: '100px', width: '100px' }} />
                                </IconButton>
                                <Button
                                    variant="outlined"
                                    startIcon={<DeleteIcon />}
                                    sx={{ m: 1 }}
                                    onClick={handleRemoveGeneratedAvatar}
                                >
                                    Delete
                                </Button>
                            </Box>
                        ) : (
                            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <IconButton sx={{ height: '125px', width: '125px' }} onClick={handleOpenGenerateAvatar}>
                                    <FaceRetouchingNaturalRoundedIcon sx={{ height: '100px', width: '100px' }} />
                                </IconButton>
                            </Box>
                        )}
                    </Box>
                    <AvatarGeneratorDialog
                        open={openGenerateAvatar}
                        onClose={handleCloseGenerateAvatar}
                        generatedAvatar={generatedAvatar}
                        setGeneratedAvatar={setGeneratedAvatar}
                    />
                </Box>
                <h1 style={{ marginBlockStart: 50, marginBlockEnd: 0, fontSize: titleFontSize }}>
                    Select your privacy preferences
                </h1>
                <Box sx={{ display: 'flex', flexDirection: 'column', marginBlockStart: 3, marginBlockEnd: 5 }}>
                    <Box sx={{ display: 'flex', flexDirection: 'row' }}>
                        <Box sx={{ m: 2 }}>
                            <Typography>Enable two factors authentication </Typography>
                        </Box>
                        <Box>
                            <FormControlLabel
                                control={<IOSSwitch sx={{ m: 2 }} />}
                                label=""
                                onChange={(event: React.SyntheticEvent, checked: boolean) => {
                                    if (event) setEnable2FA(checked);
                                }}
                                checked={enable2FA}
                            />
                        </Box>
                    </Box>
                    <Box sx={{ m: 2 }}>{twoFAQRCode !== '' && <QRCode value={twoFAQRCode} />}</Box>
                </Box>
                <Button
                    variant="outlined"
                    sx={{ maxWidth: 100 }}
                    onClick={async () => {
                        await updateUserData();
                    }}
                    disabled={submitDisabled}
                >
                    Submit
                </Button>

                <Stack sx={{ width: '100%' }} spacing={2}>
                    <NotificationSnackbar
                        open={openFinalNotification}
                        setOpen={setOpenFinalNotification}
                        message={finalNotification}
                    />
                </Stack>
            </Box>
            <Background />
        </Box>
    );
};

export default Settings;
