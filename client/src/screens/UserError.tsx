import { Box, Button, Typography } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import Background from '../components/Background';
import WarningRoundedIcon from '@mui/icons-material/WarningRounded';

const UserError = (): JSX.Element => {
    const navigate = useNavigate();
    return (
        <div>
            <Box sx={{ display: 'flex', flexDirection: 'column', m: 10, alignItems: 'center', height: '90vh' }}>
                <WarningRoundedIcon sx={{ flex: 1, height: '200px', width: '200px' }} color="error" />
                <Typography flex={1} variant="h3">
                    It looks like you don&apos;t have access to 42 intra
                </Typography>
                <Box sx={{ flex: 1 }}>
                    <Button
                        variant="outlined"
                        color="success"
                        onClick={() => {
                            navigate('/');
                        }}
                    >
                        Go back to log in page
                    </Button>
                </Box>
            </Box>
            <Background />
        </div>
    );
};

export default UserError;
