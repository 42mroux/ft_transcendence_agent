import { Box } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import { getUserProfile } from '../api/axios/profileRoutes';
import { UserProfile } from '../common/user/dto/user.dto.out';
import MatchHistory from '../components/Profile/MatchHistory';
import PlayerScores from '../components/Profile/PlayerScores';
import ProfileCard from '../components/Profile/ProfileCard';
import { AuthContext } from '../contexts/auth.context';
import { FriendsContext } from '../contexts/friends.context';
import { SnackbarContext } from '../contexts/snackbar.context';

const Profile = (): JSX.Element => {
    const auth = useContext(AuthContext);
    const [searchParams] = useSearchParams();
    const [userProfile, setUserProfile] = useState<UserProfile>();
    const friendLogin = searchParams.get('friend-login');
    const snackbarContext = useContext(SnackbarContext);
    const friendsContext = useContext(FriendsContext);

    const loadData = async () => {
        try {
            const login = friendLogin !== null ? friendLogin : auth.login;
            const user = await getUserProfile(login);
            setUserProfile(user);
        } catch (e) {
            setUserProfile(undefined);
            snackbarContext.showSnackbar('Error occured while loading', 'error');
        }
    };

    useEffect(() => {
        loadData();
    }, [friendsContext.friends, friendLogin]);

    return (
        <Box
            sx={{
                flexGrow: 1,
                flex: 1,
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
            }}
        >
            <Box sx={{ flexGrow: 0.5 }} />
            {userProfile && (
                <Box sx={{ display: 'flex', flex: 1, flexGrow: 3, flexDirection: 'column' }}>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flex: 1,
                            flexGrow: 1,
                            flexDirection: 'row',
                        }}
                    >
                        <ProfileCard
                            id={userProfile.id}
                            login={userProfile.login}
                            username={userProfile.username}
                            avatar={userProfile.avatar}
                            isActive={userProfile.isActive}
                            isPlaying={userProfile.isPlaying}
                        />

                        <MatchHistory userScores={userProfile} />
                    </Box>
                    <PlayerScores userScores={userProfile} />
                </Box>
            )}
            <Box sx={{ flexGrow: 0.5 }} />
        </Box>
    );
};

export default Profile;
