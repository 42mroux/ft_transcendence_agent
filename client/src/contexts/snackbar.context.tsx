import React, { createContext, useState, useCallback } from 'react';
import { Snackbar, AlertColor, Button } from '@mui/material';

interface SnackbarContextProps {
    showSnackbar: (text: string, type: AlertColor, action?: () => void, actionText?: string) => void;
}

interface SnackPropsType {
    text: string;
    type: AlertColor;
    action?: () => void;
    actionText?: string;
}

export const SnackbarContext = createContext<SnackbarContextProps>({} as SnackbarContextProps);

const SnackbarProvider = ({ children }: { children: JSX.Element }): JSX.Element => {
    const [snackIsVisible, setSnackIsVisible] = useState<boolean>(false);
    const [snackProps, setSnackProps] = useState<SnackPropsType>({ text: '', type: 'error' });

    const showSnackbar = useCallback(
        (text: string, type: AlertColor, action?: () => void, actionText?: string): void => {
            setSnackIsVisible(false);
            setSnackProps({ text, type, action, actionText });
            setSnackIsVisible(true);
        },
        [],
    );
    const handleAction = () => {
        snackProps.action && snackProps.action();
        setSnackIsVisible(false);
    };

    return (
        <SnackbarContext.Provider value={{ showSnackbar }}>
            {children}
            <Snackbar
                open={snackIsVisible}
                autoHideDuration={6000}
                onClose={() => setSnackIsVisible(false)}
                message={snackProps.text}
                action={
                    <Button color="inherit" size="small" onClick={handleAction}>
                        {snackProps.actionText ? 'Join' : 'OK'}
                    </Button>
                }
            />
        </SnackbarContext.Provider>
    );
};

export default SnackbarProvider;
