import React, { FC, useEffect, useState } from 'react';
import {
    generateJWT,
    generateAndStoreOTPSecret,
    getBlockedUsersId,
    getLoginFromJWT,
    getUser,
    is2FAEnabled,
    remove2FA,
    storeUniqueUsername,
    getUserIdByLogin,
} from '../api/axios/profileRoutes';
import { useCookies } from 'react-cookie';
import { connectChatSocket, connectGameSocket, disconnectChatSocket, disconnectGameSocket } from '../api/socketIo';

interface AuthContextProps {
    userId: number;
    login: string;
    username: string;
    logInWith42: (login: string) => Promise<boolean>;
    logout: () => void;
    isLoggedIn: boolean | null;
    avatar: string;
    updateUsername: (username: string) => Promise<boolean>;
    blockedUsersId: number[] | null;
    refreshBlockedUsersId: () => void;
    isUserNew: boolean;
    twoFAEnabled: boolean;
    enable2FA: () => Promise<string | null>;
    disable2FA: () => void;
    storeUserData: () => void;
    isUserBlocked: (login: string) => Promise<boolean>;
}

export const AuthContext = React.createContext<AuthContextProps>({} as AuthContextProps);

const AuthProvider: FC = ({ children }) => {
    const [userId, setUserId] = useState<number>(0);
    const [login, setLogin] = useState<string>('');
    const [username, setUsername] = useState<string>('');
    const [isLoggedIn, setIsLoggedIn] = useState<boolean | null>(null);
    const [avatar, setAvatar] = useState<string>('');
    const [cookies, setCookie] = useCookies(['trans-cookie']);
    const [blockedUsersId, setBlockedUsersId] = useState<number[] | null>(null);
    const [isUserNew, setIsUserNew] = useState<boolean>(true);
    const [twoFAEnabled, setTwoFAEnabled] = useState<boolean>(false);

    const refreshBlockedUsersId = async () => {
        const newBlockedUsersId = await getBlockedUsersId();
        setBlockedUsersId(newBlockedUsersId);
    };

    const storeUserData = async () => {
        const user = await getUser();
        setUserId(user.id);
        setLogin(user.login);
        if (user.username) {
            setUsername(user.username);
            setAvatar(user.avatar);
            setIsUserNew(false);
            setBlockedUsersId(user.blockedUsersId);
            if (await is2FAEnabled(user.login)) setTwoFAEnabled(true);
        }
        setIsLoggedIn(true);
    };

    const logout = () => {
        // removeCookie('trans-cookie');
        // remove cookie does not set SameSite when removing, so samesite is no longer valid, and so it triggers
        // an error in firefox
        // To solve this, we delete the cookie on our own
        document.cookie = 'trans-cookie=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/; SameSite=Lax';
        setUserId(0);
        setLogin('');
        setUsername('');
        setAvatar('');
        setIsLoggedIn(false);
        disconnectChatSocket();
        disconnectGameSocket();
    };

    const logInWithCookie = async (token: string): Promise<void> => {
        try {
            await storeUserData();
            connectChatSocket(token);
            connectGameSocket(token);
        } catch (e) {
            // JWT is not valid
            logout();
        }
    };

    useEffect(() => {
        const log = async () => {
            if (cookies['trans-cookie'] !== undefined) await logInWithCookie(cookies['trans-cookie']);
            else setIsLoggedIn(false);
        };
        log();
    }, []);

    const logInWith42 = async (inputLogin: string): Promise<boolean> => {
        const token = await generateJWT(inputLogin);
        const login = await getLoginFromJWT(token);
        if (login === inputLogin) {
            try {
                setCookie('trans-cookie', token, { sameSite: 'lax' });
                connectChatSocket(token);
                connectGameSocket(token);
                await storeUserData();
                return true;
            } catch (e) {
                // JWT is not valid
                logout();
            }
        } else logout();
        return false;
    };

    const updateUsername = async (newUsername: string): Promise<boolean> => {
        if (await storeUniqueUsername(newUsername)) {
            setUsername(newUsername);
            if (isUserNew) setIsUserNew(false);
            return true;
        }
        return false;
    };

    const disable2FA = async () => {
        if (twoFAEnabled === true) {
            await remove2FA(login);
            setTwoFAEnabled(false);
        }
    };

    const enable2FA = async (): Promise<string | null> => {
        if (twoFAEnabled === false) {
            // generate QR code to enable 2 factors authentication
            const qrCode = await generateAndStoreOTPSecret(login);
            setTwoFAEnabled(true);
            return qrCode;
        }
        return null;
    };

    const isUserBlocked = async (friendLogin: string): Promise<boolean> => {
        const userId = await getUserIdByLogin(friendLogin);
        if (userId && blockedUsersId && blockedUsersId !== [])
            return blockedUsersId?.find((blockedUserId) => blockedUserId === userId) !== undefined;
        return false;
    };

    return (
        <AuthContext.Provider
            value={{
                userId,
                login,
                username,
                logInWith42,
                logout,
                isLoggedIn,
                avatar,
                updateUsername,
                blockedUsersId,
                refreshBlockedUsersId,
                isUserNew,
                twoFAEnabled,
                enable2FA,
                disable2FA,
                storeUserData,
                isUserBlocked,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;
