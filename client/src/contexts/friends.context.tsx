import React, { FC, useEffect, useState } from 'react';
import { findFriends } from '../api/axios/friendsRoutes';
import { chatSocket } from '../api/socketIo';
import { joinAllRooms } from '../api/socketIo/chatMessages';
import { User } from '../common/user/dto/user.dto.out';

interface FriendsContextProps {
    friends: User[];
    loadFriendsList: () => Promise<void>;
}

export const FriendsContext = React.createContext<FriendsContextProps>({} as FriendsContextProps);

const FriendsProvider: FC = ({ children }) => {
    const [friends, setFriends] = useState<User[]>([]);

    const loadFriendsList = async () => {
        try {
            const alreadyFriends = await findFriends();
            setFriends(alreadyFriends);
        } catch (e) {
            setFriends([]);
        }
    };

    useEffect(() => {
        loadFriendsList();
        chatSocket.on('online', () => {
            loadFriendsList();
        });
        chatSocket.on('offline', () => {
            loadFriendsList();
        });
        chatSocket.on('friends-change', () => {
            loadFriendsList();
        });
        return () => {
            chatSocket.off('online');
            chatSocket.off('offline');
            chatSocket.off('friends-change');
        };
    }, []);

    useEffect(() => {
        joinAllRooms();
    }, [friends]);

    return <FriendsContext.Provider value={{ friends, loadFriendsList }}>{children}</FriendsContext.Provider>;
};

export default FriendsProvider;
