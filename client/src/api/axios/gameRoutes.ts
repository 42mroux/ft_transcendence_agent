import clientAPI from '.';
import { GameSettings, GameStatus } from '../../common/game/types/game.types';
import { UserFromDBDto } from '../../common/user/dto/user.dto.out';
import { GameResultType } from '../../components/Game/GameResult';

export const getCurrentGames = async (): Promise<GameResultType[]> => {
    const list = (await clientAPI.get('/games'))?.data;
    return list;
};

export const getGameUpdate = async (gameId: number): Promise<{ score: number[]; status: GameStatus }> => {
    const update = (await clientAPI.get(`/games/${gameId}`)).data;
    return update;
};

export const getGameUsers = async (gameId: number): Promise<UserFromDBDto[]> => {
    const users = (await clientAPI.get(`/games/${gameId}/users`)).data;
    return users;
};

export const getPlayers = async (gameId: number): Promise<UserFromDBDto[]> => {
    const players = (await clientAPI.get(`/games/${gameId}/players`)).data;
    return players;
};

export const getSettings = async (gameId: number): Promise<GameSettings> => {
    const settings = (await clientAPI.get(`/games/${gameId}/settings`)).data;
    return settings;
};
