import clientAPI from '.';
import { User } from '../../common/user/dto/user.dto.out';

export const findPotentialFriends = async (): Promise<User[]> => {
    const potentialFriends = await clientAPI.get('friends/potential-relationships');
    return potentialFriends.data;
};

export const addNewFriend = async (newFriendLogin: string): Promise<void> => {
    await clientAPI.post(`friends/new-friend/${newFriendLogin}`);
};

export const removeFriend = async (oldFriendLogin: string): Promise<void> => {
    await clientAPI.post(`friends/unfriend/${oldFriendLogin}`);
};

export const findFriends = async (): Promise<User[]> => {
    const friendsLogin = await clientAPI.get('friends/all');
    return friendsLogin.data;
};

export const findPotentialGuestsForChannel = async (roomId: number): Promise<User[] | null> => {
    const potentialGuests = await clientAPI.get(`friends/chat-guests/${roomId}`);
    return potentialGuests.data;
};

export const isAlreadyFriend = async (potentialFriendLogin: string): Promise<boolean> => {
    const isFriend = await clientAPI.get(`friends/already-friends/${potentialFriendLogin}`);
    return isFriend.data;
};
