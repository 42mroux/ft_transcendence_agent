import { io } from 'socket.io-client';
import { CHAT_SOCKET_PORT, GAME_SOCKET_PORT } from '../../common/network';

export const chatSocket = io(`http://localhost:${CHAT_SOCKET_PORT}`, {
    autoConnect: false,
});

export const gameSocket = io(`http://localhost:${GAME_SOCKET_PORT}`, {
    autoConnect: false,
});

export const connectChatSocket = (token: string): void => {
    chatSocket.auth = { token };
    chatSocket.connect();
};

export const disconnectChatSocket = (): void => {
    chatSocket.disconnect();
};

export const connectGameSocket = (token: string): void => {
    gameSocket.auth = { token };
    gameSocket.connect();
};

export const disconnectGameSocket = (): void => {
    gameSocket.disconnect();
};
