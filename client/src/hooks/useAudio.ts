import { useMemo, useState, useEffect } from 'react';

interface useAudioReturnType {
    playing: boolean;
    toggle: () => void;
    play: () => void;
}

export const useAudio = (url: string): useAudioReturnType => {
    const audio = useMemo(() => new Audio(url), []);
    const [playing, setPlaying] = useState<boolean>(false);

    const toggle = () => {
        playing ? audio.pause() : audio.play();
        setPlaying(!playing);
    };

    const play = () => {
        audio.play();
        setPlaying(true);
    };

    useEffect(() => {
        audio.addEventListener('ended', () => setPlaying(false));
        return () => {
            audio.removeEventListener('ended', () => setPlaying(false));
        };
    }, []);

    return { playing, toggle, play };
};
