--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: achievement_types; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.achievement_types (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.achievement_types OWNER TO trans_user;

--
-- Name: achievement_types_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.achievement_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.achievement_types_id_seq OWNER TO trans_user;

--
-- Name: achievement_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.achievement_types_id_seq OWNED BY public.achievement_types.id;


--
-- Name: admins_x_rooms; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.admins_x_rooms (
    id integer NOT NULL,
    "adminId" integer,
    "roomId" integer
);


ALTER TABLE public.admins_x_rooms OWNER TO trans_user;

--
-- Name: admins_x_rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.admins_x_rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admins_x_rooms_id_seq OWNER TO trans_user;

--
-- Name: admins_x_rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.admins_x_rooms_id_seq OWNED BY public.admins_x_rooms.id;


--
-- Name: banned_users; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.banned_users (
    id integer NOT NULL,
    "userId" integer,
    "roomId" integer,
    "time" timestamp without time zone
);


ALTER TABLE public.banned_users OWNER TO trans_user;

--
-- Name: banned_users_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.banned_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banned_users_id_seq OWNER TO trans_user;

--
-- Name: banned_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.banned_users_id_seq OWNED BY public.banned_users.id;


--
-- Name: blocked_users; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.blocked_users (
    id integer NOT NULL,
    "blockedUserId" integer,
    "blockedById" integer
);


ALTER TABLE public.blocked_users OWNER TO trans_user;

--
-- Name: blocked_users_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.blocked_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blocked_users_id_seq OWNER TO trans_user;

--
-- Name: blocked_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.blocked_users_id_seq OWNED BY public.blocked_users.id;


--
-- Name: friends; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.friends (
    id integer NOT NULL,
    "userId" integer,
    "associatedFriendUserId" integer
);


ALTER TABLE public.friends OWNER TO trans_user;

--
-- Name: friends_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.friends_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.friends_id_seq OWNER TO trans_user;

--
-- Name: friends_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.friends_id_seq OWNED BY public.friends.id;


--
-- Name: games; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.games (
    id integer NOT NULL,
    "player1Id" integer,
    "player2Id" integer,
    score1 integer,
    score2 integer,
    status character varying
);


ALTER TABLE public.games OWNER TO trans_user;

--
-- Name: games_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.games_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_id_seq OWNER TO trans_user;

--
-- Name: games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.games_id_seq OWNED BY public.games.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    "senderId" integer,
    "roomId" integer,
    "time" character varying,
    content character varying
);


ALTER TABLE public.messages OWNER TO trans_user;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.messages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO trans_user;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: muted_users; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.muted_users (
    id integer NOT NULL,
    "userId" integer,
    "roomId" integer,
    "time" timestamp without time zone
);


ALTER TABLE public.muted_users OWNER TO trans_user;

--
-- Name: mute_users_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.mute_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mute_users_id_seq OWNER TO trans_user;

--
-- Name: mute_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.mute_users_id_seq OWNED BY public.muted_users.id;


--
-- Name: room_types; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.room_types (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.room_types OWNER TO trans_user;

--
-- Name: room_types_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.room_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.room_types_id_seq OWNER TO trans_user;

--
-- Name: room_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.room_types_id_seq OWNED BY public.room_types.id;


--
-- Name: rooms; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.rooms (
    id integer NOT NULL,
    name character varying,
    "typeId" integer,
    "ownerId" integer,
    image character varying,
    password character varying
);


ALTER TABLE public.rooms OWNER TO trans_user;

--
-- Name: rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_id_seq OWNER TO trans_user;

--
-- Name: rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.rooms_id_seq OWNED BY public.rooms.id;


--
-- Name: scores; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.scores (
    id integer NOT NULL,
    "playerId" integer,
    victories integer,
    losses integer,
    equalities integer,
    "winningRatio" real
);


ALTER TABLE public.scores OWNER TO trans_user;

--
-- Name: scores_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.scores_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scores_id_seq OWNER TO trans_user;

--
-- Name: scores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.scores_id_seq OWNED BY public.scores.id;


--
-- Name: sockets; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.sockets (
    id integer NOT NULL,
    "userId" integer,
    "socketId" character varying
);


ALTER TABLE public.sockets OWNER TO trans_user;

--
-- Name: sockets_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.sockets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sockets_id_seq OWNER TO trans_user;

--
-- Name: sockets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.sockets_id_seq OWNED BY public.sockets.id;


--
-- Name: typeorm_metadata; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.typeorm_metadata (
    type character varying NOT NULL,
    database character varying,
    schema character varying,
    "table" character varying,
    name character varying,
    value text
);


ALTER TABLE public.typeorm_metadata OWNER TO trans_user;

--
-- Name: users; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.users (
    id integer NOT NULL,
    login character varying NOT NULL,
    username character varying,
    "twoFactorAuthenticationSecret" character varying,
    avatar character varying
);


ALTER TABLE public.users OWNER TO trans_user;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO trans_user;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users_x_achievements; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.users_x_achievements (
    id integer NOT NULL,
    "userId" integer,
    "achievementType" integer
);


ALTER TABLE public.users_x_achievements OWNER TO trans_user;

--
-- Name: users_x_achievements_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.users_x_achievements_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_achievements_id_seq OWNER TO trans_user;

--
-- Name: users_x_achievements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.users_x_achievements_id_seq OWNED BY public.users_x_achievements.id;


--
-- Name: users_x_rooms; Type: TABLE; Schema: public; Owner: trans_user
--

CREATE TABLE public.users_x_rooms (
    id integer NOT NULL,
    "userId" integer,
    "roomId" integer
);


ALTER TABLE public.users_x_rooms OWNER TO trans_user;

--
-- Name: users_x_rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: trans_user
--

CREATE SEQUENCE public.users_x_rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_rooms_id_seq OWNER TO trans_user;

--
-- Name: users_x_rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: trans_user
--

ALTER SEQUENCE public.users_x_rooms_id_seq OWNED BY public.users_x_rooms.id;


--
-- Name: achievement_types id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.achievement_types ALTER COLUMN id SET DEFAULT nextval('public.achievement_types_id_seq'::regclass);


--
-- Name: admins_x_rooms id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.admins_x_rooms ALTER COLUMN id SET DEFAULT nextval('public.admins_x_rooms_id_seq'::regclass);


--
-- Name: banned_users id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.banned_users ALTER COLUMN id SET DEFAULT nextval('public.banned_users_id_seq'::regclass);


--
-- Name: blocked_users id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.blocked_users ALTER COLUMN id SET DEFAULT nextval('public.blocked_users_id_seq'::regclass);


--
-- Name: friends id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.friends ALTER COLUMN id SET DEFAULT nextval('public.friends_id_seq'::regclass);


--
-- Name: games id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.games ALTER COLUMN id SET DEFAULT nextval('public.games_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: muted_users id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.muted_users ALTER COLUMN id SET DEFAULT nextval('public.mute_users_id_seq'::regclass);


--
-- Name: room_types id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.room_types ALTER COLUMN id SET DEFAULT nextval('public.room_types_id_seq'::regclass);


--
-- Name: rooms id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.rooms ALTER COLUMN id SET DEFAULT nextval('public.rooms_id_seq'::regclass);


--
-- Name: scores id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.scores ALTER COLUMN id SET DEFAULT nextval('public.scores_id_seq'::regclass);


--
-- Name: sockets id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.sockets ALTER COLUMN id SET DEFAULT nextval('public.sockets_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: users_x_achievements id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_achievements ALTER COLUMN id SET DEFAULT nextval('public.users_x_achievements_id_seq'::regclass);


--
-- Name: users_x_rooms id; Type: DEFAULT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_rooms ALTER COLUMN id SET DEFAULT nextval('public.users_x_rooms_id_seq'::regclass);


--
-- Data for Name: achievement_types; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.achievement_types (id, name) FROM stdin;
1	balls_hit_back
2	nbr_games_played
\.


--
-- Data for Name: admins_x_rooms; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.admins_x_rooms (id, "adminId", "roomId") FROM stdin;
\.


--
-- Data for Name: banned_users; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.banned_users (id, "userId", "roomId", "time") FROM stdin;
\.


--
-- Data for Name: blocked_users; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.blocked_users (id, "blockedUserId", "blockedById") FROM stdin;
\.


--
-- Data for Name: friends; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.friends (id, "userId", "associatedFriendUserId") FROM stdin;
\.


--
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.games (id, "player1Id", "player2Id", score1, score2, status) FROM stdin;
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.messages (id, "senderId", "roomId", "time", content) FROM stdin;
\.


--
-- Data for Name: muted_users; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.muted_users (id, "userId", "roomId", "time") FROM stdin;
\.


--
-- Data for Name: room_types; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.room_types (id, name) FROM stdin;
1	dm
3	public
2	private
4	protected
\.


--
-- Data for Name: rooms; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.rooms (id, name, "typeId", "ownerId", image, password) FROM stdin;
\.


--
-- Data for Name: scores; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.scores (id, "playerId", victories, losses, equalities, "winningRatio") FROM stdin;
\.


--
-- Data for Name: sockets; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.sockets (id, "userId", "socketId") FROM stdin;
\.


--
-- Data for Name: typeorm_metadata; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.typeorm_metadata (type, database, schema, "table", name, value) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.users (id, login, username, "twoFactorAuthenticationSecret", avatar) FROM stdin;
1	mroux	Morgan	\N	mroux.jpg
8	bboop	Betty	\N	bboop.jpg
2	alouis	Andrea		alouis0e4.jpg
\.


--
-- Data for Name: users_x_achievements; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.users_x_achievements (id, "userId", "achievementType") FROM stdin;
\.


--
-- Data for Name: users_x_rooms; Type: TABLE DATA; Schema: public; Owner: trans_user
--

COPY public.users_x_rooms (id, "userId", "roomId") FROM stdin;
\.


--
-- Name: achievement_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.achievement_types_id_seq', 1, false);


--
-- Name: admins_x_rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.admins_x_rooms_id_seq', 143, true);


--
-- Name: banned_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.banned_users_id_seq', 19, true);


--
-- Name: blocked_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.blocked_users_id_seq', 78, true);


--
-- Name: friends_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.friends_id_seq', 328, true);


--
-- Name: games_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.games_id_seq', 120, true);


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.messages_id_seq', 502, true);


--
-- Name: mute_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.mute_users_id_seq', 45, true);


--
-- Name: room_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.room_types_id_seq', 4, true);


--
-- Name: rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.rooms_id_seq', 297, true);


--
-- Name: scores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.scores_id_seq', 3, true);


--
-- Name: sockets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.sockets_id_seq', 5497, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.users_id_seq', 13, true);


--
-- Name: users_x_achievements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.users_x_achievements_id_seq', 1, false);


--
-- Name: users_x_rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: trans_user
--

SELECT pg_catalog.setval('public.users_x_rooms_id_seq', 607, true);


--
-- Name: users PK_a3ffb1c0c8416b9fc6f907b7433; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY (id);


--
-- Name: achievement_types achievement_types_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.achievement_types
    ADD CONSTRAINT achievement_types_pkey PRIMARY KEY (id);


--
-- Name: admins_x_rooms admins_x_rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.admins_x_rooms
    ADD CONSTRAINT admins_x_rooms_pkey PRIMARY KEY (id);


--
-- Name: banned_users banned_users_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.banned_users
    ADD CONSTRAINT banned_users_pkey PRIMARY KEY (id);


--
-- Name: blocked_users blocked_users_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.blocked_users
    ADD CONSTRAINT blocked_users_pkey PRIMARY KEY (id);


--
-- Name: friends friends_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.friends
    ADD CONSTRAINT friends_pkey PRIMARY KEY (id);


--
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: muted_users mute_users_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.muted_users
    ADD CONSTRAINT mute_users_pkey PRIMARY KEY (id);


--
-- Name: room_types room_types_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.room_types
    ADD CONSTRAINT room_types_pkey PRIMARY KEY (id);


--
-- Name: rooms rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id);


--
-- Name: scores scores_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.scores
    ADD CONSTRAINT scores_pkey PRIMARY KEY (id);


--
-- Name: sockets sockets_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.sockets
    ADD CONSTRAINT sockets_pkey PRIMARY KEY (id);


--
-- Name: users_x_achievements users_x_achievements_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_achievements
    ADD CONSTRAINT users_x_achievements_pkey PRIMARY KEY (id);


--
-- Name: users_x_rooms users_x_rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_rooms
    ADD CONSTRAINT users_x_rooms_pkey PRIMARY KEY (id);


--
-- Name: admins_x_rooms admins_x_rooms_adminId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.admins_x_rooms
    ADD CONSTRAINT "admins_x_rooms_adminId_fkey" FOREIGN KEY ("adminId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admins_x_rooms admins_x_rooms_roomId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.admins_x_rooms
    ADD CONSTRAINT "admins_x_rooms_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES public.rooms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: banned_users banned_users_roomId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.banned_users
    ADD CONSTRAINT "banned_users_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES public.rooms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: banned_users banned_users_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.banned_users
    ADD CONSTRAINT "banned_users_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: blocked_users blocked_users_blockedById_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.blocked_users
    ADD CONSTRAINT "blocked_users_blockedById_fkey" FOREIGN KEY ("blockedById") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: blocked_users blocked_users_userBlockedId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.blocked_users
    ADD CONSTRAINT "blocked_users_userBlockedId_fkey" FOREIGN KEY ("blockedUserId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: friends friends_associatedFriendUserId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.friends
    ADD CONSTRAINT "friends_associatedFriendUserId_fkey" FOREIGN KEY ("associatedFriendUserId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: friends friends_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.friends
    ADD CONSTRAINT "friends_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: games games_player1Id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT "games_player1Id_fkey" FOREIGN KEY ("player1Id") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: games games_player2Id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT "games_player2Id_fkey" FOREIGN KEY ("player2Id") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: messages messages_roomId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT "messages_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES public.rooms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: messages messages_senderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT "messages_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: muted_users mute_users_roomId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.muted_users
    ADD CONSTRAINT "mute_users_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES public.rooms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: muted_users mute_users_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.muted_users
    ADD CONSTRAINT "mute_users_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rooms rooms_ownerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "rooms_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rooms rooms_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_type_fkey FOREIGN KEY ("typeId") REFERENCES public.room_types(id);


--
-- Name: scores scores_playerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.scores
    ADD CONSTRAINT "scores_playerId_fkey" FOREIGN KEY ("playerId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sockets sockets_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.sockets
    ADD CONSTRAINT "sockets_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_x_achievements users_x_achievements_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_achievements
    ADD CONSTRAINT "users_x_achievements_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_x_rooms users_x_rooms_roomId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_rooms
    ADD CONSTRAINT "users_x_rooms_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES public.rooms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_x_rooms users_x_rooms_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: trans_user
--

ALTER TABLE ONLY public.users_x_rooms
    ADD CONSTRAINT "users_x_rooms_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

