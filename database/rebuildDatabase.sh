#!/bin/bash

echo 'DROP DATABASE trans_db' | psql postgres
echo 'CREATE DATABASE trans_db' | psql postgres
psql trans_db < $1
